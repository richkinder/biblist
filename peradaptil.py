import os

# Abbreviations of files downloaded from byztxt.com, e.g. MT.CCT.
NT_BYZTXT_ABBREVS = ('MT', 'MR', 'LU', 'JOH', 'AC', 'RO', '1CO', '2CO', 'GA',
                   'EPH', 'PHP', 'COL', '1TH', '2TH', '1TI', '2TI', 'TIT',
                   'PHM', 'HEB', 'JAS', '1PE', '2PE', '1JO', '2JO', '3JO',
                   'JUDE', 'RE', '3TE', '4TE')

# OSIS abbreviations in the NT.
NT_OSIS_ABBREVS = ('Matt', 'Mark', 'Luke', 'John', 'Acts', 'Rom', '1Cor',
                   '2Cor', 'Gal', 'Eph', 'Phil', 'Col', '1Thess', '2Thess',
                   '1Tim', '2Tim', 'Titus', 'Phlm', 'Heb', 'Jas', '1Pet',
                   '2Pet', '1John', '2John', '3John', 'Jude', 'Rev', '3Test',
                   '4Test')

# NT book names to be displayed to the user.
NT_FULL_BOOK_NAMES = ('Matthew', 'Mark', 'Luke', 'John', 'Acts', 'Romans',
                      '1 Corinthians', '2 Corinthians', 'Galatians',
                      'Ephesians', 'Phillipians', 'Colossians',
                      '1 Thessalonians', '2 Thessalonians', '1 Timothy',
                      '2 Timothy', 'Titus', 'Philemon', 'Hebrews', 'James',
                      '1 Peter', '2 Peter', '1 John', '2 John', '3 John',
                      'Jude', 'Revelation', '3Test', '4Test')

# Greek Parsing Dictionary. Taken from the Tischendorf edited by Ulrik
# Sandborg-Peterson.
grk_pos_dict = {'N': 'Noun', 'A': 'Adjective', 'T': 'Article', 'V': 'Verb',
                'P': 'Personal Pronoun', 'R': 'Relative Pronoun',
                'C': 'Reciprocal Pronoun', 'D': 'Demonstrative Pronoun',
                'K': 'Correlative Pronoun', 'I': 'Interrogative Pronoun',
                'X': 'Indefinite Pronoun',
                'Q': 'Correlative or Interrogative Pronoun',
                'F': 'Reflexive Pronoun', 'S': 'Possessive Pronoun',
                'ADV': 'Adverbb', 'CONJ': 'Conjunction', 'COND': 'Conditional',
                'PRT': 'Particle', 'PREP': 'Preposition',
                'INJ': 'Interjection', 'ARAM': 'Aramaic', 'HEB': 'Hebrew',
                'N-PRI': 'Proper Noun Indeclinable',
                'A-NUI': 'Numeral Indeclinable', 'N-LI': 'Letter Indeclinable',
                'N-OI': 'Noun Other Type Indeclinable'}
grk_case_dict = {'N': 'Nominative', 'G': 'Genitive', 'D': 'Dative',
                 'A': 'Accusative', 'V': 'Vocative'}
grk_num_dict = {'S': 'Singular', 'P': 'Plural'}
grk_gender_dict = {'M': 'Masculine', 'F': 'Feminine', 'N': 'Neuter'}
grk_tense_dict = {'P': 'Present', 'I': 'Imperfect', 'F': 'Future',
                  '2F': 'Second Future', 'A': 'Aorist', '2A': 'Second Aorist',
                  'R': 'Perfect', '2R': 'Second Perfect', 'L': 'Pluperfect',
                  '2L': 'Second Pluperfect', 'X': 'No Tense Stated'}
grk_voice_dict = {'A': 'Active', 'M': 'Middle', 'P': 'Passive',
                  'E': 'Middle or Passive', 'D': 'Middle Deponent',
                  'O': 'Passive Deponent', 'N': 'Middle or Passive Deponent',
                  'Q': 'Impersonal Active', 'X': 'No Voice'}
grk_mood_dict = {'I': 'Indicative', 'S': 'Subjunctive', 'O': 'Optative',
                 'M': 'Imperative', 'N': 'Infinitive', 'P': 'Participle',
                 'R': 'Imperative Participle'}
grk_person_dict = {'1': 'First Person', '2': 'Second Person',
                   '3': 'Third Person'}
grk_verb_extra_dict = {'M': 'Middle Significance', 'C': 'Contracted Form',
                       'T': 'Transitive', 'A': 'Aeolic', 'ATT': 'Attic',
                       'AP': 'Apocopated', 'IRR': 'Irregular or Impure Form'}
grk_suffix_dict = {'S': 'Superlative', 'C': 'Comparative',
                   'ABB': 'Abbreviated', 'I': 'Interrogative', 'N': 'Negative',
                   'ATT': 'Attic', 'P': 'Particle Attached', 'K': 'Crasis'}
GRK_PARSED_DICT = [grk_pos_dict, grk_case_dict, grk_num_dict, grk_gender_dict,
                   grk_tense_dict, grk_voice_dict, grk_mood_dict,
                   grk_person_dict, grk_verb_extra_dict, grk_suffix_dict]

# OSIS abbreviations in the order found in the English OT.
OT_ENG_OSIS_ABBREVS = ('Gen', 'Exod', 'Lev', 'Num', 'Deut', 'Josh', 'Judg',
                       'Ruth', '1Sam', '2Sam', '1Kgs', '2Kgs', '1Chr', '2Chr',
                       'Ezra', 'Neh', 'Esth', 'Job', 'Ps', 'Prov', 'Eccl',
                       'Song', 'Isa', 'Jer', 'Lam', 'Ezek', 'Dan', 'Hos',
                       'Joel', 'Amos', 'Obad', 'Jonah', 'Mic', 'Nah', 'Hab',
                       'Zeph', 'Hag', 'Zech', 'Mal')

class StaticInterlinear(object):
    def __init__(self, src_dir, wrk_dirname="wrk", refresh_working=False):
        parsing_classes = {"grk_eng_kjv_nt": GreekEngKJVNTParser
                           self.grk_eng_kjv_nt_parse}
        self.src_dir = src_dir
        self.wrk_dir = os.path.join(src_dir, wrk_dirname)
        self.refresh_working = refresh_working
        
        # Apply the relevant parsing method.
        src_dir_name = os.path.basename(src_dir)
        parsing_methods[dir_name]()

class GreekEngKJVNTParser(object):
    def __init__(self, sil):
        self.prim_src_dir_tagged = os.path.join(self.sil.src_dir,
                                                'byztxt/BP05FNL')
        self.prim_src_dir_check = os.path.join(self.sil.src_dir,
                                               'byztxt/TR-PRSD')
        self.prim_src_dir_text = os.path.join(self.sil.src_dir,
                                              'byztxt/BYZ05CCT')
        self.sec_src_fname = os.path.join(self.sil.src_dir, 'kjvfull.xml')
        self.prim_wrk_dir_tagged = os.path.join(self.sil.wrk_dir, 'tagged')
        self.prim_wrk_dir_check = os.path.join(self.sil.wrk_dir, 'check')
        self.prim_wrk_dir_text = os.path.join(self.sil.wrk_dir, 'text')
        self.sec_wrk_fname = os.path.join(self.sil.wrk_dir, 'kjvprocessed.xml')
        self.grk_strong_dict = {}
        self.eng_strong_dict = {}
        self.munkres = Munkres()
        self.count_mismatch = [] #diag
        self.locs_mismatch = [] #diag
        self.discrepancies = {'tag_vs_check': {}, 'tag_vs_text': {},
                              'kjv_vs_check': {}}
        self.beta_to_uni_dict = self.create_beta_to_uni_dict()
        self.discrepancy_fixes = self.grk_eng_kjv_nt_create_discrepancy_fixes()
        self.osis_lookup = {NT_BYZTXT_ABBREVS[i]: NT_OSIS_ABBREVS[i]
                            for i in range(len(NT_BYZTXT_ABBREVS))}
        self.osis_full_lookup = {NT_BYZTXT_ABBREVS[i]: NT_FULL_BOOK_NAMES[i]
                            for i in range(len(NT_BYZTXT_ABBREVS))}
        if self.refresh_working or not os.path.exists(self.wrk_dir):
            self.preprocess_all()
        self.tagged_re = re.compile(r'''
        ([a-z ]+)\              # one (or more, see Lk.13.7) words with a space
                                # before and after
        (.*?)                   # strong nums and grammar tags
        (?=((\ [a-z|])|(\ ?$))) # lookahead to the next word or end of line.
        ''', re.X)
        self.word_strong_tag_re = re.compile(r'''
        \ ([a-z ]*)\      # one (or more, see Lk.13.7) words with a space
                          # before and after
        ([0-9]*)\         # a Strong''s number followed by a space
        ({[-A-Z]*?})?\ ?  # possibly a grammar tag followed by a space
        ([0-9]*)\ ?       # possibly another Strong''s number followed by a
                          # space
        ({[^}]*?})        # a grammar tag
        ''', re.X)

        self.text_re = re.compile('([^ .,?;]+[.,?;]?)')
        self.variant_re = re.compile(r'''
        \|\               # a pipe and a space
        (.*?)                   # stuff (or perhaps nothing, see 2CO.TRP 7:16)
        \|\               # a pipe and a space
        (.*?)                   # stuff (presumably something)
        \|\               # a pipe and a space
        ''', re.X)
        self.sec_create_language_unit()

    def preprocess_all(self):
        # Create working directories if missing.
        for directory in [self.prim_wrk_dir_tagged, self.prim_wrk_dir_check,
                          self.prim_wrk_dir_text]:
            if not os.path.exists(directory):
                os.makedirs(directory)
        print 'Preprocessing Greek Source - Tagged'
        self.preprocess_prim_tagged(self.prim_src_dir_tagged,
                                    self.prim_wrk_dir_tagged)
        print 'Preprocessing Greek Source - Check'
        self.preprocess_prim_tagged(self.prim_src_dir_check,
                                    self.prim_wrk_dir_check)
        print 'Preprocessing Greek Source - Text'
        self.preprocess_prim_text(self.prim_src_dir_text,
                                  self.prim_wrk_dir_text)
        print 'Preprocessing KJV'
        self.preprocess_sec(self.sec_src_fname, self.sec_wrk_fname)

    def preprocess_prim_tagged(self, src_dir, wrk_dir):
        """Create processed tagged texts that are clean and easier to read."""
        master_text_orig = ''
        master_text = ''
        for src_fname in os.listdir(src_dir):
            # Ignore emacs temp files.
            if '~' in src_fname:
                continue
            root, ext = os.path.splitext(src_fname)
            if root in NT_BYZTXT_ABBREVS:
                print '  Preprocessing ' + src_fname
                with open(join(src_dir, src_fname), 'r') as src_file:
                    text = src_file.read()
                master_text_orig += src_fname + '\n' + text + '\n***********\n'
                for pair in self.discrepancy_fixes[src_fname]:
                    text = re.compile(pair[0], re.S).sub(pair[1], text)
                    
                # AC.BP5 has a big fat OR which I think is in the boolean
                # sense, at 24:8 (or 24:6). fix. does this occur elsewhere?
                # Remove all verses in parentheses left over from processing
                # alternates.
                #text = re.compile(r'\(\d*?:\d*?\) ').sub('', text)
                text = re.compile('- ').sub('-', text) #See 2PE.TRP 1:21 N- GSM
                text = re.compile('[()]').sub('', text)
                if os.path.splitext(src_fname)[1] == '.BP5':
                    # Allowing variants in BP5 desyncs BP5 with CCT since
                    # variants are being removed from CCT. Verify that the
                    # correct variants are being chosen. fix.
                    text = re.compile(r'\|(.*?)\|.*?\|', re.S).sub(r'\1', text)
                else:
                    # Removing variants in TRP desyncs TRP with KJVfull src #s
                    #text = re.compile(r'\| ', re.S).sub(r'', text)
                    pass
                text = re.compile(r'[^a-zA-Z0-9{}|]{2,}', re.S).sub(r' ', text)
                #text = re.compile(r'\n').sub(r' ', text)
                text = re.compile(r'([\d]*:[\d]*)', re.S).sub(r'\n\1', text)
                # Remove newlines before 1:1
                text = re.compile(r'.*? ?(1:1 .*)', re.S).sub(r'\1', text)
                master_text += '\n\n' + src_fname + '\n' + text
            else:
                continue
            with open(join(wrk_dir, src_fname), 'w') as dest_file:
                dest_file.write(text)
        with open(join(wrk_dir, 'master.txt'), 'w') as dest_file:
            dest_file.write(master_text) # Searchable master file
        with open(join(wrk_dir, 'master_orig.txt'), 'w') as dest_file:
            dest_file.write(master_text_orig) # Searchable original master file

    def preprocess_prim_text(self, src_dir, wrk_dir):
        master_text = ''
        master_text_orig = ''
        for src_fname in os.listdir(src_dir):
            # Ignore emacs temp files.
            if '~' in src_fname:
                continue
            root, ext = os.path.splitext(src_fname)
            if root in NT_BYZTXT_ABBREVS:
                print '  Preprocessing ' + src_fname
                if src_fname == 'AC.CCT':
                    with open(join(src_dir,'AC24.CCT'), 'r') as ac24:
                        fix_text = ac24.read()[:-1] # Don't take the last \n.
                        self.discrepancy_fixes['AC.CCT'].extend([
                            (r'24:06.*?(?=24:09)', fix_text)])
                with open(join(src_dir,src_fname), 'r') as src_file:
                    text = src_file.read()
                master_text_orig += src_fname + '\n' + text + '\n***********\n'
                for pair in self.discrepancy_fixes[src_fname]:
                    text = re.compile(pair[0], re.S).sub(pair[1], text)
                # Fix. This is too simplistic and eliminates things such as the
                # sou/se in AC24.CCT (I assume it's in the other files as well.
                text = re.compile(r'{.*?}', re.S).sub(r'', text)
                text = re.compile(r'[ \n\r]{2,}', re.S).sub(r' ', text)
                text = re.compile(r'([\d]+:[\d]+)', re.S).sub(r'\n\1', text)
                #remove newlines before 1:1, and initial metadata lines.
                text = re.compile(r'.*?\n.*?\n(.*)', re.S).match(text).group(1)
                text = re.compile(r'0(\d:)').sub(r'\1', text)
                text = re.compile(r'(:)0(\d)').sub(r'\1\2', text)
                master_text += '\n\n' + src_fname + '\n' + text
            else:
                continue
            with open(join(wrk_dir, src_fname), 'w') as dest_file:
                dest_file.write(text)
        with open(join(wrk_dir, 'master.txt'), 'w') as dest_file:
            dest_file.write(master_text)
        with open(join(wrk_dir, 'master_orig.txt'), 'w') as dest_file:
            dest_file.write(master_text_orig)

    def preprocess_sec():
        with open(self.sec_src_fname, 'r') as src_file:
            print '  Reading ' os.path.basename(self.sec_src_fname) + '...'
            text = src_file.read()
        print '  Cleaning seg, divineName, and inscription tags from xml...'
        text = re.compile(r'''
        (</?q.*?>)
        | (</?seg.*?>)
        | (</?divineName.*?>)
        | (</?inscription.*?>)
        | (</?transChange.*?>)
        | (</?milestone.*?>)
        | <note.*?>.*?</note>
        | <foreign.*?>.*?</foreign>
        | <verse\ eID=.*?/>
        ''', re.VERBOSE).sub('', text)
        print '  Formatting nicely for debugging purposes...'
        text = re.compile('<div type="coloph.*?>.*?</div>').sub(r'',text)
        text = re.compile('<title type="[^p].*?>.*?</title>').sub(r'',text)
        #text = re.compile('(lemma=")[^1-9]*').sub(r'\1',text)
        #text = re.compile(' morph=".*?"').sub(r'',text)
        text = re.compile('robinson:').sub(r'',text)
        # A word in Mark 15:34 appears to be in error, missing the src.
        tmp = r'<w src="21" lemma="strong:G3450"'
        text = re.compile('<w lemma="strong:G3450"').sub(tmp ,text)
        text = re.compile('strong:H0').sub(r'H',text)
        text = re.compile('strong:').sub(r'',text)
        text = re.compile('strongMorph:').sub(r'',text)
        text = re.compile('<header>.*?</header>',re.S).sub('',text)
        text = re.compile('xmlns=".*?"').sub('xmlns=""',text)
        text = re.compile('(<w .*?/w?>)').sub(r'\n\1',text)
        print 'Removing OT Books...'
        for abbrev in OT_ENG_OSIS_ABBREVS:
            text = re.compile('''
            <div\ type="book"\ osisID="''' + abbrev + '''".*?</div>
            ''', re.VERBOSE | re.DOTALL ).sub('', text)
        
        ## if self.run_type == 'lite':
        ##     print 'Removing all but Malachi and 2 Peter...'
        ##     text = re.compile('''
        ##     <div\ type="book"\ osisID="Gen".*?
        ##     (?=<div\ type="book"\ osisID="Mal")
        ##     | <div\ type="book"\ osisID="Matt".*?
        ##     (?=<div\ type="book"\ osisID="2Pet")
        ##     | <div\ type="book"\ osisID="1John".*?
        ##     (?=</osisText>)
        ##     ''', re.X | re.S).sub('', text)
        with open(self.sec_wrk_fname, 'w') as dest_file:
            print '  Writing ' os.path.basename(self.sec_wrk_fname) + '...'
            dest_file.write(text)
            
    def create_discrepancy_fixes(self):
        discrepancy_fixes = {}
        for abbrev in NT_BYZTXT_ABBREVS:
            discrepancy_fixes[abbrev + '.BP5'] = []
            discrepancy_fixes[abbrev + '.TRP'] = []
            discrepancy_fixes[abbrev + '.CCT'] = []

        # Matt 15:5-6
        # A: 15:5 "umeiv...wfelhyhv kai...autou"
        #    15:6 "kai...umwn"
        # B: 15:5 "umeiv...wfelhyhv"
        #    15:6 "kai...autou kai...umwn"                 
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['MT.BP5'].extend([
            (r'(15:5.*?)(kai .*?N-ASF} autou 846 {P-GSM})(.*?15:6)',
             r'\1\3 \2')])
        discrepancy_fixes['MT.TRP'].extend([
            (r'\(15:6\)', r'')])
        discrepancy_fixes['MT.CCT'].extend([
            (r'(15:05.*?s,) (.*?:)(.*?15:06)', r'\1\3 \2')])

        # Matt 17:14-15
        # A: 17:14 "kai...autw"
        #    17:15 "kai...legwn kurie...udwr"
        # B: 17:14 "kai...autw kai...legwn"
        #    17:15 "kurie...udwr"
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['MT.BP5'].extend([
            (r'(17:14.*?P-ASM})(.*?17:15)(.*?)(?=kurie)', r'\1\3\2 ')])
        discrepancy_fixes['MT.TRP'].extend([
            (r'\(17:15\)', r'')])
        discrepancy_fixes['MT.CCT'].extend([
            (r'(17:14.*?\)to\\n)(.*?17:15 )(.*?)(?= Ku)', r'\1 \3\2')])

        # Matt 20:4-5
        # A: 20:4 "kakeinoiv...umin"
        #    20:5 "oi...aphlyon palin...wsautwv"
        # B: 20:4 "kakeinoiv...umin oi...aphlyon"
        #    20:5 "palin...wsautwv"
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['MT.BP5'].extend([
            (r'(20:4.*?P-2DP})(.*?20:5)(.*?)(?=palin)', r'\1\3\2 ')])
        discrepancy_fixes['MT.TRP'].extend([
            (r'\(20:5\)', r'')])
        discrepancy_fixes['MT.CCT'].extend([
            (r'(20:04.*?i\^n.)(.*?20:05 )(.*?)(?= Pa/lin)', r'\1 \3\2')])

        # Matt. 23:13 and 14.
        # A: 23:13 "ouai...krima" 23:14 "ouai...eiselthein"
        # B: 23:13 "ouai...eiselthein" 23:14 "ouai...krima"
        # KJV is B, CCT is A, TRP and BP5 are nominally A but have
        # parentheses for B. Choosing B.
        discrepancy_fixes['MT.BP5'].extend([
            (r'(23:13.*?)(23:14 .*?)(?=23:15)', r'\2\1'),
            (r'23:13', r'qwertyuiop'),
            (r'23:14', r'23:13'),
            (r'qwertyuiop', r'23:14'),
            (r'\(23:1[34]\)', r'')])
        discrepancy_fixes['MT.TRP'].extend([
            (r'(23:13.*?)(23:14 .*?)(?=23:15)', r'\2\1'),
            (r'23:13', r'qwertyuiop'),
            (r'23:14', r'23:13'),
            (r'qwertyuiop', r'23:14'),
            (r'\(23:1[34]\)', r'')])
        discrepancy_fixes['MT.CCT'].extend([
            (r'(23:13.*?)(23:14 .*?)(?=23:15)', r'\2\1'),
            (r'23:13', r'qwertyuiop'),
            (r'23:14', r'23:13'),
            (r'qwertyuiop', r'23:14'),
            (r'\(23:1[34]\)', r'')])

        # Matt 26:60-61
        # A: 26:60 "kai...euron"
        #    26:61 "usteron...qeudomarturev eipon...auton"
        # B: 26:60 "kai...euron usteron...qeudomarturev"
        #    26:61 "eipon auton"
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['MT.BP5'].extend([
            (r'(26:60.*})(.*?26:61)(.*?)(?=eipon)', r'\1\3\2 ')])
        discrepancy_fixes['MT.TRP'].extend([
            (r'\(26:61\)', r'')])
        discrepancy_fixes['MT.CCT'].extend([
            (r'(26:60.*?ron\.)(.*?26:61 )(.*?)(?= ei)', r'\1 \3\2')])

        # Mark 6:27-28
        # A: 6:27 "kai...autou"
        #    6:28 "o...fulakh kai...authn"
        # B: 6:27 "kai...autou o...fulakh"
        #    6:28 "kai authn"
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['MR.BP5'].extend([
            (r'(6:27.*?P-GSM})(.*?6:28)(.*?)(?=kai)', r'\1\3\2 ')])
        discrepancy_fixes['MR.TRP'].extend([
            (r'\(6:28\)', r'')])
        discrepancy_fixes['MR.CCT'].extend([
            (r'(06:27.*?tou\^).(.*?06:28 )(.*?)(?= kai)', r'\1 \3\2')])

        # Luke 1:73-4
        # A: 1:73 "orkon...hmwn tou...hmin"
        #    1:74 "afobwv...autw"
        # B: 1:73 "orkon...hmwn"
        #    1:74 "tou...hmin afobwv...autw"                 
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['LU.BP5'].extend([
            (r'(1:73.*?)(tou .*?P-1DP})(.*?1:74)', r'\1\3 \2')])
        discrepancy_fixes['LU.TRP'].extend([
            (r'\(1:74\)', r'')])
        discrepancy_fixes['LU.CCT'].extend([
            (r'(01:73.*?), (.*?,)(.*?01:74)', r'\1\3 \2')])

        # Luke 17:36
        # A: 17:36 "duo...afeyhsetai"
        # B: 17:36 blank
        # All are B, except TRP has B with A as a variant.
        discrepancy_fixes['LU.TRP'].extend([
            (r'(17:36.*17:37)', r'17:36 \n17:37')])

        # Acts 4:5-6
        # A: 4:5 "egeneto...grammatein"
        #    4:6 "ein ierousalhm...arcieratikou"
        # B: 4:5 "egeneto...grammateiv...ein ierousalhm"
        #    4:6 "kai annas...arcieratikou"
        # All are B, except TRP has A with B as a variant.
        discrepancy_fixes['AC.TRP'].extend([
            (r'\n4:6 ', r' ')])

        # Acts 9:28-9
        # A: 9:28 "kai...ierousalhm kai...ihsou"
        #    9:29 "elalei...anelein"
        # B: 9:28 "kai...ierousalhm"
        #    9:29 "kai...ihsou elalei...anelein"
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['AC.BP5'].extend([
            (r' (9:28.*?)(kai 2532 {CONJ} parrhsiazomenov.*?})([\r\n].*?9:29)',
            r'\1\3 \2')])
        discrepancy_fixes['AC.TRP'].extend([
            (r'\(9:29\)', r'')])
        discrepancy_fixes['AC.CCT'].extend([
            (r'(09:28.*?Ierousalh/m).*?(ai.*})(.*?09:29)', r'\1.\3 K\2')])

        # Acts 13:32-33
        # A: 13:32 "kai...genomenhn oti...ihsoun
        #    13:33 "wv...se"
        # B: 13:32 "kai...genomenhn"
        #    13:33 "oti...ihsoun wv...se"
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['AC.BP5'].extend([
            (r'(13:32.*?)(oti.*?N-ASM})(.*?13:33)', r'\1\3 \2')])
        discrepancy_fixes['AC.TRP'].extend([
            (r'\(13:33\)', r'')])
        discrepancy_fixes['AC.CCT'].extend([
            (r'(13:32.*?)(o\(/ti.*?n:)(.*?13:33)', r'\1\3 \2')])

        # Acts 24:2 has a (24:3) in it. Yet all 4 texts agree on 24:2
        # and 24:3, so the (24:3) can be removed in the main body of
        # pre_process_tagged.
        #
        # Acts 24:2-3
        # A: 24:2 "klhyentov...legwn"
        #    24:3 "pollhv...pronoiav panth...eucaristiav"
        # B: 24:2 "klhyentov...legwn pollhv...pronoiav"
        #    24:3 "panth...eucaristiav"
        # All are B.
        discrepancy_fixes['AC.TRP'].extend([
            (r'\(24:3\)', r'')])
        discrepancy_fixes['AC.BP5'].extend([
            (r'\(24:3\)', r'')])

        # Acts 24:6-8
        # A: 24:6 "on...krinein parelthwn...aphgagen keleusan...se"
        #    24:7 blank
        #    24:8 "par...autou"
        # B: 24:6 "on...krinein"
        #    24:7 "parelthwn...aphgagen"
        #    24:8 "keleusan...se par...autou"
        # BP5 is A with variant for B, CCT is A, but AC24.CCT shows B
        # TRP & KJV are B. Choosing B
        discrepancy_fixes['AC.BP5'].extend([
            (r'\(24:8\)(.*?)\n:END.*?24:8 ', r'24:8\1'),
            (r'\(24:7\)', r'24:7'),
            (r'(24:6.*?)\|.*?VAR: ', r'\1')])

        # Rom. 14:24-26 and 16:25-27
        # A: verses located in chap. 14
        # B: verses located in chap. 16
        # KJV and TRP are B. CCT and BP5 are A. Choosing B.
        discrepancy_fixes['RO.BP5'].extend([
            (r'(14:24.*?)(15:1.*)', r'\2\1'),
            (r'14:2[456]', r'')])
            # There are square brackets in RO.TRP. Is this a colophon?
            # fix. See also 2CO.TRP. This may be common. Note that in
            # KJVfull this is not brought over (at least in 2CO).
        discrepancy_fixes['RO.CCT'].extend([
            (r'(14:24.*?)(15:01.*)', r'\2\1'),
            (r'14:24', r'16:25'),
            (r'14:25', r'16:26'),
            (r'14:26', r'16:27')])

        # 1 Cor. 2:13
        # This is a non-standard discrepancy fix. There are only three
        # instances in the BP5 files where two morph tags occur one
        # after the other, and this is one of them.
        #
        # A: pneumatikoiv 4152 {A-DPN} {A-DPM}
        # B: pneumatikoiv 4152 {A-DPN}
        # All are B except tagged is A. Choosing B
        discrepancy_fixes['1CO.BP5'].extend([
            (r'{A-DPN} {A-DPM}', r'{A-DPN}')])

        # 1 Cor. 12:1
        # This is a non-standard discrepancy fix. There are only three
        # instances in the BP5 files where two morph tags occur one
        # after the other, and this is one of them.
        #
        # A: twn 3588 {T-GPN} {T-GPM} pneumatikwn 4152 {A-GPN} {A-GPM}
        # B: twn 3588 {T-GPN} pneumatikwn 4152 {A-GPN}
        # All are B except tagged is A. Choosing B
        discrepancy_fixes['1CO.BP5'].extend([
            (r'{T-GPN} {T-GPM}', r'{T-GPN}'),
            (r'{A-GPN} {A-GPM}', r'{A-GPN}')])

        # 2 Cor. 13:12-14
        # A: 13:12 "aspasasye...filhmati aspazontai...pantev"
        #    13:13 "h...amhn"
        #    13:14 nonexistent
        # B: 13:12 "aspasasye...filhmati"
        #    13:13 "aspazontai...pantev"
        #    13:14 "h...amhn"
        # All are B. A is a variant suggested in TRP.
        discrepancy_fixes['2CO.TRP'].extend([
            (r'\(.*?\) ', r'')])

        # Gal 5:7
        # Technically this isn't a standard discrepancy fix. I'm just too lazy
        # to make the parser work with three strong's in a row in TRP files. So
        # I'm cheating until I actually fix this. fix.
        discrepancy_fixes['GA.TRP'].extend([
            (r'1465 348 5656', r'1465 5656')])

        # Rev 7:4
        # Technically this isn't a standard discrepancy fix. I'm just too lazy
        # to make the parser work with three strong's in a row in TRP files. So
        # I'm cheating until I actually fix this. fix.
        discrepancy_fixes['RE.TRP'].extend([
            (r'rmd.*?{A-NUI-ABB}',
             r'r 1540 {A-NUI-ABB} m 5062 {A-NUI-ABB} d 5064 {A-NUI-ABB}')])

        # Eph 3:17-18
        # A: 3:17 "katoikhsai...umwn"
        #    3:18 "en...teyemeliwmenoi ina...uqon"
        # B: 3:17 "katoikhsai...umwn en...teyemeliwmenoi"
        #    3:18 "ina uqon"
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['EPH.BP5'].extend([
            (r'(3:17.*})(.*?3:18)(.*?)(?=ina)', r'\1\3\2 ')])
        discrepancy_fixes['EPH.TRP'].extend([
            (r'\(3:18\)', r'')])
        discrepancy_fixes['EPH.CCT'].extend([
            (r'(03:17.*?mw\^n):(.*?03:18 )(.*?)(?= i)', r'\1 \3\2')])

        # 1 Thess 2:11
        # A: 2:11 "kayaper...paramuthumenoi"
        #    2:12 "kai marturoumenoi...ein...doxan"
        # B: 2:11 "kayaper...paramuthumenoi...kai marturoumenoi"
        #    2:12 "ein...doxan"
        # CCT and BP5 are A, TRP is B, and KJV does not have the phrase
        # "kai marturoumenoi". Choosing B.
        discrepancy_fixes['1TH.BP5'].extend([
            (r'(2:12 )(.*?)(?=eiv)', r'\2\1')])
        discrepancy_fixes['1TH.TRP'].extend([
            (r'\(2:12\)', r'')])
        discrepancy_fixes['1TH.CCT'].extend([
            (r'(02:12 )(.*?)(?=ei\)s)', r'\2\1')])

        # Philemon 1:11-12
        # A: 1:11 "ton...eucrhston on...anepemqa"
        #    1:12 "su...proslabou"
        # B: 1:11 "ton...eucrhston"
        #    1:12 "on...anepemqa su...proslabou"
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['PHM.BP5'].extend([
            (r'(1:11.*?)(on 37.*})(.*?1:12)', r'\1\3 \2')])
        discrepancy_fixes['PHM.TRP'].extend([
            (r'\(1:12\)', r'')])
        discrepancy_fixes['PHM.CCT'].extend([
            (r'(01:11.*?u\)/xrhston,) (.*?:)(.*?01:12)', r'\1\3 \2')])

        # Heb 1:1-2
        # A: 1:1 "polumerwn...profhtaiv ep...uiw"
        #    1:2 "on...epoihsen"
        # B: 1:1 "polumerwn...profhtaiv"
        #    1:2 "ep...uiw on...epoihsen"                 
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['HEB.BP5'].extend([
            (r'(\s1:1.*?)(ep .*?DSM})(.*?\s1:2)', r'\1\3 \2')])
        discrepancy_fixes['HEB.TRP'].extend([
            (r'\(1:2\)', r'')])
        discrepancy_fixes['HEB.CCT'].extend([
            (r'(01:01.*?), (.*?,)(.*?01:02)', r'\1\3 \2')])

        # Heb 7:20-1
        # A: 7:20 "kai...orkwmosiav oi...gegonotev"
        #    7:21 "o...melcisedek"
        # B: 7:20 "kai...orkwmosiav"
        #    7:21 "oi...gegonotev o...melcisedek"                 
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['HEB.BP5'].extend([
            (r'(7:20.*?)(oi .*?P-NPM})(.*?7:21)', r'\1\3 \2')])
        discrepancy_fixes['HEB.TRP'].extend([
            (r'\(7:21\)', r'')])
        discrepancy_fixes['HEB.CCT'].extend([
            (r'(07:20.*?) - (.*?,)(.*?07:21)', r'\1\3 \2')])

        # 3 John 1:14-15
        # A: 1:14 "elpizw...lalhsomen"
        #    1:15 "eirhnh...onoma"
        # B: 1:14 "elpizw...lalhsomen eirhnh...onoma"
        #    1:15 N/A
        # All are B. A is a variant suggested in TRP.
        discrepancy_fixes['3JO.TRP'].extend([
            (r'\(.*?\) ', r'')])

        # Rev 12:17-13:1
        # A: 12:18 "kai...yalasshv"
        #    13:1  "yalasshv...blasfhmiav"
        # B: 12:18 N/A
        #    13:1  "kai...yalasshv yalasshv...blasfhmiav"
        # All are B.
        discrepancy_fixes['RE.TRP'].extend([
            (r'\(12:18\)', r''),
            (r'\(13:1\)', r'')])

        # Rev. 17:8
        # A: 17:7 "kai...kerata to...upagein" 17:8 "kai...perastai"
        # B: 17:7 "kai...kerata" 17:8 "to...upagein kai...perastai"
        # All are B except BP5.
        discrepancy_fixes['RE.BP5'].extend([
            (r'17:8 ', r'')])
        #So many variants in rev 17:8. why? fix?
        self.discrepancy_fixes = discrepancy_fixes
    def sec_create_language_unit(self, parent):
        print '  Creating English Language Unit'
        this_lang_unit = LangUnit(parent, 'eng')
        this_lang_unit['ot'] = self.create_testament(this_lang_unit, 'ot')
        this_lang_unit['nt'] = self.create_testament(this_lang_unit, 'nt')
        return this_lang_unit


class IterableTreeDict(object):
    # Class that allows this_itd['grk_nt.Matt.1.1.1'] to be equivalent to
    # this_itd['grk_nt']['Matt']['1']['1']['1'], preserves order of key
    # additions in a list (like OrderedDict), has an appropriate repr, and
    # acts like defaultdict when a key is not found, by instantiating itself.
    def __init__(self):
        self._child_list = []
        self._child_dict = {}

    def keys(self):
        return self._child_dict.keys()

    def values(self):
        return self._child_dict.values()

    def __iter__(self):
        return self._child_list.__iter__()

    def __len__(self):
        return len(self._child_list)

    def __getitem__(self, key):
        if isinstance(key, int):
            return self._child_list[key]
        elif '.' in key:
            boundary_ind = key.index('.')
            first = key[:boundary_ind]
            rest = key[boundary_ind + 1:]
            return self.__getitem__(first)[rest]
        elif key in self._child_dict:
            return self._child_dict[key]
        else:
            tmp_dict = IterableTreeDict()
            self._child_dict[key] = tmp_dict
            self._child_list.append(tmp_dict)
            return tmp_dict

    def __setitem__(self, key, value):
        if isinstance(key, int):
            for dict_key in self._child_dict:
                if self._child_dict[dict_key] is self._child_list[key]:
                    self._child_list[key] = value
                    self._child_dict[dict_key] = value
        elif '.' in key:
            boundary_ind = key.index('.')
            first = key[:boundary_ind]
            rest = key[boundary_ind + 1:]
            self.__getitem__(first).__setitem__(rest, value)            
        elif key in self._child_dict:
            for list_key in range(len(self._child_list)):
                if self._child_dict[key] is self._child_list[list_key]:
                    self._child_list[list_key] = value
                    self._child_dict[key] = value
        else:
            self._child_dict[key] = value
            self._child_list.append(value)

    def __delitem__(self, key):
        del self._child_list[key]

    def __contains__(self, item):
        if isinstance(item, str):
            if ('.' in item):
                boundary_ind = item.index('.')
                first = item[:boundary_ind]
                rest = item[boundary_ind + 1:]
                if first in self._child_dict:
                    return rest in self.__getitem__(first)
                else:
                    return False
            else:
                return item in self._child_dict
        else:
            return item in self._child_dict

    def __repr__(self):
        printables = []
        for ii in range(len(self._child_list)):
            for key in self._child_dict.keys():
                if self._child_dict[key] == self._child_list[ii]:
                    printables.append(key)
                    break
        print_string = ('<IterableTreeDict object with ' + str(len(self)) +
                        ' keys>\n' + ', '.join(printables) + '\n')
        attribs_to_suppress = ['child_list', 'child_dict', 'keys', 'values',
                               'lang', 'name', 'parent', 'matrix', 'columns']
        attribs_to_suppress_if_long = ['strong_words', 'strong_num_paths',
                                       'strong_num_verses']
        for attr in attribs_to_suppress_if_long:
            if attr in dir(self):
                if len(getattr(self, attr)) > 10:
                    attribs_to_suppress.append('strong_nums')
                    attribs_to_suppress.append('strong_num_paths')
        lines = []
        inds = []
        for attr in dir(self):
            if (attr[0] != '_') & (attr not in attribs_to_suppress):
                this_repr = repr(getattr(self, attr))
                if len(this_repr) > 100:
                    shortened_repr = this_repr[:100] + '...'
                else:
                    shortened_repr = this_repr
                lines.append(attr + ': ' + shortened_repr)
                inds.append(lines[-1].index(':'))
        for line, ind in zip(lines, inds):
            print_string += ' ' * (max(inds) - ind) + line + '\n'
        return print_string

class BibleUnit(IterableTreeDict):
    def __init__(self, parent, name):
        super(BibleUnit, self).__init__()
        self.parent = parent
        self.name = name
        try:
            self.path = self.parent.path + '.' + name
            self.lang = parent.lang
        except AttributeError:
            self.path = name
            self.lang = name


class LangUnit(BibleUnit):
    def __init__(self, parent, name):
        super(LangUnit, self).__init__(parent, name)
    def lower(self, text):
        # Return lowercase form of text.
        lowered_text = []
        for char in text:
            if self.is_capital_dict[char]:
                lowered_text.append(self.swapcase_dict[char])
            else:
                lowered_text.append(char)
        return ''.join(lowered_text)

    def upper(self, text):
        # Return uppercase form of text.
        uppered_text = []
        for char in text:
            if self.is_capital_dict[char] is False:
                uppered_text.append(self.swapcase_dict[char])
            else:
                uppered_text.append(char)
        return ''.join(uppered_text)

    def depunctuate(self, text):
        # Return depunctuated form of text.
        depunctuated_text = []
        for char in text:
            if self.is_capital_dict[char] is None:
                pass
            else:
                depunctuated_text.append(char)
        return ''.join(depunctuated_text)


class Testament(BibleUnit):
    def __init__(self, parent, name):
        super(Testament, self).__init__(parent, name)


class Book(BibleUnit):
    def __init__(self, parent, name):
        super(Book, self).__init__(parent, name)


class Chapter(BibleUnit):
    def __init__(self, parent, name):
        super(Chapter, self).__init__(parent, name)


class Verse(BibleUnit):
    def __init__(self, parent, name):
        super(Verse, self).__init__(parent, name)
        self.is_bad = False
        self.text = ''
        # Fix. Put next and prev methods here, so that a verse can find the
        # verse that precedes and follows it. Matt.1.1.prev() should return
        # None and Mark.1.1.prev() should return Matt.28.20.

        
class BibleWord(BibleUnit):
    '''Contains textual data and methods primarily for debugging.

    Not to be confused with UserWord, which contains data and methods that are
    user-specific and relate to user knowledge of the word.'''
    def __init__(self, parent, name):
        super(BibleWord, self).__init__(parent, name)

    def __repr__(self):
        # Initial newline makes it look natural when words are shown in a list.
        print_string = '\n' + ' ' * 4 + 'Path: ' + self.path + '\n'
        if self.text:
            print_string += ' ' * 4 + 'Text: ' + self.text + '\n'
        else:
            print_string += ' ' * 4 + 'Text: *None*\n'

        counter = 0
        if hasattr(self, 'tags'):
            for item in self.tags:
                counter += 1
                print_string += ' ' * 3 + 'Tag ' + str(counter) + ': '
                print_string += item + '\n'
            if not self.tags:
                print_string += ' ' * 3 + 'Tag 1: ' + '*None*'

        counter = 0
        if hasattr(self, 'strong_words'):
            for item in self.strong_words:
                count += 1
                print_string += 'Strong ' + str(counter) + ': '
                print_string += item.lang + ' ' + str(item.strong_num) + '\n'
            if not self.strong_words:
                print_string += 'Strong 1: ' + '*None*\n'
        elif hasattr(self, 'strong_nums'):
            if self.strong_nums:
                print_string += ' Strong Nums: ' + str(self.strong_nums)
            else:
                print_string += ' Strong Nums: *None*\n'

        if 'eng_word' in dir(self):
            if self.eng_word:
                print_string += (' ' * 5 + 'Eng: ' + self.eng_word.text + ' '
                                 + '\n')

        return print_string.encode('utf-8')

class VerseMap(object):
    # Callable object which takes in either Hebrew or English Verses as
    # arguments and returns the target verse reference in English or Hebrew.
    def __init__(self, vm_src_file):
        vm_parsed = minidom.parse(vm_src_file)
        tmp = vm_parsed.getElementsByTagName('verseMap')[0]
        vm_booklist = tmp.getElementsByTagName('book')
        # A mapping from kjv to wlc and vice versa.
        mapping = {}
        for vm_book in vm_booklist:
            vm_verselist = vm_book.getElementsByTagName('verse')
            for vm_verse in vm_verselist:
                tmp_wlc = vm_verse.getAttribute('wlc')
                tmp_kjv = vm_verse.getAttribute('kjv')
                tmp_type = vm_verse.getAttribute('type')
                mapping['eng.ot.' + tmp_kjv] = {'verse': 'ot.' + tmp_wlc,
                                                'type': tmp_type}
                mapping['heb.ot.' + tmp_wlc] = {'verse': 'ot.' + tmp_kjv,
                                                'type': tmp_type}
        self.mapping = mapping
    
    def __call__(self, verse):
        if verse.path in self.mapping:
            return (self.mapping[verse.path]['verse'],
                    self.mapping[verse.path]['type'])
        else:
            if verse.lang == 'heb':
                return (verse.path.replace('heb.', ''), 'original')
            elif verse.lang == 'eng':
                return (verse.path.replace('eng.', ''), 'original')

        
class UninflectedDict(object):
    # Callable object which maps Words or Strong's Numbers to their
    # corresponding uninflected Hebrew forms.
    def __init__(self, uninf_src_file):
        uninf_src_parsed = ElementTree.parse(uninf_src_file)
        mapping = {}
        for entry in uninf_src_parsed.getroot().getchildren():
            tmp = entry.find('{http://www.APTBibleTools.com/namespace}usage')
            if tmp is not None:
                strong_num = entry.attrib['id']
                bdb = entry.find('{http://www.APTBibleTools.com/namespace}bdb')
                word = bdb.find('{http://www.APTBibleTools.com/namespace}w')
                mapping[strong_num] = word.text
        self._mapping = mapping
    
    def __getitem__(self, strong_num):
        return self._mapping[strong_num]
    
    def __contains__(self, strong_num):
        return strong_num in self._mapping
    
