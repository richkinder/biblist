# This file intended to be run with py.test. See the last block for more info.
 
# Tests for biblist_kivy.py
# Standard library imports.
import os
import sys

# Third-party imports
import pytest

# Project imports
try:
    parent_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
except NameError:
    # This file has been executed interactively (perhaps in the Python
    # interpreter through execfile) and does not necessarily have an
    # associated file. Assume that the interpreter was started in the
    # same directory as the file.
    parent_dir = os.path.dirname(os.getcwd())
sys.path.append(parent_dir)
import biblist_kivy

@pytest.fixture()
def app():
    return biblist_kivy.BiblistApp()


# VocabScreen tests.
def update_properties_check(app):
    """Verify that kivy properties have been properly updated.

    This should be able to run without error at any idle state of the
    VocabScreen.

    """
    assert app.prim_lang == app.vocab_cur_ail.prim_lang,
    assert app.sec_lang == app.vocab_cur_ail.sec_lang])
    assert app.book_abbrev == app.vocab_cur_book.abbrev
    assert app.chap_num == str(app.vocab_cur_chap.num)
    assert app.verse_num == str(app.vocab_cur_verse.num)
