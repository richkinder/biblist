# Kivy frontend for Biblist.

# Standard library imports.
import cPickle as pickle
import datetime
import functools
import gc
import itertools as it
import math
import os
import pdb
import random
import re
import sys
import time
import weakref

# Third-party imports
from django.db import transaction
import kivy
from kivy.app import App
from kivy.adapters.listadapter import ListAdapter
from kivy.adapters.models import SelectableDataItem
from kivy.clock import Clock
from kivy.config import ConfigParser
from kivy.core.text import LabelBase
from kivy.core.window import Window
from kivy.factory import Factory
from kivy.garden.graph import Graph, MeshLinePlot, Plot
from kivy.graphics import Mesh, Color, Rectangle
from kivy.properties import NumericProperty, ReferenceListProperty,\
    ObjectProperty, StringProperty, ListProperty, AliasProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.listview import ListItemLabel, SelectableView, CompositeListItem
from kivy.uix.popup import Popup
from kivy.uix.settings import Settings
from kivy.uix.textinput import TextInput
from kivy.vector import Vector

# Project imports
try:
    biblist_kivy_dir = os.path.dirname(os.path.realpath(__file__))
except NameError:
    # This file has been executed interactively (perhaps in the Python
    # interpreter through execfile) and does not necessarily have an
    # associated file. Assume that the interpreter was started in the
    # same directory as the file.
    biblist_kivy_dir = os.getcwd()
biblist_dir = os.path.dirname(biblist_kivy_dir)
django_proj_dir = os.path.join(biblist_dir, 'django_proj')
sys.path.append(biblist_dir)

# Import Django ORM stuff. I'm not sure how to keep the Django ORM
# working unless I chdir into its directory and stay there.
os.chdir(django_proj_dir)
import biblist
from biblist import Testament, Book, Chapter, Verse,\
    EngVerse, StrongWord, Word, User, UserWord, UserVerse, VRRecord,\
    DiagRecord, SATValue, PassedValue

# Miscellaneous setup.
bp = pdb.set_trace

FRONTEND_DATA_FILE_NAME = '../app_data_frontend_1.pickle'
FRONTEND_BACKUP_DATA_FILE_NAME = '../app_data_frontend_2.pickle'

DEFAULT_USER_NAME = 'Defaulto'

# Colors
CORE_BASE_COLOR = [1, 1, 1, 1]
ENG_LABEL_COLOR = [0.5, 0.25, 0, 1]
HIDDEN_COLOR = [0, 0, 0, 1]
TEST_WORD_COLOR = [0, 0, 1, 1]
SPECIAL_PASSED_COLOR = [0.8, 1, 0.8, 1]
SPECIAL_FAIL_COLOR = [1, 0.8, 0.8, 1]

# Register the fonts.
LabelBase.register(name='LinLibertine',
                   fn_regular='../fonts/LinLibertine_Rah.ttf',
                   fn_bold='../fonts/LinLibertine_RBah.ttf')

# List font sizes
FONT_SIZES = {'prim_label': {'Small': 32, 'Medium': 48, 'Large': 72},
              'eng_label': {'Small': 24, 'Medium': 36, 'Large': 54},
              'tag_label': {'Small': 16, 'Medium': 24, 'Large': 36},
              'strong_label': {'Small': 16, 'Medium': 24, 'Large': 36}}

# To do:
# -When there is more than one user, the rename listview shows only 1.5 at a
#  time.
# -In Mark 2:10, "legei" and "he saith" were not linked. Correct it and any
#  others like it.

class InvalidTransitionError(Exception):
    pass

class Timezone(datetime.tzinfo):
    '''Fake timezone object.

    Code such as "datetime.datetime.now(tz=Timezone())" produces "aware"
    rather than "naive" datetime objects. This silences Django from
    complaining that a naive dt object was passed in while timezone
    support is active, and so cleans up stdout.

    '''
    def utcoffset(self, dt):
        return datetime.timedelta()
        
    def tzname(self, dt):
        return "fake timezone"
        
    def dst(self, dt):
        return datetime.timedelta()

        
class MainMenuScreen(FloatLayout):
    def __init__(self, app, **kwargs):
        self.app = app
        super(MainMenuScreen, self).__init__(**kwargs)

    
class UserDataItem(SelectableDataItem):
    def __init__(self, name, **kwargs):
        self.name = name
        super(UserDataItem, self).__init__(**kwargs)

        
class UsersScreen(FloatLayout):
    def __init__(self, app, **kwargs):
        self.app = app
        super(UsersScreen, self).__init__(**kwargs)

        
class WelcomeScreen(FloatLayout):
    def __init__(self, app, **kwargs):
        self.app = app
        super(WelcomeScreen, self).__init__(**kwargs)

        
class VocabScreen(BoxLayout):
    bottom_bar_label_text = StringProperty('')
    secondary_label_text = StringProperty('')
    def __init__(self, app, **kwargs):
        self.app = app
        super(VocabScreen, self).__init__(**kwargs)

        # Keep references to widgets so they may be removed and re-added
        # without being garbage collected.
        self.widget_references = []
        for widget in self.ids.values():
            self.widget_references.append(widget.__self__)
            
    def update_properties(self):
        '''Update the Kivy properties in case of underlying changes.

        Care must be taken to call this whenever the following
        attributes are changed.

        '''
        prim_lang = self.app.vocab_cur_tsmt.prim_lang
        sec_lang = self.app.vocab_cur_tsmt.sec_lang
        book, chap, verse = self.app.vocab_cur_verse_path.split('.')
        self.bottom_bar_label_text = ' / '.join([prim_lang, sec_lang])

class ReadScreen(BoxLayout):
    bottom_bar_lang_text = StringProperty('')
    secondary_label_text = StringProperty('')
    def __init__(self, app, **kwargs):
        self.app = app
        super(ReadScreen, self).__init__(**kwargs)
        self.book_dropdown = DropDown()
        self.chap_dropdown = DropDown()
        self.verse_dropdown = DropDown()

    def update_properties(self):
        '''Update the Kivy properties in case of underlying changes.

        Care must be taken to call this whenever the following
        attributes are changed.

        '''
        prim_lang = self.app.vocab_cur_tsmt.prim_lang
        sec_lang = self.app.vocab_cur_tsmt.sec_lang
        book, chap, verse = self.app.vocab_cur_verse_path.split('.')
        self.bottom_bar_label_text = ' / '.join([prim_lang, sec_lang])

class AddUserPopup(Popup):
    def __init__(self, app, is_forced, **kwargs):
        self.app = app
        self.is_forced = is_forced
        super(AddUserPopup, self).__init__(**kwargs)
        self.ids['user_name_textinput'].bind(on_text_validate=self.validated)

    def validated(self, instance):
        self.ok_press(instance.text)
        
    def ok_press(self, user_name):
        try:
            self.app.backend.add_user(user_name)
        except (biblist.UsernameClashError, biblist.BadUsernameError,
                biblist.UserLimitError) as exc:
            self.app.warn_user(exc.message)
        else:
            user = User.objects.get(name=user_name)
            for tsmt in Testament.objects.order_by('name'):
                self.app.add_curs(user_name, tsmt)
                
            self.app.cur_user = user
            # Update the Kivy properties.
            self.app.cur_user_name = user.name
            self.app.cur_tsmt_name = self.app.vocab_cur_tsmt.disp_name
            self.app.users_list = User.objects.exclude(
                name__contains='\n').order_by('name')
            self.app.go_to('base.diagnostic_grk_intro')

    def cancel_press(self):
        if self.is_forced:
            self.app.warn_user('The application needs a user to continue.')
        else:
            self.app.go_to('base.users')
        

class RenameUser1Popup(Popup):
    def __init__(self, app, **kwargs):
        self.app = app
        super(RenameUser1Popup, self).__init__(**kwargs)
        
    def ok_press(self):
        if self.users_listview.adapter.selection:
            tmp = self.users_listview.adapter.selection[0].text
            self.app.selected_user = tmp
            self.app.go_to('base.users.rename_user_2')
        else:
            self.app.warn_user('Please select a user to rename.')

    def cancel_press(self):
        self.app.go_to('base.users')

        
class RenameUser2Popup(Popup):
    def __init__(self, app, **kwargs):
        self.app = app
        super(RenameUser2Popup, self).__init__(**kwargs)
        self.ids['user_name_textinput'].bind(on_text_validate=self.validated)

    def validated(self, instance):
        self.ok_press(instance.text)

    def ok_press(self, new_user_name):
        old_user_name = self.app.selected_user
        try:
            self.app.backend.rename_user(old_user_name, new_user_name)
        except (biblist.UsernameClashError, biblist.BadUsernameError) as exc:
            self.app.warn_user(exc.message)
        else:
            self.app.rename_curs(old_user_name, new_user_name)
            # Update the Kivy properties.
            self.app.cur_user = User.objects.get(name=new_user_name)
            self.app.cur_user_name = self.app.cur_user.name
            ## The list property doesn't recognize it has been changed
            ## when assigned to a Django queryset that returns the same
            ## members, even if their attributes have been
            ## changed. Force its dispatch by assigning to the empty
            ## list first.
            self.app.users_list = []
            self.app.users_list = User.objects.exclude(
                name__contains='\n').order_by('name')

            self.app.go_to('base.users')

    def cancel_press(self):
        self.app.go_to('base.users')


class DeleteUserPopup(Popup):
    def __init__(self, app, **kwargs):
        self.app = app
        super(DeleteUserPopup, self).__init__(**kwargs)
        
    def ok_press(self):
        if not self.users_listview.adapter.selection:
            self.app.warn_user('Please select a user to delete.')
            return
        tmp = self.users_listview.adapter.selection[0].text
        self.app.selected_user = tmp
        msg = 'Delete user: ' + self.app.selected_user + ' permanently?'
        options = ['No', 'Yes']

        def no_outcome(_):
            if self.app.cur_state != 'base.users.delete_user.question':
                # Protect from multiple presses in rapid succession.
                return
            self.app.go_to('base.users.delete_user')

        def yes_outcome(_):
            if self.app.cur_state != 'base.users.delete_user.question':
                # Protect from multiple presses in rapid succession.
                return
            user_name = self.app.selected_user
            try:
                self.app.backend.delete_user(user_name)
            except (biblist.DeleteLastUserError,
                    biblist.BadUsernameError) as exc:
                self.app.warn_user(exc.message)
            else:
                self.app.delete_curs(user_name)
                if self.app.cur_user_name == user_name:
                    self.app.cur_user = User.objects.exclude(
                        name__contains='\n').order_by('name')[0]
                    self.app.cur_user_name = self.app.cur_user.name
                # Update the Kivy properties.
                self.app.cur_tsmt_name = self.app.vocab_cur_tsmt.disp_name
                self.app.users_list = User.objects.exclude(
                    name__contains='\n').order_by('name')
                self.app.go_to('base.users')

        outcomes = [no_outcome, yes_outcome]
        self.app.ask_user(msg, options, outcomes)

    def cancel_press(self):
        self.app.go_to('base.users')


class SwitchUserPopup(Popup):
    def __init__(self, app, **kwargs):
        self.app = app
        super(SwitchUserPopup, self).__init__(**kwargs)
        
    def user_press(self, user_name):
        self.app.cur_user = User.objects.get(name=user_name)
        # Update the Kivy properties.
        self.app.cur_user_name = self.app.cur_user.name
        self.app.cur_tsmt_name = self.app.vocab_cur_tsmt.disp_name

        self.app.go_to('base.main_menu')


class SwitchInterlinearPopup(Popup):
    def __init__(self, app, **kwargs):
        self.app = app
        super(SwitchInterlinearPopup, self).__init__(**kwargs)
        
    def tsmt_press(self, tsmt_name):
        user_curs = self.app.pickled['curs'][self.app.cur_user.name]
        tsmt = Testament.objects.get(disp_name=tsmt_name)
        user_curs['vocab_cur_tsmt_name'] = tsmt.name
        user_curs['read_cur_tsmt_name'] = tsmt.name
        # Update the Kivy properties.
        self.app.cur_tsmt_name = self.app.vocab_cur_tsmt.disp_name
        
        self.app.go_to('base.main_menu')


class HelpScreen(FloatLayout):
    def __init__(self, app, **kwargs):
        self.app = app
        self._titles = ['Help - Vocab 1', 'Help - Vocab 2', 'Help - Reading',
                        'Help - Advanced']
        super(HelpScreen, self).__init__(**kwargs)
        self.widget_references = []
        # Keep references to widgets so they may be removed and re-added
        # without being garbage collected.
        for widget in self.ids.values():
            self.widget_references.append(widget.__self__)
        self.img_index = 0
        self.update()
        
    def next_released(self):
        self.img_index += 1
        self.update()

    def prev_released(self):
        self.img_index -= 1
        self.update()

    def back_released(self):
        self.app.go_to('base.main_menu')

    def update(self):
        # Show only the image corresponding to img_index.
        img_ind_min = 0
        img_ind_max = 0
        for w_id, widget in self.ids.items():
            match = re.match('image_([0-9]+)$', w_id)
            if match:
                img_ind = int(match.group(1))
                if img_ind == self.img_index:
                    try:
                        self.ids.core_layout.add_widget(widget)
                    except kivy.uix.widget.WidgetException:
                        pass
                else:
                    self.ids.core_layout.remove_widget(widget)
                # Collect the maximum index.
                if img_ind > img_ind_max:
                    img_ind_max = img_ind
        # Add the prev and next buttons as appropriate.
        if self.img_index > img_ind_min:
            try:
                self.ids.core_layout.add_widget(self.ids.prev_button)
            except kivy.uix.widget.WidgetException:
                pass
                
        else:
            self.ids.core_layout.remove_widget(self.ids.prev_button)
        if self.img_index < img_ind_max:
            try:
                self.ids.core_layout.add_widget(self.ids.next_button)
            except kivy.uix.widget.WidgetException:
                pass
        else:
            self.ids.core_layout.remove_widget(self.ids.next_button)

        # Set the title.
        self.title = self._titles[self.img_index]
            

class AboutPopup(Popup):
    def __init__(self, app, **kwargs):
        self.app = app
        super(AboutPopup, self).__init__(**kwargs)
        self.title = 'About'
        
    def back_released(self):
        self.app.go_to('base.main_menu')


class DiagnosticPopup(Popup):
    def __init__(self, app, lang, **kwargs):
        self.app = app
        self.title = 'Diagnostic Test'
        super(DiagnosticPopup, self).__init__(**kwargs)

    def button_pressed(self, knownness):
        # Create a DiagRecord corresponding to the chosen word.
        DiagRecord.objects.create(user=self.app.cur_user,
                                  strong_word=self.app.diag_chosen_word,
                                  knownness=knownness,
                                  datetime=datetime.datetime.now(
                                      tz=Timezone()))
        self.app.diagnostic_update_plot()
        self.app.diagnostic_inquire()


class StatusPopup(Popup):
    message = StringProperty()
    def __init__(self, app, message, **kwargs):
        self.app = app
        self.title = 'Status'
        self.message = message
        super(StatusPopup, self).__init__(**kwargs)


class WarningPopup(Popup):
    message = StringProperty()
    def __init__(self, app, message, **kwargs):
        self.app = app
        self.title = 'Warning'
        self.message = message
        super(WarningPopup, self).__init__(**kwargs)

    def ok_press(self):
        self.app.go_to('.'.join(self.app.cur_state.split('.')[:-1]))
        self.dismiss()


class QuestionPopup(Popup):
    message = StringProperty()
    def __init__(self, app, message, options, outcomes, **kwargs):
        self.title = 'Question'
        self.app = app
        self.message = message
        self.options = options
        self.outcomes = outcomes
        super(QuestionPopup, self).__init__(**kwargs)
        for option, outcome in zip(options, outcomes):
            self.options_box.add_widget(BiblistButton(text=option,
                                                    on_release=outcome))


class MeshScatterPlot(Plot):
    '''Displays points on a Graph. Based off MeshLinePlot'''

    def create_drawings(self):
        self._color = Color(*self.color)
        self._mesh = Mesh(mode='points')
        self.bind(color=lambda instr, value: setattr(self._color.rgba, value))
        return [self._color, self._mesh]

    def draw(self, *args):
        super(MeshScatterPlot, self).draw(*args)
        points = self.points
        mesh = self._mesh
        vert = mesh.vertices
        ind = mesh.indices
        params = self._params
        funcx = log10 if params['xlog'] else lambda x: x
        funcy = log10 if params['ylog'] else lambda x: x
        xmin = funcx(params['xmin'])
        ymin = funcy(params['ymin'])
        diff = len(points) - len(vert) // 4
        size = params['size']
        ratiox = (size[2] - size[0]) / float(funcx(params['xmax']) - xmin)
        ratioy = (size[3] - size[1]) / float(funcy(params['ymax']) - ymin)
        if diff < 0:
            del vert[4 * len(points):]
            del ind[len(points):]
        elif diff > 0:
            ind.extend(range(len(ind), len(ind) + diff))
            vert.extend([0] * (diff * 4))
        for k in range(len(points)):
            vert[k * 4] = (funcx(points[k][0]) - xmin) * ratiox + size[0]
            vert[k * 4 + 1] = (funcy(points[k][1]) - ymin) * ratioy + size[1]

        # Add vertices to make a small cross figure instead of just a point.
        cross_radius = 2
        new_verts = []
        for i in range(len(vert) / 4):
            cur_vert = vert[4 * i: 4 * i + 4]
            vectors = [[cross_radius, 0, 0, 0],
                       [-cross_radius, 0, 0, 0],
                       [0, cross_radius, 0, 0],
                       [0, -cross_radius, 0, 0]]
            for vector in vectors:
                new_vert = [cur_vert[0] + vector[0],
                            cur_vert[1] + vector[1],
                            cur_vert[2] + vector[2],
                            cur_vert[3] + vector[3]]
                new_verts.extend(new_vert)
        mesh.vertices = vert + new_verts
        mesh.indices = range(len(mesh.vertices) / 4)


    def _set_mode(self, value):
        if hasattr(self, '_mesh'):
            self._mesh.mode = value
    mode = AliasProperty(lambda self: self._mesh.mode, _set_mode)
    '''VBO Mode used for drawing the points. Can be one of: 'points',
    'line_strip', 'line_loop', 'lines', 'triangle_strip', 'triangle_fan'.
    See :class:`~kivy.graphics.Mesh` for more details.

    Defaults to 'line_strip'.
    '''

    
class BiblistButton(Button):
    '''Generic button with app styling.

    This class needs to be here instead of in a .kv file, otherwise some
    buttons in lists stop working.

    '''    
    def __init__(self, **kwargs):
        super(BiblistButton, self).__init__(**kwargs)
        self.background_normal = '../imgs/button_normal.png'
        self.background_down = '../imgs/button_down.png'
        self.border = 8, 8, 8, 8
    

class BiblistListItemButton(Button, SelectableView):
    '''App-styled item button. Inspired by kivy.uix.listview.ListItemButton.'''
    def __init__(self, **kwargs):
        super(BiblistListItemButton, self).__init__(**kwargs)

        self._normal_img_path = '../imgs/button_normal.png'
        self._down_img_path = '../imgs/button_down.png'
        self._selected_img_path = '../imgs/button_selected.png'

        self.background_normal = self._normal_img_path
        self.background_down = self._down_img_path
        self.border = (8, 8, 8, 8)

    def select(self, *args):
        self.background_normal = self._selected_img_path
        if isinstance(self.parent, CompositeListItem):
            self.parent.select_from_child(self, *args)

    def deselect(self, *args):
        self.background_normal = self._normal_img_path
        if isinstance(self.parent, CompositeListItem):
            self.parent.deselect_from_child(self, *args)

    def select_from_composite(self, *args):
        self.background_normal = self._selected_img_path

    def deselect_from_composite(self, *args):
        self.background_normal = self._normal_img_path

    def __repr__(self):
        return '<%s text=%s>' % (self.__class__.__name__, self.text)


class BiblistApp(App):
    # Kivy will automatically look for a file named biblist.kv because
    # this class is named BiblistApp and inherits from App.
    users_list = ListProperty([])
    tsmt_list = ListProperty([])
    cur_user_name = StringProperty()
    cur_tsmt_name = StringProperty()
    def build_config(self, config):
        config.setdefaults('vocab', {'show_tag': '1',
                                     'show_strong_num': '0',
                                     'font_size': 'Medium'})
        config.setdefaults('read', {'show_tag': '0',
                                     'show_strong_num': '0',
                                     'font_size': 'Medium'})
    def build(self):
        Window.bind(on_keyboard=self.keyboard_handler)
        # Load the serialized frontend data or create it if necessary,
        # ensuring the loaded data is represented in the default file.
        try:
            with open(FRONTEND_DATA_FILE_NAME, 'r') as fid:
                self.pickled = pickle.load(fid)
        except IOError:
            try:
                with open(FRONTEND_BACKUP_DATA_FILE_NAME, 'r') as fid:
                    self.pickled = pickle.load(fid)
            except IOError:
                all_users = User.objects.exclude(name__contains='\n')
                self.pickled = {}
                self.pickled['curs'] = {x.name: {} for x in all_users}
                for user in all_users:
                    for tsmt in Testament.objects.order_by('-name'):
                        self.add_curs(user.name, tsmt)
                with open(FRONTEND_DATA_FILE_NAME, 'w') as fid:
                    pickle.dump(self.pickled, fid)
            else:
                os.rename(FRONTEND_BACKUP_DATA_FILE_NAME,
                          FRONTEND_DATA_FILE_NAME)

        # Miscellaneous variables.
        self.backend = biblist.AppBackend()
        self.diag_language_known = None
        self.vocab_hist_num = 0
        self.font_sizes = FONT_SIZES
        self.cur_state = 'base'
        self.valid_state_transitions = {
            'base': ['base.main_menu', 'base.ensure_user'],
            'base.ensure_user': ['base.main_menu',
                                 'base.welcome_screen'],
            'base.welcome_screen': ['base.add_user_forced'],
            'base.main_menu': ['base.vocab', 'base.read',
                               'base.diagnostic_grk_intro',
                               'base.settings', 'base.users', 'base.help',
                               'base.about', 'base.main_menu.switch_user',
                               'base.main_menu.switch_tsmt',
                               'base.main_menu.quit'],
            'base.main_menu.switch_user': ['base.main_menu'],
            'base.main_menu.switch_tsmt': ['base.main_menu'],
            'base.vocab': ['base.main_menu', 'base.vocab.vocab_ask'],
            'base.vocab.vocab_ask': ['base.main_menu',
                                     'base.vocab.vocab_answer',
                                     'base.vocab.vocab_hist',
                                     'base.vocab.vocab_ask'],
            'base.vocab.vocab_answer': ['base.main_menu',
                                        'base.vocab.vocab_ask'],
            'base.vocab.vocab_hist': ['base.main_menu',
                                      'base.vocab.vocab_ask'],
            'base.read': ['base.main_menu', 'base.read.read_disp'],
            'base.read.read_disp': ['base.main_menu', 'base.read',
                                    'base.read.read_disp'],
            'base.settings': ['base.main_menu'],
            'base.diagnostic_grk_intro': ['base.diagnostic_grk_intro.status',
                                          'base.diagnostic_grk'],
            'base.diagnostic_grk_intro.status': ['base.diagnostic_heb_intro'],
            'base.diagnostic_grk': ['base.diagnostic_heb_intro',
                                    'base.diagnostic_grk.status'],
            'base.diagnostic_grk.status': ['base.diagnostic_heb_intro'],
            'base.diagnostic_heb_intro': ['base.diagnostic_heb_intro.status',
                                          'base.diagnostic_heb'],
            'base.diagnostic_heb_intro.status': ['base.main_menu'],
            'base.diagnostic_heb': ['base.main_menu',
                                    'base.diagnostic_heb.status'],
            'base.diagnostic_heb.status': ['base.main_menu'],
            'base.help': ['base.main_menu'],
            'base.about': ['base.main_menu'],
            'base.add_user_forced': ['base.add_user_forced.warning',
                                     'base.diagnostic_grk_intro'],
            'base.add_user_forced.warning': ['base.add_user_forced'],
            'base.users': ['base.main_menu', 'base.users.add_user',
                           'base.users.delete_user',
                           'base.users.rename_user_1'],
            'base.users.add_user': ['base.users',
                                    'base.users.add_user.warning',
                                    'base.diagnostic_grk_intro'],
            'base.users.add_user.warning': ['base.users.add_user'],
            'base.users.delete_user': ['base.users',
                                       'base.users.delete_user.warning',
                                       'base.users.delete_user.question'],
            'base.users.delete_user.warning': ['base.users.delete_user'],
            'base.users.delete_user.question': [
                'base.users',
                'base.users.delete_user',
                'base.users.delete_user.question.warning'],
            'base.users.delete_user.question.warning': [
                'base.users.delete_user.question'],
            'base.users.rename_user_1': ['base.users',
                                         'base.users.rename_user_2',
                                         'base.users.rename_user_1.warning'],
            'base.users.rename_user_1.warning': ['base.users.rename_user_1'],
            'base.users.rename_user_2': ['base.users',
                                         'base.users.rename_user_1',
                                         'base.users.rename_user_2.warning'],
            'base.users.rename_user_2.warning': ['base.users.rename_user_2']}
        
        # Create the root widget and start the application by ensuring
        # there is a user.
        self.root = FloatLayout()
        self.go_to('base.ensure_user')
        #Clock.schedule_once(lambda x: self.go_to('base.vocab'), 3)
        #Clock.schedule_once(lambda x: self.go_to('base.read'), 3)
        #Clock.schedule_once(lambda x: self.go_to('base.diagnostic_grk'), 3)
        return self.root

    def keyboard_handler(self, window, keycode1, keycode2, text, modifiers):
        # Enter debug if Ctrl-d is pressed.
        if modifiers == ['ctrl'] and keycode1 == 100: # i.e. Ctrl-d
            bp()
            return True
        if keycode1 == 27: # i.e. escape
            if self.cur_state == 'base.main_menu':
                self.save_data()
                # Do not consume the keypress, instead let it
                # propagate. This results in exit under standard Kivy
                # behavior.
                return False
            else:
                self.go_to('base.main_menu')
                return True
        return False

    def add_curs(self, uname, tsmt):
        tsmt_name = tsmt.name
        book_path = tsmt.book_set.get(index=0).name
        chap_path = book_path + '.1'
        verse_path = chap_path + '.1'
        self.pickled['curs'].setdefault(uname, {
                'read_cur_tsmt_name': tsmt_name,
                'vocab_cur_tsmt_name': tsmt_name})
        self.pickled['curs'][uname][tsmt_name] = {
            'read_cur_verse_path': verse_path,
            'vocab_cur_verse_path': verse_path}
        self.save_data()

    def delete_curs(self, uname):
        del self.pickled['curs'][uname]
        self.save_data()

    def rename_curs(self, from_uname, to_uname):
        self.pickled['curs'][to_uname] = self.pickled['curs'][from_uname]
        del self.pickled['curs'][from_uname]
        self.save_data()

    def validate_state_transition(self, proposed_state):
        if proposed_state not in self.valid_state_transitions[self.cur_state]:
            tmp = 'Cannot reach this state ({}) from the current one ({}).'
            tmp = tmp.format(proposed_state, self.cur_state)
            raise InvalidTransitionError(tmp)
        
    def on_pause(self):
        '''Allows the program to be paused.'''
        return True

    # Transition methods associated with a state of departure and arrival.
    def base_to_main_menu(self):
        self.main_menu_entry()

    def main_menu_to_base(self):
        self.main_menu_exit()

    def base_to_ensure_user(self):
        self.ensure_user_entry()

    def ensure_user_to_base(self):
        self.ensure_user_exit()

    def base_to_welcome_screen(self):
        self.welcome_screen_entry()

    def welcome_screen_to_base(self):
        self.welcome_screen_exit()

    def base_to_read(self):
        self.read_entry()

    def read_to_base(self):
        self.read_exit()

    def base_to_vocab(self):
        self.vocab_entry()

    def vocab_to_base(self):
        self.vocab_exit()

    def base_to_settings(self):
        self.settings_entry()

    def settings_to_base(self):
        self.settings_exit()

    def base_to_diagnostic_grk_intro(self):
        self.diagnostic_grk_intro_entry()

    def diagnostic_grk_intro_to_base(self):
        self.diagnostic_grk_intro_exit()

    def diagnostic_grk_intro_to_status(self):
        self.status_entry()

    def status_to_diagnostic_grk_intro(self):
        self.status_exit()

    def base_to_diagnostic_grk(self):
        self.diagnostic_grk_entry()

    def diagnostic_grk_to_base(self):
        self.diagnostic_grk_exit()

    def diagnostic_grk_to_status(self):
        self.status_entry()

    def status_to_diagnostic_grk(self):
        self.status_exit()

    def base_to_diagnostic_heb_intro(self):
        self.diagnostic_heb_intro_entry()

    def diagnostic_heb_intro_to_base(self):
        self.diagnostic_heb_intro_exit()

    def diagnostic_heb_intro_to_status(self):
        self.status_entry()

    def status_to_diagnostic_heb_intro(self):
        self.status_exit()

    def base_to_diagnostic_heb(self):
        self.diagnostic_heb_entry()

    def diagnostic_heb_to_base(self):
        self.diagnostic_heb_exit()

    def diagnostic_heb_to_status(self):
        self.status_entry()

    def status_to_diagnostic_heb(self):
        self.status_exit()

    def base_to_help(self):
        self.help_entry()

    def help_to_base(self):
        self.help_exit()

    def base_to_about(self):
        self.about_entry()

    def about_to_base(self):
        self.about_exit()

    def base_to_add_user_forced(self):
        self.add_user_forced_entry()

    def add_user_forced_to_base(self):
        self.add_user_forced_exit()

    def main_menu_to_switch_user(self):
        self.switch_user_entry()

    def switch_user_to_main_menu(self):
        self.switch_user_exit()

    def main_menu_to_switch_tsmt(self):
        self.switch_tsmt_entry()

    def switch_tsmt_to_main_menu(self):
        self.switch_tsmt_exit()

    def vocab_to_vocab_ask(self):
        self.vocab_ask_entry()

    def vocab_ask_to_vocab(self):
        self.vocab_ask_exit()

    def vocab_to_vocab_answer(self):
        self.vocab_answer_entry()

    def vocab_answer_to_vocab(self):
        self.vocab_answer_exit()

    def read_to_read_disp(self):
        self.read_disp_entry()

    def read_disp_to_read(self):
        self.read_disp_exit()

    def read_disp_to_read_disp(self):
        self.read_disp_exit()
        self.read_disp_entry()

    def base_to_users(self):
        self.users_entry()

    def users_to_base(self):
        self.users_exit()

    def users_to_warning(self):
        self.warning_entry()

    def warning_to_users(self):
        self.warning_exit()

    def main_menu_to_quit(self):
        self.quit_entry()

    def add_user_forced_to_warning(self):
        self.warning_entry()

    def warning_to_add_user_forced(self):
        self.warning_exit()

    def add_user_to_users(self):
        self.add_user_exit()

    def users_to_add_user(self):
        self.add_user_entry()

    def add_user_to_warning(self):
        self.warning_entry()

    def warning_to_add_user(self):
        self.warning_exit()

    def rename_user_1_to_users(self):
        self.rename_user_1_exit()

    def users_to_rename_user_1(self):
        self.rename_user_1_entry()

    def rename_user_1_to_warning(self):
        self.warning_entry()

    def warning_to_rename_user_1(self):
        self.warning_exit()

    def rename_user_2_to_users(self):
        self.rename_user_2_exit()

    def users_to_rename_user_2(self):
        self.rename_user_2_entry()

    def rename_user_2_to_warning(self):
        self.warning_entry()

    def warning_to_rename_user_2(self):
        self.warning_exit()
        
    def delete_user_to_users(self):
        self.delete_user_exit()

    def users_to_delete_user(self):
        self.delete_user_entry()

    def delete_user_to_warning(self):
        self.warning_entry()

    def warning_to_delete_user(self):
        self.warning_exit()

    def delete_user_to_question(self):
        self.question_entry()

    def question_to_delete_user(self):
        self.question_exit()

    def warning_to_question(self):
        self.warning_exit()

    def question_to_warning(self):
        self.warning_entry()

    # Individual entry and exit methods associated with a state. These
    # are simply methods used by the transition methods above.
    def main_menu_entry(self):
        self.main_menu_screen = MainMenuScreen(self)
        self.root.add_widget(self.main_menu_screen)

    def main_menu_exit(self):
        self.root.remove_widget(self.main_menu_screen)

    def ensure_user_entry(self):
        # Ensure there is a user.
        if User.objects.exclude(name__contains='\n').count() == 0:
            self.go_to('base.welcome_screen')
        else:
            # Set the current user and current user name.
            if hasattr(self.pickled, 'cur_user_name'):
                all_names = [x.name for x in User.objects.exclude(
                    name__contains='\n')]
                if not self.pickled['cur_user_name'] in all_names:
                    self.pickled['cur_user_name'] = User.objects.exclude(
                        name__contains='\n').order_by('pk')[0].name
            else:
                self.pickled['cur_user_name'] = User.objects.exclude(
                    name__contains='\n').order_by('pk')[0].name

            self.cur_user_name = self.pickled['cur_user_name']
            for user in User.objects.exclude(name__contains='\n'):
                if user.name == self.cur_user_name:
                    self.cur_user = user

            # Set the current testament name.
            tmp = self.pickled['curs'][self.cur_user.name]
            self.cur_tsmt_name = self.vocab_cur_tsmt.disp_name
            
            self.users_list = User.objects.exclude(
                name__contains='\n').order_by('name')
            self.tsmts_list = sorted([x.disp_name for x in
                                      Testament.objects.all()])
            self.go_to('base.main_menu')

    def ensure_user_exit(self):
        pass

    def welcome_screen_entry(self):
        self.welcome_screen = WelcomeScreen(self)
        self.root.add_widget(self.welcome_screen)

    def welcome_screen_exit(self):
        self.root.remove_widget(self.welcome_screen)

    def switch_user_entry(self):
        self.switch_user_popup = SwitchUserPopup(self, auto_dismiss=False)
        self.switch_user_popup.open()

    def switch_user_exit(self):
        self.switch_user_popup.dismiss()

    def switch_tsmt_entry(self):
        self.switch_tsmt_popup = SwitchInterlinearPopup(self,
                                                       auto_dismiss=False)
        self.switch_tsmt_popup.open()

    def switch_tsmt_exit(self):
        self.switch_tsmt_popup.dismiss()

    def users_entry(self):
        self.users_screen = UsersScreen(self)
        self.root.add_widget(self.users_screen)

    def users_exit(self):
        self.root.remove_widget(self.users_screen)

    def save_data(self):
        # Save frontend data.
        os.rename(FRONTEND_DATA_FILE_NAME, FRONTEND_BACKUP_DATA_FILE_NAME)
        with open(FRONTEND_DATA_FILE_NAME, 'w') as fid:
            pickle.dump(self.pickled, fid)

    def quit_entry(self):
        self.save_data()
        # fixme. I don't think this is the right way to exit since
        # pygame throws an error, but a mere stop leaves a blank window
        # open.
        self.root.get_root_window().close()
        self.stop()

    def read_entry(self):
        self.read_screen = ReadScreen(self)
        self.root.add_widget(self.read_screen)
        self.go_to('base.read.read_disp')

    def read_exit(self):
        self.root.remove_widget(self.read_screen)

    def read_disp_entry(self):
        # Assign the currents appropriately so that they can be displayed.
        self.read_sat = self.backend.get_read_sat(self.cur_user,
                                                  self.read_cur_tsmt,
                                                  self.read_cur_verse_path)
        self.read_passed = [True if x == 'show' else False for x in
                            self.read_sat]

        # Hide the secondary text.
        self.read_hide_english()
        
        # Clear then build the core layout from the verse.
        core_layout = self.read_screen.ids.core_layout
        core_layout.clear_widgets()
        ## This is necessary for the ScrollView to work.
        core_layout.bind(minimum_height=core_layout.setter('height'))

        ## Determine font sizes based on the setting.
        font_size = self.config.get('read', 'font_size')
        font_sizes = [FONT_SIZES['prim_label'][font_size],
                      FONT_SIZES['eng_label'][font_size],
                      FONT_SIZES['tag_label'][font_size],
                      FONT_SIZES['strong_label'][font_size]]

        self.word_boxes = []
        self.word_labels = []
        cur_verse = self.read_cur_tsmt.get_verse(self.read_cur_verse_path)
        if self.read_cur_tsmt.prim_lang == 'grk':
            self.read_screen.ids.core_layout.orientation = 'lr-tb'
        elif self.read_cur_tsmt.prim_lang == 'heb':
            self.read_screen.ids.core_layout.orientation = 'rl-tb'

        words = cur_verse.word_set.order_by('index')
        for word, cur_sat in zip(words, self.read_sat):
            eng_text = word.eng_text if word.eng_text else '-'
            if self.read_cur_tsmt.prim_lang == 'grk':
                prim_text = word.text
            elif self.read_cur_tsmt.prim_lang == 'heb':
                # Filter out cantillation marks and make text R to L
                tmp = [x for x in word.text if self.keep_hebrew_char(x)]
                prim_text = ''.join(reversed(tmp))

            labels = [Label(text=prim_text, font_name='LinLibertine',
                            bold=True, font_size=font_sizes[0],
                            on_touch_up=self.read_word_label_pressed,
                            size_hint=(None, None)),
                      Label(text=eng_text, color=ENG_LABEL_COLOR,
                            on_touch_up=self.read_word_label_pressed,
                            font_size=font_sizes[1], size_hint=(None, None))]
            if self.config.getint('read', 'show_tag'):
                labels.append(Label(text=word.tag, font_size=font_sizes[2],
                                    size_hint=(None, None)))

            if self.config.getint('read', 'show_strong_num'):
                labels.append(Label(text=str(word.strong_word.number),
                                    font_size=font_sizes[3],
                                    size_hint=(None, None)))
            self.word_labels.append(labels)
            if cur_sat == 'show':
                labels[1].color = HIDDEN_COLOR

            label_spacing = 10
            word_box = BoxLayout(orientation='vertical',
                                 size_hint=(None, None), padding=[5, 5, 5, 5],
                                 spacing=label_spacing)
            self.word_boxes.append(word_box)
            for label in labels:
                word_box.add_widget(label)
                # The texture updates are scheduled for the next frame
                # but we need them now to set the box sizes, so force
                # their update.
                label.texture_update()
            core_layout.add_widget(word_box)

        # Set the word_box widths based on the largest Label width, and
        # their heights based on the sum of the child heights.
        for word_box in core_layout.children:
            max_width = max([x.texture_size[0] for x in word_box.children])
            word_box.width = max_width
            texture_height_sum = 0
            num_widgets = 0
            for label in word_box.children:
                texture_height_sum += label.texture_size[1]
                num_widgets += 1
                label.height = label.texture_size[1]
                label.width = max_width
            word_box.height = ((num_widgets - 1) * label_spacing +
                               texture_height_sum)

        self.read_refresh_dropdowns()
        self.update_properties()

    def read_disp_exit(self):
        pass

    def vocab_entry(self):
        self.vocab_screen = VocabScreen(self)
        self.root.add_widget(self.vocab_screen)

        # Remove the sidebar widgets, and let ask_entry add only the
        # ones it wants.
        sidebar = self.vocab_screen.ids.sidebar
        sidebar.remove_widget(self.vocab_screen.ids.right_btn)
        sidebar.remove_widget(self.vocab_screen.ids.wrong_btn)
        sidebar.remove_widget(self.vocab_screen.ids.show_answer_btn)

        self.go_to('base.vocab.vocab_ask')

    def vocab_exit(self):
        self.root.remove_widget(self.vocab_screen)
        
    def vocab_ask_entry(self):
        # Get the test verse from the backend. This could be one that
        # was given before and never answered, or a new one.
        self.backend.vocab_set_next_verse(self.cur_user, self.vocab_cur_tsmt)

        # Assign the currents appropriately so that they can be displayed.
        record = VRRecord.objects.latest('datetime')
        test_verse = record.verse
        sat_vals = SATValue.objects.filter(vrrecord=record).order_by('index')
        sat = [x.value for x in sat_vals]
        ask_ind = sat.index('ask')
        test_word = test_verse.word_set.get(number=ask_ind + 1)
        test_user_word = UserWord.objects.get(
            user=self.cur_user, strong_word=test_word.strong_word)
        
        self.vocab_cur_verse_path = test_verse.path
        
        sidebar = self.vocab_screen.ids.sidebar
        sidebar.add_widget(self.vocab_screen.ids.show_answer_btn)

        # Set the secondary text.
        self.vocab_screen.secondary_label_text = ''
        
        # Clear then build the core layout from the verse.
        core_layout = self.vocab_screen.ids.core_layout
        core_layout.clear_widgets()
        ## This is necessary for the ScrollView to work.
        core_layout.bind(minimum_height=core_layout.setter('height'))

        ## Determine font sizes based on the setting.
        font_size = self.config.get('vocab', 'font_size')
        font_sizes = [FONT_SIZES['prim_label'][font_size],
                      FONT_SIZES['eng_label'][font_size],
                      FONT_SIZES['tag_label'][font_size],
                      FONT_SIZES['strong_label'][font_size]]

        self.word_boxes = []
        self.word_labels = []

        cur_verse = self.vocab_cur_tsmt.get_verse(self.vocab_cur_verse_path)
        if self.vocab_cur_tsmt.prim_lang == 'grk':
            self.vocab_screen.ids.core_layout.orientation = 'lr-tb'
        elif self.vocab_cur_tsmt.prim_lang == 'heb':
            self.vocab_screen.ids.core_layout.orientation = 'rl-tb'

        for word, cur_sat in zip(test_verse.word_set.order_by('index'), sat):
            if self.vocab_cur_tsmt.prim_lang == 'grk':
                prim_text = word.text
            elif self.vocab_cur_tsmt.prim_lang == 'heb':
                # Filter out cantillation marks and make text R to L
                tmp = [x for x in word.text if self.keep_hebrew_char(x)]
                prim_text = ''.join(reversed(tmp))
            eng_text = word.eng_text if word.eng_text else '-'
            labels = [Label(text=prim_text, font_name='LinLibertine',
                            bold=True, font_size=font_sizes[0],
                            on_touch_up=self.vocab_word_label_pressed,
                            size_hint=(None, None)),
                      Label(text=eng_text, color=ENG_LABEL_COLOR,
                            on_touch_up=self.vocab_word_label_pressed,
                            font_size=font_sizes[1], size_hint=(None, None))]
            if self.config.getint('vocab', 'show_tag') and word.tag:
                labels.append(Label(text=word.tag, font_size=font_sizes[2],
                                    size_hint=(None, None)))

            if self.config.getint('vocab', 'show_strong_num'):
                labels.append(Label(text=str(word.strong_word.number),
                                    font_size=font_sizes[3],
                                    size_hint=(None, None)))
            self.word_labels.append(labels)
            if cur_sat == 'ask':
                # This is an instance of the test word.
                labels[0].color = TEST_WORD_COLOR
                labels[1].color = HIDDEN_COLOR
            elif cur_sat == 'show':
                labels[1].color = HIDDEN_COLOR

            label_spacing = 10
            word_box = BoxLayout(orientation='vertical',
                                 size_hint=(None, None), padding=[5, 5, 5, 5],
                                 spacing=label_spacing)
            self.word_boxes.append(word_box)
            for label in labels:
                word_box.add_widget(label)
                # The texture updates are scheduled for the next frame
                # but we need them now to set the box sizes, so force
                # their update.
                label.texture_update()
            core_layout.add_widget(word_box)

        # Set the word_box widths based on the largest Label width, and
        # their heights based on the sum of the child heights.
        for word_box in core_layout.children:
            max_width = max([x.texture_size[0] for x in word_box.children])
            word_box.width = max_width
            texture_height_sum = 0
            num_widgets = 0
            for label in word_box.children:
                texture_height_sum += label.texture_size[1]
                num_widgets += 1
                label.height = label.texture_size[1]
                label.width = max_width
            word_box.height = ((num_widgets - 1) * label_spacing +
                               texture_height_sum)

        self.update_properties()
        
    def vocab_ask_exit(self):
        sidebar = self.vocab_screen.ids.sidebar
        sidebar.remove_widget(self.vocab_screen.ids.show_answer_btn)
        
    def vocab_answer_entry(self):
        sidebar = self.vocab_screen.ids.sidebar
        core_layout = self.vocab_screen.ids.core_layout
        sidebar.add_widget(self.vocab_screen.ids.right_btn)
        sidebar.add_widget(self.vocab_screen.ids.wrong_btn)

        # Reveal the secondary text label.
        record = VRRecord.objects.latest('datetime')
        test_verse = record.verse
        chapter = test_verse.chapter
        book = chapter.book
        sec_body = test_verse.engverse.text
        sec_text = ' '.join([book.full_name, str(chapter.number), ':',
                             str(test_verse.number), sec_body])

        self.vocab_screen.secondary_label_text = sec_text

        # Reveal the secondary label of the test words by changing its
        # color.
        sat_vals = SATValue.objects.filter(vrrecord=record).order_by('index')
        sat = [x.value for x in sat_vals]
        passed_vals = PassedValue.objects.filter(vrrecord=record).order_by(
            'index')
        passed = [x.value for x in passed_vals]
        for i in range(len(sat)):
            if sat[i] == "ask":
                eng_label = self.word_labels[i][1]
                eng_label.color = TEST_WORD_COLOR
                ask_ind = i

        self.update_properties()
        
    def vocab_answer_exit(self):
        sidebar = self.vocab_screen.ids.sidebar
        sidebar.remove_widget(self.vocab_screen.ids.right_btn)
        sidebar.remove_widget(self.vocab_screen.ids.wrong_btn)
        
    def add_user_entry(self):
        self.add_user_popup = AddUserPopup(self, False, auto_dismiss=False)
        self.add_user_popup.open()

    def add_user_exit(self):
        self.add_user_popup.dismiss()

    def add_user_forced_entry(self):
        self.add_user_popup = AddUserPopup(self, True, auto_dismiss=False)
        self.add_user_popup.open()

    def add_user_forced_exit(self):
        self.add_user_popup.dismiss()
        # Set the current user and current user name.
        all_users = User.objects.exclude(name__contains='\n')
        if 'cur_user_name' in self.pickled:
            all_names = [x.name for x in all_users]
            if not self.pickled['cur_user_name'] in all_names:
                self.pickled['cur_user_name'] = User.objects.exclude(
                    name__contains='\n').order_by('pk')[0].name
        else:
            self.pickled['cur_user_name'] = User.objects.exclude(
                name__contains='\n').order_by('pk')[0].name

        self.cur_user_name = self.pickled['cur_user_name']
        for user in all_users:
            if user.name == self.cur_user_name:
                self.cur_user = user
        self.users_list = all_users
        self.tsmts_list = sorted([x.disp_name for x in
                                  Testament.objects.all()])

    def rename_user_1_entry(self):
        self.rename_user_1_popup = RenameUser1Popup(self, auto_dismiss=False)
        self.rename_user_1_popup.open()

    def rename_user_1_exit(self):
        self.rename_user_1_popup.dismiss()

    def rename_user_2_entry(self):
        self.rename_user_2_popup = RenameUser2Popup(self, auto_dismiss=False)
        self.rename_user_2_popup.open()

    def rename_user_2_exit(self):
        self.rename_user_2_popup.dismiss()

    def delete_user_entry(self):
        self.delete_user_popup = DeleteUserPopup(self, auto_dismiss=False)
        self.delete_user_popup.open()

    def delete_user_exit(self):
        self.delete_user_popup.dismiss()

    def help_entry(self):
        self.help_screen = HelpScreen(self)
        self.root.add_widget(self.help_screen)

    def help_exit(self):
        self.root.remove_widget(self.help_screen)

    def about_entry(self):
        self.about_popup = AboutPopup(self, auto_dismiss=False)
        self.about_popup.open()

    def about_exit(self):
        self.about_popup.dismiss()

    def diagnostic_grk_intro_entry(self):
        self.post_diagnostic_state = 'base.diagnostic_heb_intro'
        self.diag_tsmt = Testament.objects.get(name='nt')
        msg = 'Do you know any Greek?'
        options = ['No', 'Yes']
        def no_outcome(_):
            self.diagnostic_intro_popup.message = "Please wait..."
            self.modify_user_objects(False)
        
        def yes_outcome(_):
            self.go_to('base.diagnostic_grk')
        
        outcomes = [no_outcome, yes_outcome]
        self.diagnostic_intro_popup = QuestionPopup(self, msg, options,
                                                    outcomes)
        self.diagnostic_intro_popup.open()

    def diagnostic_grk_intro_exit(self):
        self.diagnostic_intro_popup.dismiss()

    def diagnostic_grk_entry(self):
        self.diagnostic_popup = DiagnosticPopup(self, 'grk',
                                                auto_dismiss=False)
        self.diag_strat = biblist.diagnostic_emphasize_slope_strategy(
            self.cur_user, self.diag_tsmt)
        self.diagnostic_inquire()
        self.diagnostic_popup.open()

    def diagnostic_grk_exit(self):
        self.diagnostic_popup.dismiss()

    def diagnostic_heb_intro_entry(self):
        self.post_diagnostic_state = 'base.main_menu'
        self.diag_tsmt = Testament.objects.get(name='ot')
        msg = 'Do you know any Hebrew?'
        options = ['No', 'Yes']
        def no_outcome(_):
            self.modify_user_objects(False)
        
        def yes_outcome(_):
            self.go_to('base.diagnostic_heb')
        
        outcomes = [no_outcome, yes_outcome]
        self.diagnostic_intro_popup = QuestionPopup(self, msg, options,
                                                    outcomes)
        self.diagnostic_intro_popup.open()

    def diagnostic_heb_intro_exit(self):
        self.diagnostic_intro_popup.dismiss()

    def diagnostic_heb_entry(self):
        self.diagnostic_popup = DiagnosticPopup(self, 'heb',
                                                auto_dismiss=False)
        self.diag_strat = biblist.diagnostic_emphasize_slope_strategy(
            self.cur_user, self.diag_tsmt)
        self.diagnostic_inquire()
        self.diagnostic_popup.open()

    def diagnostic_heb_exit(self):
        self.diagnostic_popup.dismiss()

    def settings_entry(self):
        self.settings = Settings()
        self.settings.add_json_panel('Vocab', self.config,
                                     '../biblist_kivy/settings_vocab.json')
        self.settings.add_json_panel('Read', self.config,
                                     '../biblist_kivy/settings_read.json')
        self.root.add_widget(self.settings)
        self.settings.bind(on_close=lambda x: self.go_to('base.main_menu'))

    def settings_exit(self):
        self.root.remove_widget(self.settings)

    def status_entry(self):
        self.status_popup = StatusPopup(self, self.status_message,
                                        auto_dismiss=False)
        self.status_popup.open()

    def status_exit(self):
        self.status_popup.dismiss()

    def warning_entry(self):
        self.warning_popup = WarningPopup(self, self.warning_message,
                                          auto_dismiss=False)
        self.warning_popup.open()

    def warning_exit(self):
        self.warning_popup.dismiss()

    def question_entry(self):
        self.question_popup = QuestionPopup(self, self.question_message,
                                            self.question_options,
                                            self.question_outcomes,
                                            auto_dismiss=False)
        self.question_popup.open()

    def question_exit(self):
        self.question_popup.dismiss()

    def go_to(self, new_state):
        '''Transition to the specified state.

        In the following diagram, if the app is at state D, but has been
        told to go to F, it must first perform the D-C transition, the
        C-B, the B-E, and finally the E-F. The chain A-B-C-D is the "old
        chain", while A-B-E-F is the "new chain".
        
                  E-----F
                 / 
        A-----B-----C-----D

        '''
        self.validate_state_transition(new_state)
        old_state = self.cur_state

        # This has to occur here and not at the end of the method,
        # otherwise if a transition method makes another go_to call,
        # changing cur_state, the current call will override it after it
        # returns. Is it a good design decision to allow transition
        # methods to make go_to calls?
        self.cur_state = new_state
        
        new_chain = new_state.split('.')
        old_chain = old_state.split('.')
        zipped = zip(old_chain, new_chain)
        num_in_common = len([x[0] for x in it.takewhile(lambda x: x[0]==x[1],
                                                        zipped)])
        common = new_chain[:num_in_common]
        
        # Unwind across the states unique to the old chain,
        # transitioning state-by-state from a state to the one before
        # it.
        unique_to_old = old_chain[num_in_common:]
        unique_to_old_reversed = unique_to_old[::-1]
        old_undo = unique_to_old_reversed + common[-1:]
        for ii in range(len(old_undo) - 1):
            from_state, to_state = old_undo[ii:ii + 2]
            transition_method_name = '_to_'.join([from_state, to_state])
            method = getattr(self, transition_method_name)
            method()            

        unique_to_new = new_chain[num_in_common:]
        new_redo = common[-1:] + unique_to_new
        for ii in range(len(new_redo) - 1):
            from_state, to_state = new_redo[ii:ii + 2]
            transition_method_name = '_to_'.join([from_state, to_state])
            method = getattr(self, transition_method_name)
            method()

        # Special case where the old state is the same as the new one.
        if old_state == new_state:
            transition_method_name = '_to_'.join([old_chain[-1],
                                                  new_chain[-1]])
            method = getattr(self, transition_method_name)
            method()

    def warn_user(self, message):
        self.warning_title = 'Warning'
        self.warning_message = message
        self.go_to('.'.join([self.cur_state, 'warning']))

    def ask_user(self, message, options, outcomes):
        self.question_title = 'Question'
        self.question_message = message
        self.question_options = options
        self.question_outcomes = outcomes
        self.go_to('.'.join([self.cur_state, 'question']))

    def update_properties(self):
        '''Update all Kivy properties for the current screen.'''
        all_screen_names = ['main_menu_screen', 'users_screen', 'vocab_screen',
                            'read_screen']
        for screen_name in all_screen_names:
            if hasattr(self, screen_name):
                screen = getattr(self, screen_name)
                if screen in self.root.children:
                    screen.update_properties()

    def read_next(self):
        '''Update UserWords of the current verse, then go to the next one.

        This may be in another book or chapter.

        '''
        
        self.read_update_user_words()
        
        # Set read_cur_verse_path to the next verse.
        book, chap, verse = self.read_cur_verse_path.split('.')
        book_obj = Book.objects.get(testament=self.read_cur_tsmt, name=book)
        chap_obj = Chapter.objects.get(book=book_obj, number=chap)
        verse_obj = Verse.objects.get(chapter=chap_obj, number=verse)

        # Determine whether each book/chapter/verse is last in its
        # testament/book/chapter.
        last_book_obj = Book.objects.filter(
            testament=self.read_cur_tsmt).order_by('-index')[0]
        book_is_last = True if book_obj == last_book_obj else False

        last_chap_obj = book_obj.chapter_set.order_by('-index')[0]
        chap_is_last = True if chap_obj == last_chap_obj else False
        
        last_verse_obj = chap_obj.verse_set.order_by('-index')[0]
        verse_is_last = True if verse_obj == last_verse_obj else False

        if verse_is_last:
            if chap_is_last:
                if book_is_last:
                    new_book_obj = Book.objects.filter(
                        testament=self.read_cur_tsmt).order_by('index')[0]
                else:
                    new_book_obj = Book.objects.filter(
                        testament=self.read_cur_tsmt).get(
                            index=book_obj.index + 1)
                new_chap_obj = new_book_obj.chapter_set.order_by('index')[0]
            else:
                new_book_obj = book_obj
                new_chap_obj = book_obj.chapter_set.get(
                    index=chap_obj.index + 1)
            new_verse_obj = new_chap_obj.verse_set.order_by('index')[0]
        else:
            new_book_obj = book_obj
            new_chap_obj = chap_obj
            new_verse_obj = chap_obj.verse_set.get(index=verse_obj.index + 1)

        book = new_book_obj.name
        chap = str(new_chap_obj.number)
        verse = str(new_verse_obj.number)
        self.read_cur_verse_path = '.'.join([book, chap, verse])
        self.go_to('base.read.read_disp')

    def read_prev(self):
        '''Update UserWords of the current verse, then go to the previous one.

        This may be in another book or chapter.

        '''
        
        self.read_update_user_words()
        
        # Set read_cur_verse_path to the next verse.
        book, chap, verse = self.read_cur_verse_path.split('.')
        book_obj = Book.objects.get(testament=self.read_cur_tsmt, name=book)
        chap_obj = Chapter.objects.get(book=book_obj, number=chap)
        verse_obj = Verse.objects.get(chapter=chap_obj, number=verse)

        # Determine whether each book/chapter/verse is first in its
        # testament/book/chapter.
        first_book_obj = Book.objects.filter(
            testament=self.read_cur_tsmt).order_by('index')[0]
        book_is_first = True if book_obj == first_book_obj else False

        first_chap_obj = book_obj.chapter_set.order_by('index')[0]
        chap_is_first = True if chap_obj == first_chap_obj else False
        
        first_verse_obj = chap_obj.verse_set.order_by('index')[0]
        verse_is_first = True if verse_obj == first_verse_obj else False

        if verse_is_first:
            if chap_is_first:
                if book_is_first:
                    new_book_obj = Book.objects.filter(
                        testament=self.read_cur_tsmt).order_by('-index')[0]
                else:
                    new_book_obj = Book.objects.filter(
                        testament=self.read_cur_tsmt).get(
                            index=book_obj.index - 1)
                new_chap_obj = new_book_obj.chapter_set.order_by('-index')[0]
            else:
                new_book_obj = book_obj
                new_chap_obj = book_obj.chapter_set.get(
                    index=chap_obj.index - 1)
            new_verse_obj = new_chap_obj.verse_set.order_by('-index')[0]
        else:
            new_book_obj = book_obj
            new_chap_obj = chap_obj
            new_verse_obj = chap_obj.verse_set.get(index=verse_obj.index - 1)

        book = new_book_obj.name
        chap = str(new_chap_obj.number)
        verse = str(new_verse_obj.number)
        self.read_cur_verse_path = '.'.join([book, chap, verse])
        self.go_to('base.read.read_disp')

    def read_show_english(self):
        cur_verse = self.read_cur_tsmt.get_verse(self.read_cur_verse_path)
        sec_body = cur_verse.engverse.text
        _, chap, verse = self.read_cur_verse_path.split('.')
        full_book_name = cur_verse.chapter.book.full_name
        sec_text = ' '.join([full_book_name, chap + ':' + verse, sec_body])
        self.read_screen.secondary_label_text = sec_text
        self.read_screen.ids.secondary_text_label.color = CORE_BASE_COLOR
        toggle_btn = self.read_screen.ids.toggle_english_btn
        toggle_btn.text = 'Hide\nEnglish'
        toggle_btn.on_release = self.read_hide_english

    def read_hide_english(self):
        self.read_screen.secondary_label_text = ''
        toggle_btn = self.read_screen.ids.toggle_english_btn
        toggle_btn.text = 'Show\nEnglish'
        toggle_btn.on_release = self.read_show_english

    def read_book_btn_released(self):
        book, chap, verse = self.read_cur_verse_path.split('.')
        book_btn = self.read_screen.ids.bottom_bar_book_btn

        # Fill the dropdowns with button widgets.
        book_dd = self.read_screen.book_dropdown
        book_dd.clear_widgets()
        for cur_book in self.read_cur_tsmt.book_set.all():
            btn = BiblistButton(text=cur_book.name, size_hint=(1, None),
                                height=44)
            btn.bind(on_release=lambda button: book_dd.select(button.text))
            book_dd.add_widget(btn)
        book_dd.bind(on_select=self.read_set_book)
        book_dd.open(book_btn)

    def read_chap_btn_released(self):
        book, chap, verse = self.read_cur_verse_path.split('.')
        chap_btn = self.read_screen.ids.bottom_bar_chap_btn

        # Fill the dropdowns with button widgets.
        chap_dd = self.read_screen.chap_dropdown
        chap_dd.clear_widgets()
        all_chaps = self.read_cur_tsmt.book_set.get(
            name=book).chapter_set.all()
        for i in range(len(all_chaps)):
            btn = BiblistButton(text=str(i+1), size_hint=(1, None), height=44)
            btn.bind(on_release=lambda button: chap_dd.select(button.text))
            chap_dd.add_widget(btn)
        chap_dd.bind(on_select=self.read_set_chap)
        chap_dd.open(chap_btn)

    def read_verse_btn_released(self):
        book, chap, verse = self.read_cur_verse_path.split('.')
        verse_btn = self.read_screen.ids.bottom_bar_verse_btn

        # Fill the dropdowns with button widgets.
        verse_dd = self.read_screen.verse_dropdown
        verse_dd.clear_widgets()
        all_verses = self.read_cur_tsmt.book_set.get(
            name=book).chapter_set.get(number=chap).verse_set.all()
        for i in range(len(all_verses)):
            btn = BiblistButton(text=str(i+1), size_hint=(1, None), height=44)
            btn.bind(on_release=lambda button: verse_dd.select(button.text))
            verse_dd.add_widget(btn)
        verse_dd.bind(on_select=self.read_set_verse)
        verse_dd.open(verse_btn)

    def read_update_user_words(self):
        # Build the is_remarkable list.
        is_remarkable = []
        for sat_value, passed_value in zip(self.read_sat, self.read_passed):
            cond = ((sat_value is 'show' and passed_value is False) or
                    (sat_value is 'tell' and passed_value is True))
            if cond:
                is_remarkable.append(True)
            else:
                is_remarkable.append(False)

        with transaction.atomic():
            if any(is_remarkable):
                cur_verse = self.read_cur_tsmt.get_verse(
                    self.read_cur_verse_path)
                record = VRRecord.objects.create(
                    user=self.cur_user,
                    datetime=datetime.datetime.now(tz=Timezone()),
                    verse=cur_verse, module='read')

                for i in range(len(self.read_sat)):
                    tmp = cur_verse.word_set.get(number=i+1)
                    strong_word = tmp.strong_word
                    user_word = UserWord.objects.filter(
                        user=self.cur_user).get(strong_word=strong_word)

                    sat_value = self.read_sat[i]
                    passed_value = self.read_passed[i]

                    user_word.update_after_test(None, sat_value, passed_value)
                    user_word.save()
                    SATValue.objects.create(vrrecord=record, index=i,
                                            value=sat_value)
                    PassedValue.objects.create(vrrecord=record, index=i,
                                               value=passed_value)

                    
    def read_set_book(self, widget, text):
        self.read_update_user_words()
        
        # Set read_cur_verse_path and display the new verse.
        book, chap, verse = self.read_cur_verse_path.split('.')
        self.read_cur_verse_path = '.'.join([text, '1', '1'])
        self.read_refresh_dropdowns()
        self.go_to('base.read.read_disp')
            
    def read_set_chap(self, widget, text):
        self.read_update_user_words()
            
        # Set read_cur_verse_path and display the new verse.
        book, chap, verse = self.read_cur_verse_path.split('.')
        self.read_cur_verse_path = '.'.join([book, text, '1'])
        self.read_refresh_dropdowns()
        self.go_to('base.read.read_disp')
            
    def read_set_verse(self, widget, text):
        self.read_update_user_words()
            
        # Set read_cur_verse_path and display the new verse.
        book, chap, verse = self.read_cur_verse_path.split('.')
        self.read_cur_verse_path = '.'.join([book, chap, text])
        self.read_refresh_dropdowns()
        self.go_to('base.read.read_disp')
            
    def read_refresh_dropdowns(self):
        book, chap, verse = self.read_cur_verse_path.split('.')
        self.read_screen.ids.bottom_bar_book_btn.text = book
        self.read_screen.ids.bottom_bar_chap_btn.text = chap
        self.read_screen.ids.bottom_bar_verse_btn.text = verse
        
    def vocab_right(self):
        self.vocab_right_or_wrong(True)
        
    def vocab_wrong(self):
        self.vocab_right_or_wrong(False)

    def vocab_right_or_wrong(self, passed):
        # Update the VRRecord and constituents.
        record = VRRecord.objects.latest('datetime')
        sat_vals = SATValue.objects.filter(vrrecord=record).order_by('index')
        passed_vals = PassedValue.objects.filter(vrrecord=record).order_by(
            'index')
        for passed_val in passed_vals:
            if passed_val.value is None:
                passed_val.value = passed
                passed_val.save()

        # Update the user and verse histories following the answer.
        sat = [x.value for x in sat_vals]
        passed = [x.value for x in passed_vals]
        record = VRRecord.objects.latest('datetime')
        test_num = VRRecord.objects.filter(user=self.cur_user).count()
        with transaction.atomic():
            for i in range(len(sat)):
                strong_word = record.verse.word_set.get(number=i+1).strong_word
                user_word = UserWord.objects.filter(
                    user=self.cur_user).get(strong_word=strong_word)
                user_word.update_after_test(test_num, sat[i], passed[i])
                user_word.save()

            user_verse = UserVerse.objects.filter(user=self.cur_user).get(
                verse__path=self.vocab_cur_verse_path)
            user_verse.last_asked = test_num
            user_verse.save()
        self.go_to('base.vocab.vocab_ask')
                         
    def vocab_show_answer(self):
        '''Display the answer to the vocab question.'''
        self.go_to('base.vocab.vocab_answer')

    def vocab_word_label_pressed(self, widget, event):
        if not widget.collide_point(*event.pos):
            return
        prim_labels = [x[0] for x in self.word_labels]
        sec_labels = [x[1] for x in self.word_labels]
        try:
            ind = prim_labels.index(widget)
        except ValueError:
            ind = sec_labels.index(widget)
            
        record = VRRecord.objects.latest('datetime')
        sat_val = SATValue.objects.filter(vrrecord=record).get(index=ind)
        passed_val = PassedValue.objects.filter(vrrecord=record).get(index=ind)
        
        grk_label = self.word_labels[ind][0]
        eng_label = self.word_labels[ind][1]
        if sat_val.value == 'tell':
            if eng_label.color == HIDDEN_COLOR:
                eng_label.color = ENG_LABEL_COLOR
                grk_label.color = CORE_BASE_COLOR
            elif eng_label.color == ENG_LABEL_COLOR:
                eng_label.color = HIDDEN_COLOR
                grk_label.color = SPECIAL_PASSED_COLOR
        elif sat_val.value == 'show':
            if eng_label.color == HIDDEN_COLOR:
                eng_label.color = ENG_LABEL_COLOR
                grk_label.color = SPECIAL_FAIL_COLOR
            elif eng_label.color == ENG_LABEL_COLOR:
                eng_label.color = HIDDEN_COLOR
                grk_label.color = CORE_BASE_COLOR

        passed_val.value = not passed_val.value
        passed_val.save()
        return True
            
    def read_word_label_pressed(self, widget, event):
        if not widget.collide_point(*event.pos):
            return
        prim_labels = [x[0] for x in self.word_labels]
        sec_labels = [x[1] for x in self.word_labels]
        try:
            ind = prim_labels.index(widget)
        except ValueError:
            ind = sec_labels.index(widget)
        # Set the label colors appropriately and flip the passed
        # variable.
        grk_label = self.word_labels[ind][0]
        eng_label = self.word_labels[ind][1]
        if self.read_sat[ind] == "tell":
            if eng_label.color == HIDDEN_COLOR:
                eng_label.color = ENG_LABEL_COLOR
                grk_label.color = CORE_BASE_COLOR
            elif eng_label.color == ENG_LABEL_COLOR:
                eng_label.color = HIDDEN_COLOR
                grk_label.color = SPECIAL_PASSED_COLOR
        elif self.read_sat[ind] == "show":
            if eng_label.color == HIDDEN_COLOR:
                eng_label.color = ENG_LABEL_COLOR
                grk_label.color = SPECIAL_FAIL_COLOR
            elif eng_label.color == ENG_LABEL_COLOR:
                eng_label.color = HIDDEN_COLOR
                grk_label.color = CORE_BASE_COLOR
        self.read_passed[ind] = not self.read_passed[ind]
        return True

    def diagnostic_inquire(self):
        '''Ask the user about the next recommended diagnostic word.'''
        try:
            self.diag_chosen_word = self.diag_strat.next()
        except StopIteration:
            self.modify_user_objects(True)
        else:
            # Set the displayed text appropriately.
            tmp = self.diag_chosen_word.uninflected_word
            if self.diag_tsmt.prim_lang == 'grk':
                self.diagnostic_popup.ids.word_label.text = tmp
            elif self.diag_tsmt.prim_lang == 'heb':
                self.diagnostic_popup.ids.word_label.text = tmp[::-1]

    def diagnostic_update_plot(self):
        known_words = biblist.find_known_words(self.cur_user, self.diag_tsmt)
        avail_words = biblist.find_available_words(self.cur_user,
                                                   self.diag_tsmt)
        all_words = known_words | avail_words # join the querysets

        if len(known_words) >= 8:
            offset, steepness = biblist.curve_fit_tanh(known_words)
            num_plot_points = 100
            tmp = set([math.log(x.occs + 1) for x in all_words])
            max_x = max(tmp)
            min_x = min(tmp)
            x_vals = biblist.my_linspace(min_x, max_x, num_plot_points)
            y_vals = [biblist.tanh_func(x, offset, steepness) for x in x_vals]
            tanh_points = zip(x_vals, y_vals)
            known_points = []
            for word in known_words:
                latest_dr = word.diagrecord_set.latest('datetime')
                known_points.append((math.log(word.occs), latest_dr.knownness))

            known_points = sorted(known_points)

            # Clear the graph of existing plots.
            graph = self.diagnostic_popup.ids.graph
            graph.xmin = min_x
            graph.xmax = max_x
            for plot in graph.plots[:]:
                graph.remove_plot(plot)

            # Plot the tanh curve fit.
            plot = MeshLinePlot(color=[1, 0, 0, 1])
            plot.points = tanh_points
            graph.add_plot(plot)
            
            # Plot the known points.
            plot = MeshScatterPlot(color=[0, 1, 0, 1])
            plot.points = known_points
            graph.add_plot(plot)

    def modify_user_objects(self, diag_test_taken):
        self.status_message = 'This may take a minute. Please wait...'
        self.go_to('.'.join([self.cur_state, 'status']))
        Clock.schedule_once(lambda x: self.modify_user_objects_scheduled(
            diag_test_taken))

    def modify_user_objects_scheduled(self, diag_test_taken):
        if diag_test_taken:
            known_words = biblist.find_known_words(self.cur_user,
                                                   self.diag_tsmt)
            avail_words = biblist.find_available_words(self.cur_user,
                                                     self.diag_tsmt)
            offset, steepness = biblist.curve_fit_tanh(known_words)
            
        # Modify UserWords associated with this user.
        uwords = []
        all_words = StrongWord.objects.filter(testament=self.diag_tsmt)
        with transaction.atomic():
            completed_occs = set()
            for strong_word in all_words:
                num_occs = strong_word.occs
                if num_occs in completed_occs:
                    continue
                
                # Calculate the knownness for this word.
                if diag_test_taken:
                    drs = DiagRecord.objects.filter(
                        strong_word=strong_word).filter(
                            user=self.cur_user)
                    if drs:
                        knownness = drs.latest('datetime').knownness
                    else:
                        # Estimate the word's knownness using the curve fit.
                        x_val = math.log(num_occs + 1)
                        knownness = biblist.tanh_func(x_val, offset, steepness)
                else:
                    knownness = 0.0

                # Apply the knownness to the corresponding UserWords.
                UserWord.objects.filter(
                    user=self.cur_user, strong_word__occs=num_occs,
                    strong_word__testament=self.diag_tsmt).update(
                        knownness=knownness)
                completed_occs.add(num_occs)

        self.go_to(self.post_diagnostic_state)

    def keep_hebrew_char(self, character):
        if character in '/':
            return False
        code_point_value = ord(character)
        # code points 0591 through 05AF
        if 1425 <= code_point_value <= 1455:
            return False
        # code points 05C4, 05C5, and 05C7
        if code_point_value in [1476, 1477, 1479]:
            return False
        return True
            
    # Convenience properties.
    @property
    def vocab_cur_tsmt(self):
        name = self.pickled['curs'][self.cur_user.name]['vocab_cur_tsmt_name']
        return Testament.objects.get(name=name)

    @property
    def vocab_cur_verse_path(self):
        tmp = self.pickled['curs'][self.cur_user.name]
        return tmp[self.vocab_cur_tsmt.name]['vocab_cur_verse_path']

    @vocab_cur_verse_path.setter
    def vocab_cur_verse_path(self, value):
        tmp = self.pickled['curs'][self.cur_user.name]
        tmp[self.vocab_cur_tsmt.name]['vocab_cur_verse_path'] = value

    @property
    def read_cur_tsmt(self):
        name = self.pickled['curs'][self.cur_user.name]['read_cur_tsmt_name']
        return Testament.objects.get(name=name)

    @property
    def read_cur_verse_path(self):
        tmp = self.pickled['curs'][self.cur_user.name]
        return tmp[self.read_cur_tsmt.name]['read_cur_verse_path']

    @read_cur_verse_path.setter
    def read_cur_verse_path(self, value):
        tmp = self.pickled['curs'][self.cur_user.name]
        tmp[self.read_cur_tsmt.name]['read_cur_verse_path'] = value
