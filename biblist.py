import datetime
import time
import cPickle as pickle
import pdb
import re
import os
import sys
from math import atanh, exp, tanh, log
from xml.dom import minidom
from xml.etree import ElementTree

from django.db import transaction

# Import Django ORM stuff.
os.environ['DJANGO_SETTINGS_MODULE'] = 'django_proj.settings'
sys.path.append('')
from biblist_django.models import Testament, Book, Chapter, Verse,\
    EngVerse, StrongWord, Word, User, UserWord, UserVerse, VRRecord,\
    DiagRecord, SATValue, PassedValue

bp = pdb.set_trace

SHOW_THRESHOLD = .75
NUM_CANDIDATE_VERSES = 30

def diagnostic_emphasize_slope_strategy(user, test):
    '''Generate a recommended word for the diagnostic test's next iteration.

    Note that DiagRecord.objects will change in between yield
    iterations, resulting in different sets of available and known
    words.

    '''

    num_from_full_dist = 8
    num_per_fit = 4
    num_to_choose = 16
    num_chosen = 0

    # Take num_from_full_dist samples evenly from the full distribution.
    avail_words = find_available_words(user, test)
    known_words = find_known_words(user, test)
    tmp = set([log(x.occs) for x in avail_words])
    max_x = max(tmp)
    min_x = min(tmp)
    initial_samples_x = my_linspace(min_x, max_x, num_from_full_dist, False)
    for x in initial_samples_x:
        closest_dist = float('inf')
        closest_word = None
        for cur_word in avail_words:
            cur_x = log(cur_word.occs)
            cur_dist = abs(x - cur_x)
            if cur_dist < closest_dist:
                closest_word = cur_word
                closest_dist = cur_dist
        chosen = closest_word
        num_chosen += 1
        yield chosen
        avail_words = find_available_words(user, test)
        known_words = find_known_words(user, test)
        
    # Assuming the desirability of sampling an available x-value is
    # proportional to f'(x), where f(x) is the current best fit
    # tanh. Then if we want, say, 3 samples, one should be at x0, dead
    # center, and the other two should be equal distance from the
    # centermost one. But how far away from center should they be?
    # Let's suppose they should be such that the area under the curve
    # integrated from -inf to the chosen point is equal to the area
    # integrated from the chosen point to x0. In other words, the
    # points chosen should divide the areas under the curve
    # equally. But the areas under these curves are represented by the
    # tanh curve itself. So if we want 3 points, all we have to do is
    # find where f(x) = 0.25, 0.5, and 0.75.
    samples_y = my_linspace(0, 1, num_per_fit, False)
    while True:
        if len(avail_words) == 0:
            raise StopIteration
        if num_chosen > num_to_choose:
            # Stopping conditions are set at a fixed number. After
            # gathering some user data I should be able to
            # determine more sensible stopping conditions.
            raise StopIteration
        cur_off, cur_stp = curve_fit_tanh(known_words)
        samples_x = [tanh_func_inv(y, cur_off, cur_stp) for y in samples_y]
        for x in samples_x:
            closest_dist = float('inf')
            closest_item = None
            for cur_word in avail_words:
                cur_x = log(cur_word.occs)
                cur_dist = abs(x - cur_x)
                if cur_dist < closest_dist:
                    closest_item = cur_word
                    closest_dist = cur_dist
            chosen = closest_item
            num_chosen += 1
            yield chosen
            avail_words = find_available_words(user, test)
            known_words = find_known_words(user, test)

def find_available_words(user, test):
    '''Return words available for inquiry by the diagnostic test.'''
    return StrongWord.objects.exclude(diagrecord__user=user).filter(
        testament=test).filter(uninflected_word__isnull=False)
        
def find_known_words(user, test):
    '''Return words suitable for curve-fitting by the diagnostic test.

    "Known" in the function name means not that the user knows the word
    well, but that the application knows the user's level of knowledge.

    '''
    return StrongWord.objects.filter(diagrecord__user=user).filter(
        testament=test).filter(uninflected_word__isnull=False)

def curve_fit_tanh(words):
    '''Curve fit a tanh to the knownnesses of the given words.

    Use x as the log of the occurrences and y as the reported
    knownness. Vary the steepness and offset iteratively to minimize the
    error sum.

    '''
    words_to_fit = words.order_by('occs')

    # Add one to the occurrences. This prevents a log(0) producing -inf
    # since some words have zero occurrences.
    x1 = [log(x.occs + 1) for x in words_to_fit]
    y1 = []
    for word in words_to_fit:
        diag_records = word.diagrecord_set.order_by('datetime')
        # Negative indexing for querysets not yet supported in Django.
        latest_dr = diag_records[len(diag_records) - 1]
        y1.append(latest_dr.knownness)

    # Perform a coarse search over a range of offsets and steepnesses.
    x_range = max(x1) - min(x1)
    offset_range = (-0.3 * x_range, 1.1 * x_range)
    offsets = my_linspace(offset_range[0], offset_range[1], 20)
    steepness_range = (0.01, 10)
    steepnesses = my_logspace(steepness_range[0], steepness_range[1], 20)
    errs = curve_fit_tanh_errors_sq(x1, y1, offsets, steepnesses)
    cur_min = float('inf')
    for ii in range(len(errs)):
        for jj in range(len(errs[ii])):
            if errs[ii][jj] < cur_min:
                cur_min = errs[ii][jj]
                cur_min_inds = (ii, jj)
    best_offset = offsets[cur_min_inds[0]]
    best_steepness = steepnesses[cur_min_inds[1]]

    # Refine the search.
    o_interval = (offset_range[1] - offset_range[0]) / (len(offsets) - 1)
    o_start = best_offset - o_interval
    o_end = best_offset + o_interval
    offsets = my_linspace(o_start, o_end, 20)
    s_log_interval = ((log(steepness_range[1]) - log(steepness_range[0])) /
                      (len(steepnesses) - 1))
    s_start = exp(log(best_steepness) - s_log_interval)
    s_end = exp(log(best_steepness) + s_log_interval)
    steepnesses = my_logspace(s_start, s_end, 20)
    errs = curve_fit_tanh_errors_sq(x1, y1, offsets, steepnesses)
    cur_min = float('inf')
    for ii in range(len(errs)):
        for jj in range(len(errs[ii])):
            if errs[ii][jj] < cur_min:
                cur_min = errs[ii][jj]
                cur_min_inds = (ii, jj)
    # If the curve fit ended up on an edge, warn the programmer.
    if cur_min_inds[0] == 0:
        print 'WARNING: minimum offset chosen in diagnostic curve fit'
    elif cur_min_inds[0] == len(offsets) - 1:
        print 'WARNING: maximum offset chosen in diagnostic curve fit'
    if cur_min_inds[1] == 0:
        print 'WARNING: minimum steepness chosen in diagnostic curve fit'
    elif cur_min_inds[1] == len(steepnesses) - 1:
        print 'WARNING: maximum steepness chosen in diagnostic curve fit'
    return offsets[cur_min_inds[0]], steepnesses[cur_min_inds[1]]

def curve_fit_tanh_errors_sq(x, y, offsets, steepnesses):
    sums = []
    for ii in range(len(offsets)):
        off = offsets[ii]
        sums.append([])
        for jj in range(len(steepnesses)):
            stp = steepnesses[jj]
            cur_err_sq_sum = 0
            for kk in range(len(x)):
                err = y[kk] - tanh_func(x[kk], off, stp)
                cur_err_sq_sum += err ** 2
            sums[ii].append(cur_err_sq_sum)
    return sums

def my_linspace(start, end, num, include_end_points=True):
    '''Return num elements evenly spaced on the range from start to end.'''
    if include_end_points:
        interval = (end - start) / float(num - 1)
        return [interval * x + start for x in range(num)]
    else:
        interval = (end - start) / float(num + 1)
        return [interval * x + start for x in range(1, num + 1)]

def my_logspace(start, end, num):
    interval = (log(end) - log(start)) / (num - 1)
    return [exp(1) ** (interval * x + log(start)) for x in range(num)]

def tanh_func(x, offset, steepness):
    '''Hyperbolic tangent function of x, given offset and slope.

    Offset is point of highest slope and steepness is slope at said
    point. Function approaches 0 as x -> -inf and approaches 1 as
    x -> inf. All new user knowledge profiles are assumed to take this
    shape.'''
    return 0.5 * tanh(steepness * (x - offset)) + 0.5

def tanh_func_inv(y, offset, steepness):
    '''Inverse function of tanh_func.'''
    return atanh(2 * (y - 0.5)) / steepness + offset

class BadUsernameError(Exception):
    pass
    
class UsernameClashError(Exception):
    pass

class UserLimitError(Exception):
    pass

class DeleteLastUserError(Exception):
    pass

class NoSuitableTestWordError(Exception):
    pass

    
class AppBackend(object):
    def _validate_proposed_user_name(self, name):
        # Validate the new user's name wrt string validity and
        # uniqueness.

        # These conditions need to be lazily evaluated, otherwise the
        # ones assuming a non-zero length will raise an IndexError.
        conds = [lambda: isinstance(name, str) or isinstance(name, unicode),
                 lambda: len(name) > 0,
                 lambda: len(name) <= 20,
                 lambda: name[0] != ' ',
                 lambda: name[-1] != ' ',
                 lambda: all([x not in name for x in '\r\n	']), # <-- tab
                 lambda: name.lower() not in [x.name.lower() for x in
                                              User.objects.all()]]
        err_msgs = ['User name is not a string',
                    'User name has zero length',
                    'User name must be 20 characters or less',
                    'User name has an initial space',
                    'User name has a final space',
                    'User name has a forbidden character',
                    'User name already exists']
        err_types = [BadUsernameError,
                     BadUsernameError,
                     BadUsernameError,
                     BadUsernameError,
                     BadUsernameError,
                     BadUsernameError,
                     UsernameClashError]
                     
        applicable_err_msg = None
        for cond, err_msg, err_type in zip(conds, err_msgs, err_types):
            if not cond():
                raise err_type(err_msg)

    def add_user(self, name):
        '''Add a user with the given name.'''
        self._validate_proposed_user_name(name)
        # Identify a fake user to be replaced by the new user.
        fake_user_nums = []
        fake_users = User.objects.filter(name__contains='\n')
        if len(fake_users) > 0:
            for fake_user in fake_users:
                match = re.match('fake_user_([0-9])+\n', fake_user.name)
                fake_user_nums.append(int(match.group(1)))
            min_num = min(fake_user_nums)
        else:
            raise UserLimitError('There are too many users. Please delete one.')
        target_fake_user_name = 'fake_user_' + str(min_num) + '\n'
        target_fake_user = User.objects.get(name=target_fake_user_name)

        # Rename the target fake user.
        target_fake_user.name = name
        target_fake_user.save()

    def delete_user(self, name):
        try:
            user = User.objects.get(name=name)
        except User.DoesNotExist:
            raise BadUsernameError('User does not exist.')
        if len(User.objects.exclude(name__contains='\n')) == 1:
            tmp = 'Cannot delete last user. Please create another one first.'
            raise DeleteLastUserError(tmp)

        # Identify a free fake user name to replace the existing user
        # name.
        fake_user_nums = []
        fake_users = User.objects.filter(name__contains='\n')
        for fake_user in fake_users:
            match = re.match('fake_user_([0-9])+\n', fake_user.name)
            fake_user_nums.append(int(match.group(1)))
        ## Select the fake user name with the lowest number.
        missing_num = 0
        while True:
            if missing_num in fake_user_nums:
                missing_num += 1
            else:
                break
        
        target_fake_user_name = 'fake_user_' + str(missing_num) + '\n'

        # Rename the user to be deleted.
        user.name = target_fake_user_name
        user.save()

    def rename_user(self, existing_name, proposed_name):
        try:
            user = User.objects.get(name=existing_name)
        except User.DoesNotExist:
            raise BadUsernameError('User does not exist.')
        self._validate_proposed_user_name(proposed_name)
        user.name = proposed_name
        user.save()

    def vocab_set_next_verse(self, user, testament):
        tmp = self._find_suitable_word_and_verse(user, testament)
        test_word, test_verse, candidate_words = tmp

        # Create tuples for the pass/fail and display states of the
        # words. The valid display states are show, ask, and tell. Show
        # is a word displayed in the primary language only. Ask means a
        # word is an instance of the word under test. Tell is a word is
        # displayed with primary and secondary languages. For show, fail
        # means the user asked what the word was by clicking on it
        # (assume pass). For ask, fail means the user guessed the word
        # wrong (assume pass). For tell, a pass means the user clicked
        # on the word indicating she already knew it. Assume fail.
        with transaction.atomic():
            record = VRRecord.objects.create(user=user,
                                             datetime=datetime.datetime.now(),
                                             verse=test_verse.verse,
                                             module='vocab')
            word_set = test_verse.verse.word_set.order_by('index')
            for ii, bible_word in enumerate(word_set):
                uword = UserWord.objects.get(
                    user=user, strong_word=bible_word.strong_word)
                knownness = uword.knownness
                # If this is the test word, then assign it ask. Assume None,
                # which means that the question has not yet been answered.
                if bible_word.strong_word == test_word.strong_word:
                    sat_value = 'ask'
                    passed_value = None
                # If it's not the test word but is above show threshold, assign
                # it show. Assume pass.
                elif (knownness is not None) and knownness > SHOW_THRESHOLD:
                    sat_value = 'show'
                    passed_value = True
                # If it's neither, assign it tell. Assume fail.
                else:
                    sat_value = 'tell'
                    passed_value = False
                SATValue.objects.create(vrrecord=record, index=ii,
                                        value=sat_value)
                PassedValue.objects.create(vrrecord=record, index=ii,
                                           value=passed_value)
            
        print 'Test verse chosen was: ' + test_verse.verse.path

    def _find_suitable_word_and_verse(self, user, testament):
        cur_test_num = VRRecord.objects.filter(user=user,
                                               module='vocab').count()
        user_words = UserWord.objects.filter(user=user).filter(
            strong_word__testament=testament).exclude(is_blacklisted=True)
        
        # Update all user words.
        for uw in user_words:
            uw.update_learnability()
            uw.update_ripeness(cur_test_num)
            uw.update_suitability()
        sorted_user_words = sorted(user_words, key=lambda x: x.suitability,
                                   reverse=True)
        test_user_verse = None
        ind = 0
        while test_user_verse is None:
            try:
                test_user_word = sorted_user_words[ind]
            except IndexError:
                raise NoSuitableTestWordError, 'No suitable test word found.'
            max_verse_suitability = 0
            # Take only the NUM_CANDIDATE_VERSES most important verses,
            # since taking too many results in slowdown. For some reason
            # I have to turn this into a list right now. I assume
            # because it forces the queryset's evaluation.
            verses = list(test_user_word.strong_word.verses.order_by(
                '-importance')[:NUM_CANDIDATE_VERSES])
            
            user_verses = UserVerse.objects.filter(user=user).filter(
                testament=testament).filter(verse__in=verses).order_by(
                    '-verse__importance')
            with transaction.atomic():
                for user_verse in user_verses:
                    user_verse.update_ripeness(cur_test_num)
                    user_verse.update_suitability()
                    
            sorted_verses = sorted(user_verses, key=lambda x: x.suitability,
                                   reverse=True)
            # Ensure the test word has English associated with it in the
            # selected verse.
            for uv in sorted_verses:
                for word in uv.verse.word_set.all():
                    if word.strong_word == test_user_word.strong_word:
                        if word.eng_text:
                            if not word.eng_text == '-':
                                test_user_verse = uv
                if test_user_verse:
                    break
            if not test_user_verse:
                test_user_word.is_blacklisted = True
                # Since blacklisted words will never be tested, we set
                # their knownness to max.
                test_user_word.knownness = 1.0
                test_user_word.save()
            ind += 1
        return test_user_word, test_user_verse, sorted_user_words

    def get_read_sat(self, user, testament, test_verse_path):
        sat = []
        test_verse = testament.get_verse(test_verse_path)
        for word in test_verse.word_set.all():
            user_word = UserWord.objects.get(
                user=user, strong_word__number=word.strong_word.number)
            knownness = user_word.knownness
            # If it's above show threshold, assign it show. Assume pass.
            if (knownness is not None) and knownness > SHOW_THRESHOLD:
                sat.append('show')
            # Otherwise assign it tell. Assume fail.
            else:
                sat.append('tell')
        return sat
