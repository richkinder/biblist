# execfile('/home/rich/work/bible/ltrgh/bible_parser.py')
# TO DO
# -One or more ITDs are implemented with defaultdicts, which is fine for
#  their construction process, but they should be converted to regular
#  dicts before use, so that KeyErrors can be thrown.
# -Some of the below are old and refer to previous parser versions, esp. < 7
# -Word matcher currently handles multiple words found quite poorly. fix.
# -Look in dictionaries to determine if a word is a proper noun. If not,
#  lowercase the English.
# -Generate strong dict for hebrew, and find uninflectedword using BDB or
#  equivalent.
# -Take the 'usage' node from the BDB HebrewMesh, and use it as a fallback
#  word (shown in parentheses) if no English word is found.
# -Pericope Adulterae and verses 24:6-8 of Acts are located in their own .CCT
#  files (greek betacode). Make sure they are actually being used if needed,
#  because the BP5 files (greek parsed) are not separated out this way.
# -Ezra 1 has a lot of text elements that are mere new lines (as I assume all
#  OT elements of kjv_bible do). Fix.
# -Search for any occurrence of "fix", "diag", or "deleteme" and remove
#  diagnostic code when ready.
# -See if there's any use to be gotten out of strongMorph codes in kjvfull.
# -At 2Cor.13.13, kjvfull is lacking some English that could correspond to the
#  greek in the .TRP. Update: It is? I don't see what. Maybe I meant .CCT
#  instead?
# -Go through verses that are deemed bad and determine why.
# -kjvfull Luke.13.7 "cumbereth it" should be linked to src=25 and "the ground"
#  should be linked to srcs 23 and 24.
# -Remove execs and evals, especially in beta_to_uni_dict.
# -Remove code redundancy: most of the code in the tagged_sync sections (where
#  munkes is employed) is duplicated in grk and heb.
#
# Notes
# -Figure out how to handle when gnt to kjv verse mapping functions return two
#  verses instead of one.
# -Incorporate transChange from KJV Full so that it produces English words with
#  italics.
# -Gen 3:21 has a comma between two words. Am I handling this properly?
# -KJV does not fully match the Hebrew OT. See e.g. Mal.1.1, where KJV lacks
#  any word corresponding to strong_num 413 in the Hebrew. Actually this is not
#  due to any discrepancy between the two, simply that the KJV has no English
#  word to correspond to that Hebrew word.
# -At 2Cor.13.13, the .TRP seems to have an indication that this is referenced
#  elsewhere as 13:12 by including an extra verse reference with parentheses.
#  Same for 13:13, and for several places in Acts. Solution: examine each
#  occurrence of the parentheses, compare all three texts, and ensure that
#  removal of parentheses maintains line-to-line correspondence in tagged/check
#  /text. See discrepancy_fixes.
#
# Parsing Notes
# *Current plan is to link KJVfull to .TRP, then .TRP to .BP5, and finally .BP5
#  to .CCT.
# -kjvfull is supposed to be linked to .TRP files.
# -1CO.BP5 2:13 has: pneumatikoiv 4152 {A-DPN} {A-DPM}. I must handle two
#  tags in a row.
# -Variants (indicated by |) cannot be removed from TRP because it desyncs the
#  src numbering from KJVfull. Also the src numbering in KJVfull seems to be
#  linked to grammar tags and not [a-z]* matches. Also, note that if a variant
#  such as "swthrov 4990 {N-GSM} | | hmwn 2257 {P-1GP} | ihsou 2424  {N-GSM}"
#  appears, as in 2PE.TRP, then since the first variant is blank, the src
#  numbering is N for swthrov and N + 1 for hmwn, NOT N + 2.
# -Multiple KJV words that match a single TRP word. E.g. 1 Cor. 1:29
#  with enwpion. The tag 'type="x-split"' refers to this split.
# -KJVfull TURNS ONE WORD WITH TWO TAGS INTO 2 src NUMBERs
#  2PE.TRP 2:19 has w 3739 {R-DSM} 3739 {R-DSN}, i.e. two strongs with tags.
#  These turn into src 9 and 10. src 10 has no text while 9 does.
# -KJVfull SPLITS ONE TRP WORD INTO TWO src NUMBERS.
#  1 Cor 10:8. From TRP to KJV, eikositreiv 1501 5140 {A-NPM} turns into
#  <src=13, strng=5140 morph=A-NPM text=three> and <src=12, strng=1501, no
#  morph tag, text=and twenty> No variants involved. In this verse other
#  words have two strong nums, but are counted as only one word element. 
# -KJVfull SEES ONLY ONE VARIANT:
#  1PE.TRP 1:4 has hman 2248 {P-1AP} umav 5209 {P-2AP}, but KJVfull does not
#  handle the former (note that it is a variant in 1PE.TRP raw), but takes the
#  latter variant only. Likewise in 3:21. It looks like most of the time the
#  KJV takes the latter variant, but not always. See 2Cor.3.1, where the first
#  variant is chosen.
#  Also 1CO.TRP 5:7 seems to have 22 words while kjvfull has 21 words (and by
#  word I mean grammar tags, which is what the src numbers in kjvfull refer to,
#  see previous note about 2Pet.2.19). The missing word is etuyh 2380 5681
#  {V-API-3S) or its variant which is identical except it is spelled eyuyh.
#  Probably only one of the variants is counted, and the other is left behind.
#  How should I determine which one is used? I guess just by matching tags in
#  kjv.
# -KJVfull SEES BOTH VARIANTS:
#  2PE.TRP 1:1 does see variant B (simwn), and marks it with a src number but
#  assigns it no word. How am I supposed to know whether it is going to see the
#  variant or not? I suppose I'll have to check.
# -KJVfull HAS NO CORRESPONDENT
#  kjvfull 1Pet.3.21 has <w src="1" lemma="3739">whereunto</w> even, i.e. the
#  word "even" has no correspondent source word.
# -2CO.TRP 7:16 has an empty variant: 7:16 cairw 5463 5719 {V-PAI-1S} | | oun
#  3767 {CONJ} | oti 3754 {CONJ} en 1722. See also 2Pet.1.1
# -KJVFULL HAS TWO SRCS, ONE STRONG'S, ONE TAG: Matt.16:17, "Bar-jona"
# -KJVFULL HAS TWO SRCS, ONE STRONG'S, TWO TAGS: Luke 12:13, "speak"
# -TRP HAS THREE STRONG'S, ONE TAG: Gal 5:7
# -KJVFULL HAS NO SOURCE.
# -KJVFULL Luke 12:13 is generally messed up. Note that src="1 9" "speak"
#  matches eipen at the beginning of the verse and eipe later on although they
#  are distinctly different.

# deleteme stats: books, chaps, verses, words = (27, 260, 7957, 135074)
#           chaps_per_book, verses_per_chap, words_per_verse = 9.7, 30.6, 17.0

# Profiling:
# import cProfile
# p = cProfile.Profile()
# p.run('execfile('/home/rich/work/bible/ltrgh/bible_parser.py')')
# p.dump_stats('bible_parser.prof')

#
# Some Bible Tree stats:
# Hebrew OT
# number_of_books: 39
# number_of_chaps: 929
# number_of_verses: 23213
# number_of_words: 305497
# chaps_per_book: 23.8
# verses_per_chap: 24.98
# words_per_verse: 13.16
# number_of_unique_word_forms: 111173
#
# Greek NT
# number_of_books: 27
# number_of_chaps: 260
# number_of_verses: 7957
# number_of_words: 139904
# chaps_per_book: 9.63
# verses_per_chap: 30.60
# words_per_verse: 17.58

from xml.etree import ElementTree
from xml.dom import minidom
import shutil
import sys
import os
from os.path import join
from math import sqrt, atan, sin
import unicodedata
import time
import cPickle as pickle
import re
import itertools
from copy import copy
from pprint import PrettyPrinter
from collections import defaultdict
import numpy as np
from munkres import Munkres
from pdb import set_trace as bp
from math import log
#from IPython.core.debugger import Tracer
#bp = Tracer()
#diag. This is only necessary if I am using C-c C-c to run this python script from buffer.
OUTPUT_FILE_NAME_FULL = 'biblist_bible_full.pickle'
OUTPUT_FILE_NAME_LIGHT = 'biblist_bible_light.pickle'

# Display names of Old and New Testaments.
TESTAMENT_DISP_NAME = {'ot': 'Old Testament (beta)', 'nt': 'New Testament'}

# OT book names to be shown to the user, in the order found in the Hebrew OT.
OT_HEB_FULL_BOOK_NAMES = ('Genesis', 'Exodus', 'Leviticus', 'Numbers',
                          'Deuteronomy', 'Joshua', 'Judges', '1 Samuel',
                          '2 Samuel', '1 Kings', '2 Kings', 'Isaiah',
                          'Jeremiah', 'Ezekiel', 'Hosea', 'Joel', 'Amos',
                          'Obadiah','Jonah', 'Micah', 'Nahum', 'Habakkuk',
                          'Zephaniah', 'Haggai', 'Zechariah', 'Malachi',
                          'Psalms', 'Job', 'Proverbs', 'Ruth', 'Song of Songs',
                          'Ecclesiastes', 'Lamentations', 'Esther', 'Daniel',
                          'Ezra', 'Nehemiah', '1 Chronicles', '2 Chronicles')

# OT book names to be shown to the user, in the order found in the English OT.
OT_ENG_FULL_BOOK_NAMES = ('Genesis', 'Exodus', 'Leviticus', 'Numbers',
                          'Deuteronomy', 'Joshua', 'Judges', 'Ruth',
                          '1 Samuel', '2 Samuel', '1 Kings', '2 Kings',
                          '1 Chronicles', '2 Chronicles', 'Ezra', 'Nehemiah',
                          'Esther', 'Job', 'Psalms', 'Proverbs',
                          'Ecclesiastes', 'Song of Songs', 'Isaiah',
                          'Jeremiah', 'Song of Songs', 'Lamentations',
                          'Ezekiel', 'Hosea', 'Joel', 'Amos', 'Obadiah',
                          'Jonah', 'Micah', 'Nahum', 'Habakkuk', 'Zephaniah',
                          'Haggai', 'Zechariah', 'Malachi')

# OSIS abbreviations in the order found in the Hebrew OT.
OT_HEB_OSIS_ABBREVS = ('Gen', 'Exod', 'Lev', 'Num', 'Deut', 'Josh', 'Judg',
                       '1Sam', '2Sam', '1Kgs', '2Kgs', 'Isa', 'Jer', 'Ezek',
                       'Hos', 'Joel', 'Amos', 'Obad','Jonah', 'Mic', 'Nah',
                       'Hab', 'Zeph', 'Hag', 'Zech', 'Mal', 'Ps', 'Job',
                       'Prov', 'Ruth', 'Song', 'Eccl', 'Lam', 'Esth', 'Dan',
                       'Ezra', 'Neh', '1Chr', '2Chr')

# OSIS abbreviations in the order found in the English OT.
OT_ENG_OSIS_ABBREVS = ('Gen', 'Exod', 'Lev', 'Num', 'Deut', 'Josh', 'Judg',
                       'Ruth', '1Sam', '2Sam', '1Kgs', '2Kgs', '1Chr', '2Chr',
                       'Ezra', 'Neh', 'Esth', 'Job', 'Ps', 'Prov', 'Eccl',
                       'Song', 'Isa', 'Jer', 'Lam', 'Ezek', 'Dan', 'Hos',
                       'Joel', 'Amos', 'Obad', 'Jonah', 'Mic', 'Nah', 'Hab',
                       'Zeph', 'Hag', 'Zech', 'Mal')

# Abbreviations of files downloaded from byztxt.com, e.g. MT.CCT.
NT_BYZTXT_ABBREVS = ('MT', 'MR', 'LU', 'JOH', 'AC', 'RO', '1CO', '2CO', 'GA',
                   'EPH', 'PHP', 'COL', '1TH', '2TH', '1TI', '2TI', 'TIT',
                   'PHM', 'HEB', 'JAS', '1PE', '2PE', '1JO', '2JO', '3JO',
                   'JUDE', 'RE', '3TE', '4TE')

# OSIS abbreviations in the NT.
NT_OSIS_ABBREVS = ('Matt', 'Mark', 'Luke', 'John', 'Acts', 'Rom', '1Cor',
                   '2Cor', 'Gal', 'Eph', 'Phil', 'Col', '1Thess', '2Thess',
                   '1Tim', '2Tim', 'Titus', 'Phlm', 'Heb', 'Jas', '1Pet',
                   '2Pet', '1John', '2John', '3John', 'Jude', 'Rev', '3Test',
                   '4Test')

# NT book names to be displayed to the user.
NT_FULL_BOOK_NAMES = ('Matthew', 'Mark', 'Luke', 'John', 'Acts', 'Romans',
                      '1 Corinthians', '2 Corinthians', 'Galatians',
                      'Ephesians', 'Phillipians', 'Colossians',
                      '1 Thessalonians', '2 Thessalonians', '1 Timothy',
                      '2 Timothy', 'Titus', 'Philemon', 'Hebrews', 'James',
                      '1 Peter', '2 Peter', '1 John', '2 John', '3 John',
                      'Jude', 'Revelation', '3Test', '4Test')

# Greek Parsing Dictionary. Taken from the Tischendorf edited by Ulrik
# Sandborg-Peterson.
grk_pos_dict = {'N': 'Noun', 'A': 'Adjective', 'T': 'Article', 'V': 'Verb',
                'P': 'Personal Pronoun', 'R': 'Relative Pronoun',
                'C': 'Reciprocal Pronoun', 'D': 'Demonstrative Pronoun',
                'K': 'Correlative Pronoun', 'I': 'Interrogative Pronoun',
                'X': 'Indefinite Pronoun',
                'Q': 'Correlative or Interrogative Pronoun',
                'F': 'Reflexive Pronoun', 'S': 'Possessive Pronoun',
                'ADV': 'Adverb', 'CONJ': 'Conjunction', 'COND': 'Conditional',
                'PRT': 'Particle', 'PREP': 'Preposition',
                'INJ': 'Interjection', 'ARAM': 'Aramaic', 'HEB': 'Hebrew',
                'N-PRI': 'Proper Noun Indeclinable',
                'A-NUI': 'Numeral Indeclinable', 'N-LI': 'Letter Indeclinable',
                'N-OI': 'Noun Other Type Indeclinable'}
grk_case_dict = {'N': 'Nominative', 'G': 'Genitive', 'D': 'Dative',
                 'A': 'Accusative', 'V': 'Vocative'}
grk_num_dict = {'S': 'Singular', 'P': 'Plural'}
grk_gender_dict = {'M': 'Masculine', 'F': 'Feminine', 'N': 'Neuter'}
grk_tense_dict = {'P': 'Present', 'I': 'Imperfect', 'F': 'Future',
                  '2F': 'Second Future', 'A': 'Aorist', '2A': 'Second Aorist',
                  'R': 'Perfect', '2R': 'Second Perfect', 'L': 'Pluperfect',
                  '2L': 'Second Pluperfect', 'X': 'No Tense Stated'}
grk_voice_dict = {'A': 'Active', 'M': 'Middle', 'P': 'Passive',
                  'E': 'Middle or Passive', 'D': 'Middle Deponent',
                  'O': 'Passive Deponent', 'N': 'Middle or Passive Deponent',
                  'Q': 'Impersonal Active', 'X': 'No Voice'}
grk_mood_dict = {'I': 'Indicative', 'S': 'Subjunctive', 'O': 'Optative',
                 'M': 'Imperative', 'N': 'Infinitive', 'P': 'Participle',
                 'R': 'Imperative Participle'}
grk_person_dict = {'1': 'First Person', '2': 'Second Person',
                   '3': 'Third Person'}
grk_verb_extra_dict = {'M': 'Middle Significance', 'C': 'Contracted Form',
                       'T': 'Transitive', 'A': 'Aeolic', 'ATT': 'Attic',
                       'AP': 'Apocopated', 'IRR': 'Irregular or Impure Form'}
grk_suffix_dict = {'S': 'Superlative', 'C': 'Comparative',
                   'ABB': 'Abbreviated', 'I': 'Interrogative', 'N': 'Negative',
                   'ATT': 'Attic', 'P': 'Particle Attached', 'K': 'Crasis'}
GRK_PARSED_DICT = [grk_pos_dict, grk_case_dict, grk_num_dict, grk_gender_dict,
                   grk_tense_dict, grk_voice_dict, grk_mood_dict,
                   grk_person_dict, grk_verb_extra_dict, grk_suffix_dict]

def apply_case(word):
    '''Leave the English text case as-is.
    
    Previously I tried to apply case differently depending on whether
    the word was a proper noun. I realized that it's difficult since
    there are many instances of "o ihsous", where the english word for
    "o" is Jesus, as is the english word for "ihsous".

    '''
    return word.eng_word.text

def parse_info(parsetext, parsed_dict):
    # Parse Info had trouble parsing the following tags:
    # S-1DPF, S-2DPM, S-2DPF, S-1APN, S-1GSF, S-2GPF, S-1DSM, S-1DSN, S-1APM,
    # S-1DSF, S-2APF, S-2NPF, S-2ASF, S-2NPN, S-2ASN, S-2NPM, S-2DSM, S-1NPF,
    # S-1NPN, S-1NPM, S-2DSF, S-1GPF, S-2APM, S-1DPN, S-1GPN, S-1DPM, S-1ASF,
    # S-1APF, S-1ASM, S-2GSF, S-1ASN, S-1NSN, S-1NSM, S-2NSN, S-2NSM, S-1NSF,
    # S-2APN
    # Was this ever fixed? fix.
    try:
        parse_info = []
        [posdict, casedict, numdict, genderdict, tensedict, voicedict,
         mooddict, persondict, verbextradict, suffixdict] = parsed_dict
        # Find part-of-speech identifier.
        if parsetext[1] == '-':
            if parsetext == 'N-PRI':
                pos = 'N-PRI'
            elif parsetext == 'A-NUI':
                pos = 'A-NUI'
            elif parsetext == 'N-LI':
                pos = 'N-LI'
            elif parsetext == 'N-OI':
                pos = 'N-OI'
            else:
                pos = parsetext[0]
        else:
            pos = parsetext.split('-')[0]
        parse_info.append(posdict[pos])
        # Based on part-of-speech identifier find parsing info to display.
        if pos in 'NAT':
            case = casedict[parsetext[2]]
            num = numdict[parsetext[3]]
            gender = genderdict[parsetext[4]]
            parse_info.extend([case, num, gender])
        elif pos in 'V':
            if len(parsetext.split('-')[1]) == 4:
                # If the tense indicator has two letters (e.g. 2A for
                # aorist), set the tense to those two letters but adjust
                # parsetext because the rest of the code expects a single
                # letter tense.
                tense = tensedict[parsetext[2:4]]
                parsetext = parsetext[0:2] + 'x' + parsetext[4:]
            else:
                tense = tensedict[parsetext[2]]
            voice = voicedict[parsetext[3]]
            mood = mooddict[parsetext[4]]
            if parsetext[4] in 'ISOM':
                person = persondict[parsetext[6]]
                num = numdict[parsetext[7]]
                parse_info.extend([tense, voice, mood, person, num])
                if len(parsetext) > 8:
                    verbextra = verbextradict[parsetext[9:]]
                    parse_info.append(verbextra)
            elif parsetext[4] in 'N':
                parse_info.extend([tense, voice, mood])
            elif parsetext[4] in 'PR':
                case = casedict[parsetext[6]]
                num = numdict[parsetext[7]]
                gender = genderdict[parsetext[8]]
                parse_info.extend([tense, voice, mood, case, num, gender])
                if len(parsetext) > 9:
                    verbextra = verbextradict[parsetext[10:]]
                    parse_info.append(verbextra)
        elif pos in 'PRCDKIXQF':
            cur_ind = 2
            if parsetext[cur_ind] in '123':
                parse_info.append(persondict[parsetext[cur_ind]])
                cur_ind += 1
            parse_info.append(casedict[parsetext[cur_ind]])
            cur_ind += 1
            parse_info.append(numdict[parsetext[cur_ind]])
            cur_ind += 1
            if len(parsetext) > cur_ind:
                if parsetext[cur_ind] in 'MFN':
                    parse_info.append(genderdict[parsetext[cur_ind]])
                    cur_ind += 1
                    if len(parsetext) > cur_ind:
                        parse_info.append(suffixdict[parsetext[cur_ind + 1:]])
                else:
                    parse_info.append(suffixdict[parsetext[cur_ind + 1:]])
        elif pos in 'S':
            if len(parsetext) == 5:
                # This occurs in .TRP and .BP5 files.
                person_of_possessor = persondict[parsetext[2]]
                num_of_possessor = numdict[parsetext[3]]
                case_of_possessed = casedict[parsetext[4]]
                num_of_possessed = numdict[parsetext[5]]
                gender_of_possessed = genderdict[parsetext[6]]
                parse_info.extend([person_of_possessor, num_of_possessor,
                                   case_of_possessed, num_of_possessed,
                                   gender_of_possessed])
            else:
                # This occurs in Tischendorf 2.6.
                person_of_possessor = persondict[parsetext[2]]
                case_of_possessed = casedict[parsetext[3]]
                num_of_possessed = numdict[parsetext[4]]
                gender_of_possessed = genderdict[parsetext[5]]
                parse_info.extend([person_of_possessor, case_of_possessed,
                                   num_of_possessed,
                                   gender_of_possessed])
        elif pos[1] in '-':
            parsetext_split = parsetext.split('-')
            if len(parsetext_split) > 2:
                parse_info.append(suffixdict[parsetext_split[2]])
        else:
            parsetext_split = parsetext.split('-')
            if len(parsetext_split) > 1:
                parse_info.append(suffixdict[parsetext_split[1]])
    except:
        print 'Could not parse ' + parsetext
        return None
    return parse_info
    
    
class IterableTreeDict(object):
    '''Dictionary-like type that simplifies the syntax of deep access.

    Features:
    - Successive key access can be performed with '.' separators, as in
      this_itd['grk_nt.Matt.1.1.1'], which is equivalent to
      this_itd['grk_nt']['Matt']['1']['1']['1'].
    - Preserves order of key additions in a list (like OrderedDict)
    - Has a repr that displays a few levels deep
    - When unlocked (the default), it acts like defaultdict when a key
      is not found, by instantiating itself. When locked, it returns to
      the normal dict behavior of throwing KeyErrors. Can be locked and
      unlocked recursively.

    '''
    def __init__(self):
        self._child_list = []
        self._child_dict = {}
        self.is_locked = False

    def keys(self):
        return self._child_dict.keys()

    def values(self):
        return self._child_dict.values()

    def __iter__(self):
        return self._child_list.__iter__()

    def __len__(self):
        return len(self._child_list)

    def __getitem__(self, key):
        if isinstance(key, int):
            return self._child_list[key]
        elif '.' in key:
            boundary_ind = key.index('.')
            first = key[:boundary_ind]
            rest = key[boundary_ind + 1:]
            return self.__getitem__(first)[rest]
        elif key in self._child_dict or self.is_locked:
            return self._child_dict[key]
        else:
            tmp_dict = IterableTreeDict()
            self._child_dict[key] = tmp_dict
            self._child_list.append(tmp_dict)
            return tmp_dict

    def __setitem__(self, key, value):
        if isinstance(key, int):
            for dict_key in self._child_dict:
                if self._child_dict[dict_key] is self._child_list[key]:
                    self._child_list[key] = value
                    self._child_dict[dict_key] = value
        elif '.' in key:
            boundary_ind = key.index('.')
            first = key[:boundary_ind]
            rest = key[boundary_ind + 1:]
            self.__getitem__(first).__setitem__(rest, value)            
        elif key in self._child_dict:
            for list_key in range(len(self._child_list)):
                if self._child_dict[key] is self._child_list[list_key]:
                    self._child_list[list_key] = value
                    self._child_dict[key] = value
        else:
            self._child_dict[key] = value
            self._child_list.append(value)

    def __delitem__(self, key):
        del self._child_list[key]

    def __contains__(self, item):
        if isinstance(item, str):
            if ('.' in item):
                boundary_ind = item.index('.')
                first = item[:boundary_ind]
                rest = item[boundary_ind + 1:]
                if first in self._child_dict:
                    return rest in self.__getitem__(first)
                else:
                    return False
            else:
                return item in self._child_dict
        else:
            return item in self._child_dict

    def __repr__(self):
        printables = []
        for ii in range(len(self._child_list)):
            for key in self._child_dict.keys():
                if self._child_dict[key] == self._child_list[ii]:
                    printables.append(key)
                    break
        print_string = ('<IterableTreeDict object with ' + str(len(self)) +
                        ' keys>\n' + ', '.join(printables) + '\n')
        attribs_to_suppress = ['child_list', 'child_dict', 'keys',
                               'values', 'lang', 'name', 'parent',
                               'matrix', 'columns', 'lock', 'unlock',
                               'lock_recursively', 'unlock_recursively',
                               'is_locked', 'index_to_key', 'key_to_index']
        attribs_to_suppress_if_long = ['strong_words', 'strong_num_paths',
                                       'strong_num_verses']
        for attr in attribs_to_suppress_if_long:
            if attr in dir(self):
                if len(getattr(self, attr)) > 10:
                    attribs_to_suppress.append('strong_nums')
                    attribs_to_suppress.append('strong_num_paths')
        lines = []
        inds = []
        for attr in dir(self):
            if (attr[0] != '_') & (attr not in attribs_to_suppress):
                this_repr = repr(getattr(self, attr))
                if len(this_repr) > 100:
                    shortened_repr = this_repr[:100] + '...'
                else:
                    shortened_repr = this_repr
                lines.append(attr + ': ' + shortened_repr)
                inds.append(lines[-1].index(':'))
        for line, ind in zip(lines, inds):
            print_string += ' ' * (max(inds) - ind) + line + '\n'
        return print_string

    def key_to_index(self, key):
        '''Given a key, return the corresponding index.

        For example, my_bible.key_to_index('Luke') would return 2.

        '''
        value = self._child_dict[key]
        for list_key in range(len(self._child_list)):
            if self._child_list[list_key] is value:
                return list_key
        
    def index_to_key(self, index):
        '''Given an index, return the corresponding key.

        For example, my_bible.index_to_key(0) would return 'Matt'.

        '''
        value = self._child_list[index]
        for dict_key in self._child_dict:
            if self._child_dict[dict_key] is value:
                return dict_key
                
    def lock(self):
        self.is_locked = True

    def unlock(self):
        self.is_locked = False

    def lock_recursively(self):
        self.lock()
        for child in self._child_list:
            if isinstance(child, IterableTreeDict):
                child.lock_recursively()

    def unlock_recursively(self):
        self.unlock()
        for child in self._child_list:
            if isinstance(child, IterableTreeDict):
                child.unlock_recursively()


class BibleUnit(IterableTreeDict):
    def __init__(self, parent, name):
        super(BibleUnit, self).__init__()
        self.parent = parent
        self.name = name
        try:
            self.path = self.parent.path + '.' + name
            self.lang = parent.lang
        except AttributeError:
            self.path = name
            self.lang = name


class LangUnit(BibleUnit):
    def __init__(self, parent, name):
        super(LangUnit, self).__init__(parent, name)
    def lower(self, text):
        # Return lowercase form of text.
        lowered_text = []
        for char in text:
            if self.is_capital_dict[char]:
                lowered_text.append(self.swapcase_dict[char])
            else:
                lowered_text.append(char)
        return ''.join(lowered_text)

    def upper(self, text):
        # Return uppercase form of text.
        uppered_text = []
        for char in text:
            if self.is_capital_dict[char] is False:
                uppered_text.append(self.swapcase_dict[char])
            else:
                uppered_text.append(char)
        return ''.join(uppered_text)

    def depunctuate(self, text):
        # Return depunctuated form of text.
        depunctuated_text = []
        for char in text:
            if self.is_capital_dict[char] is None:
                pass
            else:
                depunctuated_text.append(char)
        return ''.join(depunctuated_text)


class Testament(BibleUnit):
    def __init__(self, parent, name):
        super(Testament, self).__init__(parent, name)


class Book(BibleUnit):
    def __init__(self, parent, name):
        super(Book, self).__init__(parent, name)


class Chapter(BibleUnit):
    def __init__(self, parent, name):
        super(Chapter, self).__init__(parent, name)


class Verse(BibleUnit):
    def __init__(self, parent, name):
        super(Verse, self).__init__(parent, name)
        self.is_bad = False
        self.text = ''
        # Fix. Put next and prev methods here, so that a verse can find the
        # verse that precedes and follows it. Matt.1.1.prev() should return
        # None and Mark.1.1.prev() should return Matt.28.20.
        
class BibleWord(BibleUnit):
    '''Contains textual data and methods primarily for debugging.

    Not to be confused with UserWord, which contains data and methods that are
    user-specific and relate to user knowledge of the word.'''
    def __init__(self, parent, name):
        super(BibleWord, self).__init__(parent, name)

    def __repr__(self):
        # Initial newline makes it look natural when words are shown in a list.
        print_string = '\n' + ' ' * 4 + 'Path: ' + self.path + '\n'
        if self.text:
            print_string += ' ' * 4 + 'Text: ' + self.text + '\n'
        else:
            print_string += ' ' * 4 + 'Text: *None*\n'

        counter = 0
        if hasattr(self, 'tags'):
            for item in self.tags:
                counter += 1
                print_string += ' ' * 3 + 'Tag ' + str(counter) + ': '
                print_string += item + '\n'
            if not self.tags:
                print_string += ' ' * 3 + 'Tag 1: ' + '*None*'

        counter = 0
        if hasattr(self, 'strong_words'):
            for item in self.strong_words:
                count += 1
                print_string += 'Strong ' + str(counter) + ': '
                print_string += item.lang + ' ' + str(item.strong_num) + '\n'
            if not self.strong_words:
                print_string += 'Strong 1: ' + '*None*\n'
        elif hasattr(self, 'strong_nums'):
            if self.strong_nums:
                print_string += ' Strong Nums: ' + str(self.strong_nums)
            else:
                print_string += ' Strong Nums: *None*\n'

        if 'eng_word' in dir(self):
            if self.eng_word:
                print_string += (' ' * 5 + 'Eng: ' + self.eng_word.text + ' '
                                 + '\n')

        return print_string.encode('utf-8')

class VerseMap(object):
    # Callable object which takes in either Hebrew or English Verses as
    # arguments and returns the target verse reference in English or Hebrew.
    def __init__(self, vm_src_file):
        vm_parsed = minidom.parse(vm_src_file)
        tmp = vm_parsed.getElementsByTagName('verseMap')[0]
        vm_booklist = tmp.getElementsByTagName('book')
        # A mapping from kjv to wlc and vice versa.
        mapping = {}
        for vm_book in vm_booklist:
            vm_verselist = vm_book.getElementsByTagName('verse')
            for vm_verse in vm_verselist:
                tmp_wlc = vm_verse.getAttribute('wlc')
                tmp_kjv = vm_verse.getAttribute('kjv')
                tmp_type = vm_verse.getAttribute('type')
                mapping['eng.ot.' + tmp_kjv] = {'verse': 'ot.' + tmp_wlc,
                                                'type': tmp_type}
                mapping['heb.ot.' + tmp_wlc] = {'verse': 'ot.' + tmp_kjv,
                                                'type': tmp_type}
        self.mapping = mapping
    
    def __call__(self, verse):
        if verse.path in self.mapping:
            return (self.mapping[verse.path]['verse'],
                    self.mapping[verse.path]['type'])
        else:
            if verse.lang == 'heb':
                return (verse.path.replace('heb.', ''), 'original')
            elif verse.lang == 'eng':
                return (verse.path.replace('eng.', ''), 'original')

        
class UninflectedDict(object):
    # Callable object which maps Words or Strong's Numbers to their
    # corresponding uninflected Hebrew forms.
    def __init__(self, uninf_src_file):
        uninf_src_parsed = ElementTree.parse(uninf_src_file)
        mapping = {}
        for entry in uninf_src_parsed.getroot().getchildren():
            tmp = entry.find('{http://www.APTBibleTools.com/namespace}usage')
            if tmp is not None:
                strong_num = entry.attrib['id']
                bdb = entry.find('{http://www.APTBibleTools.com/namespace}bdb')
                word = bdb.find('{http://www.APTBibleTools.com/namespace}w')
                mapping[strong_num] = word.text
        self._mapping = mapping
    
    def __getitem__(self, strong_num):
        return self._mapping[strong_num]
    
    def __contains__(self, strong_num):
        return strong_num in self._mapping

        
class Struct(object):
    # Class for arbitrary data storage. I could have just used dictionaries
    # but I like this repr, and I much prefer to type and read
    # this_verse.eng_verse.text than this_verse['eng_verse']['text'].
    # Besides, it is dictsp internally anyway.
    def __repr__(self):
        attribs = []
        values = []
        for attrib in dir(self):
            if attrib[0] != '_':
                attribs.append(attrib)
                values.append(getattr(self, attrib))
        if not attribs:
            return 'no attribs yet'
        max_length = max([len(x) for x in attribs])
        prnt = '\n'
        for (attrib, value) in zip(attribs, values):
            prnt += ' ' * (max_length - len(attrib) + 1) + attrib + ': '
            prnt += repr(value) + '\n'
        return prnt

class BibleParser(object):
    #Initialize variables of Bible Parser.
    #Pre-process the raw source files into files that are easy to read and
    #debug, cleaning out stuff I don't need.
    def __init__(self, output_full, output_light, wrk_dir='working_texts',
                 src_dir='source_texts', testing=None, refresh_working=False,
                 mini=False):
        self.mini = mini
        self.testing = testing
        self.refresh_working = refresh_working
        if testing:
            print 'Performing test run'
            wrk_dir = 'test_texts'
            test_results_file_name = 'test_texts/results'
        else:
            print 'Performing normal run'
        if refresh_working:
            'Refreshing working files'
        self.src_dirs = {}
        self.wrk_dirs = {}
        for lang in ['eng', 'heb', 'grk']:
            self.src_dirs[lang] = join(src_dir, lang)
            self.wrk_dirs[lang] = join(wrk_dir, lang)
            
        # Create Polyglot Bible
        self.bible = IterableTreeDict()
        self.bible.name = ''
        
        # Create English KJV.
        print 'Generating EnglishSource object'
        eng_src = EnglishSource(self)
        self.bible['eng'] = eng_src.create_language_unit(self.bible)
        
        # Create Hebrew OT.
        print 'Generating HebrewSource object'
        heb_src = HebrewSource(self)
        self.bible['heb'] = heb_src.create_language_unit(self.bible)
        
        # Create Greek NT.
        print 'Generating GreekSource object'
        grk_src = GreekSource(self)
        self.bible['grk'] = grk_src.create_language_unit(self.bible)
        
        # If testing, validate the results.
        if testing:
            with open(test_results_file_name, 'w') as fid:
                fid.write(self.bible['grk_nt'].test_results)
        
        # Pickle full data structure to examine for debugging purposes.
        print 'Pickling full Bible using pickle protocol 2.'
        t = time.time()
        with open(output_full, 'w') as pickle_file:
            pickle.dump(self.bible, pickle_file, 2)
        print 'Bible full pickled in: '
        print str(time.time() - t) + ' seconds'

        # Pickle lightweight version of data structure for LTRGH actual use.
        tree = IterableTreeDict()
        eng_verse_texts = {}
        for pair in [('heb', 'ot'), ('grk', 'nt')]:
        #for pair in [('grk', 'nt')]:
            lang_name = pair[0]
            test_name = pair[1]
            lang_unit = self.bible[lang_name]
            test = lang_unit[test_name]
            verse_paths = set()
            for book in test:
                for chap in book:
                    for verse in chap:
                        tmp = tree[lang_name][test.name][book.name][chap.name]
                        tree_verse = tmp[verse.name]
                        tree_verse.eng_verse = Struct()

                        # Strip the lang and test name off the
                        # front. For biblist we only want book/chap/verse.
                        tmp = verse.eng_verse.path.split('.')
                        tree_verse.eng_verse.path = '.'.join(tmp[2:])
                        
                        tree_verse.eng_verse.text = verse.eng_verse.text
                        tree_verse.is_bad = verse.is_bad

                        # Strip the lang and test name off the
                        # front. For biblist we only want book/chap/verse.
                        tmp = verse.path.split('.')
                        tree_verse.path = '.'.join(tmp[2:])
                        
                        for ii, word in enumerate(verse):
                            tree_word = Struct()
                            tree_word.text = word.text
                            if hasattr(word, 'strong_nums'):
                                # Pick the last one in the hopes that it
                                # just has articles in front of it or
                                # something.
                                tree_word.strong_num = word.strong_nums[-1]
                            elif hasattr(word, 'strong_num'):
                                tree_word.strong_num = word.strong_num
                            else:
                                tree_word.strong_num = None
                            try:
                                eng_text = word.eng_word.text
                            except AttributeError:
                                tree_word.eng_text = None
                            else:
                                if not eng_text:
                                    tree_word.eng_text = None
                                else:
                                    is_proper = True
                                    tmp = apply_case(word)
                                    tree_word.eng_text = tmp
                            if hasattr(word, 'tags'):
                                tree_word.tag = word.tags[0]
                            else:
                                tree_word.tag = None
                            tree_verse[str(ii + 1)] = tree_word
                        verse_paths.add('.'.join(verse.path.split('.')[2:]))
                tree_book = tree[lang_name][test.name][book.name]
                tree_book.full_name = book.full_name
                tree_book.name = book.name
            tree_test = tree[lang_name][test.name]
            tree_test.name = test.name
            
            # Strip the lang and test name off the front. For biblist we
            # only want book/chap/verse.
            tree_test.strong_num_verses = {}
            for key, value in test.strong_num_verses.items():
                new_set = set(['.'.join(x.split('.')[2:]) for x in value])
                tree_test.strong_num_verses[key] = new_set
            tree_test.strong_num_paths = {}
            for key, value in test.strong_num_paths.items():
                new_set = set(['.'.join(x.split('.')[2:]) for x in value])
                tree_test.strong_num_paths[key] = new_set
            tree_test.strong_num_uninfs = test.strong_num_uninfs
            tree_lang_unit = tree[lang_name]
            if lang_name == 'grk':
                tree_test.strong_num_tags = test.strong_num_tags
                tree_test.strong_num_is_proper = test.strong_num_is_proper
                tree_test.parsed_dict = test.parsed_dict
                tree_lang_unit.swapcase_dict = lang_unit.swapcase_dict
                # I would like a way to have the lang objects be able to
                # lowercase chars, for example, but this code will cause
                # lang_unit to be pickled since its method requires it as an
                # object, right?
                #tree_lang_unit.is_capital_dict = lang_unit.is_capital_dict
                #tree_lang_unit.lower = lang_unit.lower
                #tree_lang_unit.upper = lang_unit.upper
                #tree_lang_unit.depunctuate = lang_unit.depunctuate

                # HACK
                # This is a hack to make this ITD work with biblist, which
                # expects interlinears to have a primary and secondary
                # language. Originally this was supposed to be a
                # polyglot but it would be cleaner if every interlinear
                # was a diglot.
                tree_test.prim_lang = 'grk'
                tree_test.sec_lang = 'eng'
                # /HACK
            elif lang_name == 'heb':
                # HACK
                # This is a hack to make this ITD work with biblist, which
                # expects interlinears to have a primary and secondary
                # language. Originally this was supposed to be a
                # polyglot but it would be cleaner if every interlinear
                # was a diglot.
                tree_test.prim_lang = 'heb'
                tree_test.sec_lang = 'eng'
                # /HACK
                
            tree_test.verse_paths = verse_paths
        tree.lock_recursively()
        t = time.time()
        print 'Bible light pickled in:'
        with open(output_light, 'w') as pickle_file:
            # Protocol 2 was loaded in 4.4 secs vs. 8.5 for protocol 0.
            pickle.dump(tree, pickle_file, 2)
        print str(time.time() - t) + ' seconds'

        # Create entries in the Django ORM.
        ## Set up Django.
        os.environ['DJANGO_SETTINGS_MODULE'] = 'django_proj.settings'
        os.chdir('django_proj')
        sys.path.append('')
        import django
        from django.db.models import Max
        from django.db import transaction
        from biblist_django.models import Testament as DTestament,\
            Book as DBook,\
            Chapter as DChapter,\
            Verse as DVerse,\
            EngVerse as DEngVerse,\
            Word as DWord,\
            StrongWord as DStrongWord,\
            User, UserWord, UserVerse, VRRecord, DiagRecord, SATValue,\
            PassedValue

        ## Clear all entries.
        print 'Clearing existing entries'
        cls_list = [DTestament, DBook, DChapter, DVerse, DEngVerse,
                    DWord, DStrongWord, User, UserWord, UserVerse,
                    VRRecord, DiagRecord, SATValue, PassedValue]
        for cls in cls_list:
            for x in cls.objects.all():
                x.delete()

        ## Create fake users to be used for UserWord and UserVerse
        ## pre-population.
        NUM_USERS_MAX = 10
        fake_users = []
        for ii in range(NUM_USERS_MAX):
            # Include a forbidden character in the user name. This
            # guarantees no collision with names chosen by users.
            fake_users.append(User(name='fake_user_{}\n'.format(ii)))
        User.objects.bulk_create(fake_users)

        ## Generate structural elements of each Testament (Book,
        ## Chapter, etc.), and StrongWord objects.
        eng_verse_texts = {}
        max_ev_length = 0
        for pair in [('heb', 'ot'), ('grk', 'nt')]:
            dword_to_strong_num = {}
            lang_name = pair[0]
            test_name = pair[1]
            disp_name = TESTAMENT_DISP_NAME[test_name]
            lang_unit = self.bible[lang_name]
            test = lang_unit[test_name]
            dtest = DTestament.objects.create(name=test_name,
                                              disp_name=disp_name,
                                              prim_lang=lang_name,
                                              sec_lang='eng')
            for i, book in enumerate(test):
                dbook = DBook.objects.create(testament=dtest,
                                             full_name=book.full_name,
                                             name=book.name, index=i)
                with transaction.atomic():
                    for i, chap in enumerate(book):
                        chap_num = chap.path.split('.')[-1]
                        dchap = DChapter.objects.create(book=dbook,
                                                        number=chap_num,
                                                        index=i)
                        print chap.path
                        for i, verse in enumerate(chap):
                            dverse_is_bad = verse.is_bad
                            verse_num = verse.path.split('.')[-1]
                            path = '.'.join([book.name, chap_num, verse_num])
                            dverse = DVerse.objects.create(
                                chapter=dchap, number=verse_num, path=path,
                                is_bad=dverse_is_bad, index=i)

                            # Build the English verse object.
                            eng_verse_text = verse.eng_verse.text
                            if len(eng_verse_text) > max_ev_length:
                                max_ev_length = len(eng_verse_text)
                            deng_verse = DEngVerse.objects.create(
                                verse=dverse, text=eng_verse_text)

                            for ii, word in enumerate(verse):
                                dword_text = word.text
                                if hasattr(word, 'strong_nums'):
                                    # Pick the last one in the hopes that it
                                    # just has articles in front of it or
                                    # something.
                                    dword_strong_num = word.strong_nums[-1]
                                elif hasattr(word, 'strong_num'):
                                    dword_strong_num = word.strong_num
                                else:
                                    dword_strong_num = '-'
                                try:
                                    eng_text = word.eng_word.text
                                except AttributeError:
                                    dword_eng_text = '-'
                                else:
                                    if not eng_text:
                                        dword_eng_text = '-'
                                    else:
                                        is_proper = True
                                        dword_eng_text = apply_case(word)
                                if hasattr(word, 'tags'):
                                    dword_tag = word.tags[0]
                                else:
                                    dword_tag = '-'
                                d_word = DWord.objects.create(
                                    verse=dverse, text=dword_text,
                                    eng_text=dword_eng_text, tag=dword_tag,
                                    number=str(ii+1), index=ii)
                                dword_to_strong_num[d_word] = dword_strong_num

            # Create StrongWord objects with their attributes.
            with transaction.atomic():
                total = len(test.strong_num_verses)
                for i, key in enumerate(test.strong_num_verses):
                    print ('Strong Num ' + str(i) + ' / ' + str(total) +
                           ' (' + key + ')')
                    dstrong_word = DStrongWord.objects.create(number=key,
                                                              testament=dtest)

                    # Add verses to the StrongWord object. These correspond
                    # to verses where the Strong's word occurs.
                    verse_paths = test.strong_num_verses[key]
                    for verse_path in verse_paths:
                        tmp = verse_path.split('.')[-3:]
                        book_name, chap_num, verse_num = tmp
                        cur_book = DBook.objects.get(name=book_name)
                        cur_chap = cur_book.chapter_set.get(number=chap_num)
                        cur_verse = cur_chap.verse_set.get(number=verse_num)
                        dstrong_word.verses.add(cur_verse)

                    # Add words to the StrongWord object. These correspond
                    # to paths where the Strong's word occurs.
                    word_paths = test.strong_num_paths[key]
                    for word_path in word_paths:
                        tmp = word_path.split('.')[-4:]
                        book_name, chap_num, verse_num, word_num = tmp
                        cur_book = DBook.objects.get(name=book_name)
                        cur_chap = cur_book.chapter_set.get(number=chap_num)
                        cur_verse = cur_chap.verse_set.get(number=verse_num)
                        cur_word = cur_verse.word_set.get(number=word_num)
                        cur_word.strong_word = dstrong_word
                        cur_word.save()

                    # Add uninflected word to the StrongWord object.
                    dstrong_word.uninflected_word = test.strong_num_uninfs[
                        dstrong_word.number]

                    # Set the number of occurrences.
                    dstrong_word.occs = len(word_paths)
                    # Save now so that the occs attribute can be aggregated.
                    dstrong_word.save()

            with transaction.atomic():
                # Calculate the importances of the StrongWord objects.
                max_occs = DStrongWord.objects.filter(
                    testament=dtest).aggregate(Max('occs'))['occs__max']
                for i, dw in enumerate(DStrongWord.objects.filter(
                        testament=dtest)):
                    print ('StrongWord Importance ' + str(i) + ' / ' +
                           str(total))
                    dw.importance = (log(float(dw.occs + 1)) /
                                     log(float(max_occs + 1)))
                    dw.save()

                # If this testament includes the empty StrongWord
                # (i.e. with number '-'), then set the importance as if
                # there were no occurrences.
                try:
                    dw = DStrongWord.objects.get(testament=dtest, number='-')
                except DStrongWord.DoesNotExist:
                    pass
                else:
                    dw.importance = (log(float(1)) / log(float(max_occs + 1)))

                # Since StrongWord objects have now been created, the
                # Word.strong_word field can be filled out.
                total = len(dword_to_strong_num)
                for i, dword in enumerate(dword_to_strong_num):
                    if i % 100 == 0:
                        print 'Word.strong_word ' + str(i) + ' / ' + str(total)
                    number = dword_to_strong_num[dword]
                    dword.strong_word = DStrongWord.objects.filter(
                        testament=dtest).get(number=number)
                    dword.save()

                # Calculate the importances of the Verse objects.
                dverses = DVerse.objects.filter(chapter__book__testament=dtest)
                total = dverses.count()
                for i, verse in enumerate(dverses):
                    if i % 100 == 0:
                        print 'Verse Importance ' + str(i) + ' / ' + str(total)
                    imps = []
                    for word in verse.word_set.all():
                        imps.append(word.strong_word.importance)
                    try:
                        verse.importance = float(sum(imps)) / len(imps)
                    except ZeroDivisionError:
                        # E.g. Luke 17:36, Acts 8:37, 15:34
                        verse.importance = 0
                    verse.save()

            # Pre-populate the database with UserWord and UserVerse
            # objects to be modified at user creation. Creating these
            # objects at user creation is too slow.
            for i, user in enumerate(User.objects.all()):
                print 'Creating objects for user {0} / {1}'.format(
                    i + 1, len(fake_users))
                                                                   
                test_words = DStrongWord.objects.filter(testament=dtest)
                uwords = []
                for strong_word in test_words:
                    uwords.append(UserWord(user=user,
                                           strong_word=strong_word,
                                           importance=strong_word.importance,
                                           knownness=0.0, suitability=0.0))
                uverses = []
                verses = DVerse.objects.filter(chapter__book__testament=dtest)
                for verse in verses:
                    uverses.append(UserVerse(
                        user=user, testament=dtest, verse=verse,
                        path=verse.path, word_count=verse.word_set.count()))
                with transaction.atomic():
                    UserVerse.objects.bulk_create(uverses)
                    UserWord.objects.bulk_create(uwords)
                    
            # if lang_name == 'grk':
            #     tree_test.strong_num_tags = test.strong_num_tags
            #     tree_test.strong_num_is_proper = test.strong_num_is_proper
            #     tree_test.parsed_dict = test.parsed_dict
            #     tree_lang_unit.swapcase_dict = lang_unit.swapcase_dict
                
    def run_test(self):
        bp()
        

class EnglishSource(IterableTreeDict):
    def __init__(self, parent):
        # fix. Include exceptions from word_matcher in here somewhere.
        self.parent = parent
        self.src_dir = parent.src_dirs['eng']
        self.wrk_dir = parent.wrk_dirs['eng']
        self.strong_dict = {}
        # Create processed files if not already existent.
        if (parent.refresh_working and not parent.testing):
            try:
                shutil.rmtree(self.wrk_dir)
            except OSError:
                pass
        if not os.path.exists(self.wrk_dir):
            os.makedirs(self.wrk_dir)
            self.pre_process()
        
    def pre_process(self):
        print 'Preprocessing KJV'
        with open(join(self.src_dir, 'kjvfull.xml')) as src_file:
            print '  Reading kjvfull.xml...'
            text = src_file.read()
        print '  Cleaning seg, divineName, and inscription tags from xml...'
        text = re.compile(r'''
        (</?q.*?>)
        | (</?seg.*?>)
        | (</?divineName.*?>)
        | (</?inscription.*?>)
        | (</?transChange.*?>)
        | (</?milestone.*?>)
        | <note.*?>.*?</note>
        | <foreign.*?>.*?</foreign>
        | <verse\ eID=.*?/>
        ''', re.VERBOSE).sub('', text)
        print '  Formatting nicely for debugging purposes...'
        text = re.compile('<div type="coloph.*?>.*?</div>').sub(r'',text)
        text = re.compile('<title type="[^p].*?>.*?</title>').sub(r'',text)
        #text = re.compile('(lemma=")[^1-9]*').sub(r'\1',text)
        #text = re.compile(' morph=".*?"').sub(r'',text)
        text = re.compile('robinson:').sub(r'',text)
        # A word in Mark 15:34 appears to be in error, missing the src.
        tmp = r'<w src="21" lemma="strong:G3450"'
        text = re.compile('<w lemma="strong:G3450"').sub(tmp ,text)
        text = re.compile('strong:H0').sub(r'H',text)
        text = re.compile('strong:').sub(r'',text)
        text = re.compile('strongMorph:').sub(r'',text)
        text = re.compile('<header>.*?</header>',re.S).sub('',text)
        text = re.compile('xmlns=".*?"').sub('xmlns=""',text)
        text = re.compile('(<w .*?/w?>)').sub(r'\n\1',text)
        ## if self.run_type == 'lite':
        ##     print 'Removing all but Malachi and 2 Peter...'
        ##     text = re.compile('''
        ##     <div\ type="book"\ osisID="Gen".*?
        ##     (?=<div\ type="book"\ osisID="Mal")
        ##     | <div\ type="book"\ osisID="Matt".*?
        ##     (?=<div\ type="book"\ osisID="2Pet")
        ##     | <div\ type="book"\ osisID="1John".*?
        ##     (?=</osisText>)
        ##     ''', re.X | re.S).sub('', text)
        with open(join(self.wrk_dir, 'kjvfull.xml'), 'w') as dest_file:
            print '  Writing kjvfull.xml...'
            dest_file.write(text)
            
    def create_language_unit(self, parent):
        print '  Creating English Language Unit'
        this_lang_unit = LangUnit(parent, 'eng')
        this_lang_unit['ot'] = self.create_testament(this_lang_unit, 'ot')
        this_lang_unit['nt'] = self.create_testament(this_lang_unit, 'nt')
        return this_lang_unit

    def create_testament(self, parent, test_name):
        t0 = time.time()
        with open(join(self.wrk_dir, 'kjvfull.xml'), 'r') as src_file:
            kjv_parsed = ElementTree.fromstring(src_file.read())
        books_parsed = kjv_parsed[0].getchildren()
        this_test = Testament(parent, test_name)
        self.cur_test = this_test
        for book_parsed in books_parsed:
            book_name = book_parsed.attrib['osisID']
            if (test_name == 'ot') and (book_name in OT_HEB_OSIS_ABBREVS):
                cond = True
            elif (test_name == 'nt') and (book_name in NT_OSIS_ABBREVS):
                cond = True
            else:
                cond = False
            if cond:
                tmp = self.create_book(this_test, book_name, book_parsed)
                this_test[book_name] = tmp

        # Collect some stats.
        book_count = chap_count = verse_count = word_count = 0
        for book in this_test:
            book_count += 1
            for chap in book:
                chap_count += 1
                for verse in chap:
                    verse_count += 1
                    for word in verse:
                        word_count += 1
        this_test.number_of_books = book_count
        this_test.number_of_chaps = chap_count
        this_test.number_of_verses = verse_count
        this_test.number_of_words = word_count
        this_test.chaps_per_book = chap_count / float(book_count)
        this_test.verses_per_chap = verse_count / float(chap_count)
        this_test.words_per_verse = word_count / float(verse_count)
        this_test.time_to_create = time.time() - t0
        return this_test
    
    def create_book(self, parent, book_name, book_parsed):
        t0 = time.time()
        this_book = Book(parent, book_name)
        print '    Creating ' + this_book.name
        self.cur_book = this_book
        for chap_parsed in book_parsed:
            chap_name = chap_parsed.attrib['osisID'].split('.')[-1]
            this_book[chap_name] = self.create_chap(this_book, chap_name,
                                                   chap_parsed)
            if self.parent.mini:
                if len(this_book) == 2:
                    break
        this_book.time_to_create = time.time() - t0
        return this_book
    
    def create_chap(self, parent, chap_name, chap_parsed):
        t0 = time.time()
        this_chap = Chapter(parent, chap_name)
        self.cur_chap = this_chap
        for node in chap_parsed:
            if 'w' in node.tag:
                word_name = str(len(this_verse) + 1)
                this_verse[word_name] = self.create_word(this_verse,
                                                         node, word_name)
                if node.text:
                    this_verse.text += node.text + node.tail.replace('\n', '')
            elif 'verse' in node.tag:
                try:
                    this_verse.time_to_create = time.time() - t1
                except NameError:
                    pass
                t1 = time.time()
                verse_name = node.attrib['osisID'].split('.')[-1]
                this_verse = Verse(this_chap, verse_name)
                self.cur_verse = this_verse
                this_chap[verse_name] = this_verse
            elif 'title' in node.tag: #Psalm Titles
                try:
                    this_verse.time_to_create = time.time() - t1
                except NameError:
                    pass
                t1 = time.time()
                verse_name = '0' 
                this_verse = Verse(this_chap, verse_name)
                self.cur_verse = this_verse
                this_chap[verse_name] = this_verse
            if node.tail: #test #diag: count the number of tails there are.
                bare_text_name = str(len(this_verse)+1)
        this_verse.time_to_create = time.time() - t1
        this_chap.time_to_create = time.time() - t0
        return this_chap
    
    def create_word(self, parent, word_parsed, word_name):
        t0 = time.time()
        this_word = BibleWord(parent, word_name)
        self.cur_word = this_word
        this_word.text = word_parsed.text
        try:
            tmp = word_parsed.attrib['lemma'].split(' ')
        except KeyError:
            tmp = [] #See Ezra 5.3.3
        this_word.strong_nums = tmp
        ## this_word.strong_words = []
        ## strong_words_list = list(self.cur_test.parent.strong_words)
        ## for strong_num in strong_nums:
        ##     this_strong_word = UserWord(int(strong_num), 'eng')
        ##     try:
        ##         # Note that the index function finds the first element of a
        ##         # list that is equal to the object in question, not necessarily
        ##         # one having the same id.
        ##         ind = fstrong_words_list.index(this_strong_word)
        ##     except ValueError:
        ##         self.cur_test.parent.strong_words.add(this_strong_word)
        ##     else:
        ##         this_strong_word = strong_words_list[ind]
        ##     this_strong_word.paths.add(this_word.path)
        ##     this_strong_word.verses.add(this_word.parent)
        ##     this_word.strong_words.append(this_strong_word)
        try:
            morph_tag = word_parsed.attrib['morph']
        except KeyError:
            this_word.tags = []
        else:
            this_word.tags = morph_tag.split(' ') # See Gen. 24:33 for multi.
        try:
            temp = word_parsed.attrib['src']
        except KeyError:
            this_word.srcs = None
        else:
            # Convert to zero-based index.
            this_word.srcs = map(lambda x: int(x)-1, temp.split(' '))
        this_word.time_to_create = time.time() - t0
        return this_word
    
class HebrewSource(IterableTreeDict):
    def __init__(self, parent):
        self.src_dir = parent.src_dirs['heb']
        self.wrk_dir = parent.wrk_dirs['heb']
        self.parent = parent
        self.munkres = Munkres()
        self.strong_dict = {}
        self.osis_lookup = {OT_HEB_OSIS_ABBREVS[i]: OT_HEB_FULL_BOOK_NAMES[i]
                            for i in range(len(OT_HEB_OSIS_ABBREVS))}

        # Create processed files if not already existent.
        if (parent.refresh_working and not parent.testing):
            try:
                shutil.rmtree(self.wrk_dir)
            except OSError:
                pass
        if not os.path.exists(self.wrk_dir):
            os.makedirs(self.wrk_dir)
            self.pre_process()
        with open(join(self.wrk_dir, 'VerseMap.xml')) as src_file:
            print '  Creating Hebrew Verse Map'
            self.verse_map = VerseMap(src_file)
        with open(join(self.wrk_dir, 'HebrewMesh.xml')) as src_file:
            print '  Creating Hebrew Uninflected Dictionary'
            self.uninflected_dict = UninflectedDict(src_file)
            
    def pre_process(self):
        #From raw source texts, create processed texts that are cleaner and
        #easier to read during debugging.
        print 'Preprocessing Heb OT'
        for src_fname in os.listdir(self.src_dir):
            if (src_fname.startswith('#')) | (src_fname.endswith('~')):
                continue
            with open(join(self.src_dir, src_fname), 'r') as src_file:
                text = src_file.read()
            if os.path.splitext(src_fname)[0] in OT_HEB_OSIS_ABBREVS:
                #Remove letters mixed in with strong numbers.
                tmp_str = '(emma=")[^1-9]*?([0-9]*)[^1-9]*?"'
                text = re.compile(tmp_str).sub(r'\1\2"', text)
                ## tmp_str = '(<w[^\n]*?)(<w)'
                ## text = re.compile(tmp_str).sub(r'\1\n\2', text)
            elif src_fname == 'VerseMap.xml':
                text = text
            elif src_fname == 'HebrewMesh.xml':
                #tmp_re = re.compile('http://www.APTBibleTools.com/namespace')
                #text = tmp_re.sub("",text)
                text = text #prior lines not working
            else:
                continue
            with open(join(self.wrk_dir, src_fname), 'w') as dest_file:
                dest_file.write(text)
    
    def create_language_unit(self, parent):
        print '  Creating Hebrew Language Unit'
        self.eng = parent['eng']
        this_lang_unit = LangUnit(parent, 'heb')
        this_lang_unit['ot'] = self.create_testament(this_lang_unit)
        return this_lang_unit

    def create_testament(self, parent):
        print '  Creating Old Testament'
        this_test = Testament(parent, 'ot')
        this_test.strong_dict = self.strong_dict
        this_test.strong_num_paths = defaultdict(set)
        this_test.strong_num_verses = defaultdict(set)
        this_test.strong_num_uninfs = {}
        this_test.bad_verses = set()
        ordered_filenames = [x + '.xml' for x in OT_HEB_OSIS_ABBREVS]
        ordered_filenames.extend(['HebrewMesh.xml', 'VerseMap.xml'])
        for src_fname in ordered_filenames:
            book_name = os.path.splitext(src_fname)[0]
            if book_name in OT_HEB_OSIS_ABBREVS:
                with open(join(self.wrk_dir, src_fname), 'r') as src_file:
                    book_parsed = ElementTree.fromstring(src_file.read())
                this_test[book_name] = self.create_book(this_test, book_name,
                                                        book_parsed)
        # Build a dict of OSIS paths to strong words.
        for book in this_test:
            for chap in book:
                for verse in chap:
                    if verse.is_bad:
                        this_test.bad_verses.add(verse.path)
                    for word in verse:
                        tmp = this_test.strong_num_paths
                        tmp[word.strong_num].add(word.path)
                        tmp = this_test.strong_num_verses
                        tmp[word.strong_num].add(verse.path)
                        tmp = this_test.strong_num_uninfs
                        if word.strong_num in tmp:
                            uninf_word = word.uninflected_word
                            assert(tmp[word.strong_num] == uninf_word)
                        tmp[word.strong_num] = word.uninflected_word

        # Collect some stats.
        unique_word_forms = set()
        book_count = chap_count = verse_count = word_count = 0
        for book in this_test:
            book_count += 1
            for chap in book:
                chap_count += 1
                for verse in chap:
                    verse_count += 1
                    for word in verse:
                        unique_word_forms.add(word.text.replace('/', ''))
                        word_count += 1
        this_test.number_of_books = book_count
        this_test.number_of_chaps = chap_count
        this_test.number_of_verses = verse_count
        this_test.number_of_words = word_count
        this_test.number_of_unique_word_forms = len(unique_word_forms)
        this_test.chaps_per_book = chap_count / float(book_count)
        this_test.verses_per_chap = verse_count / float(chap_count)
        this_test.words_per_verse = word_count / float(verse_count)
        return this_test
    
    def create_book(self, parent, book_name, book_parsed):
        this_book = Book(parent, book_name)
        print '    Creating ' + this_book.name
        this_book.full_name = self.osis_lookup[this_book.name]
        self.cur_book = this_book
        osis_text = book_parsed.getchildren()[0]
        div = [x for x in osis_text.getchildren() if 'div' in x.tag][0]
        for chap_parsed in div:
            chap_name = chap_parsed.attrib['osisID'].split('.')[-1]
            this_book[chap_name] = self.create_chap(this_book, chap_name,
                                                    chap_parsed)
            if self.parent.mini:
                if len(this_book) == 2:
                    break

        return this_book
    
    def create_chap(self, parent, chap_name, chap_parsed):
        this_chap = Chapter(parent, chap_name)
        self.cur_chap = this_chap
        verses_parsed = [x for x in chap_parsed if 'verse' in x.tag]
        for verse_parsed in verses_parsed:
            verse_name = verse_parsed.attrib['osisID'].split('.')[-1]
            this_chap[verse_name] = self.create_verse(this_chap, verse_name,
                                                      verse_parsed)
        return this_chap
    
    def create_verse(self, parent, verse_name, verse_parsed):
        this_verse = Verse(parent, verse_name)
        self.cur_verse = this_verse
        (eng_verse_path, verse_type) = self.verse_map(this_verse)
        if 'partial' in verse_type:
            this_verse.is_bad = 'Verse Map only partially converted'
        words_parsed = [x for x in verse_parsed.getchildren() if '}w' in x.tag]
        eng_verse = self.eng[eng_verse_path]
        this_verse.eng_verse = eng_verse
        (words_matrix, max_dist) = self.tagged_sync(eng_verse, words_parsed)
        sorted_columns = sorted([x for x in zip(*words_matrix) if x[1]],
                         key=lambda x: x[1].position)
        this_verse.columns = sorted_columns
        this_verse.max_dist = max_dist
        for ii in range(len(sorted_columns)):
            word_name = str(ii + 1)
            this_verse[word_name] = self.create_word(this_verse, word_name,
                                                     sorted_columns[ii])
        return this_verse
    
    def create_word(self, parent, word_name, column):
        this_word = BibleWord(parent, word_name)
        self.cur_word = this_word
        this_word.text = column[1].text
        if column[0]:
            this_word.eng_word = column[0]
        else:
            this_word.eng_word = None
        assert(len(column[1].strong_nums) == 1)
        this_word.strong_num = column[1].strong_nums[0]
        try:
            uninf_word = self.uninflected_dict[this_word.strong_num]
        except KeyError:
            uninf_word = None
        this_word.uninflected_word = uninf_word
        #self.add_to_strong_dict(strong_num, uninf_word, eng_word)
        return this_word

    ## def find_eng_words(self, words_parsed, eng_verse):
    ##     # This might need optimizing. Also it takes the first match rather
    ##     # than finding the nearest match. fix. test.
    ##     lang_strong_nums = ['H' + x.attrib['lemma'] for x in words_parsed]
    ##     eng_pairs = zip([x.strong_nums for x in eng_verse],
    ##                        [x.text for x in eng_verse])
    ##     eng_words = []
    ##     for lang_num in lang_strong_nums:
    ##         try:
    ##             # I believe that in kjvfull, the OT has exactly one strong num
    ##             # per word.
    ##             eng_ind = [x[0][0] for x in eng_pairs].index(lang_num)
    ##         except ValueError:
    ##             eng_words.append(None)
    ##             bp()
    ##         else:
    ##             eng_words.append(eng_pairs.pop(eng_ind)[1])
    ##     return eng_words
    
    def tagged_sync(self, eng_verse, words_parsed):
        '''Synchronize the tagged Hebrew to the English.'''
        # Create an object for each English word.
        eng_srcs = []
        for word in eng_verse:
            this_src = Struct()
            this_src.text = word.text
            this_src.strong_nums = word.strong_nums
            eng_srcs.append(this_src)

        # Create an object for each Hebrew word.
        position = 0
        heb_srcs = []
        for word in words_parsed:
            this_src = Struct()
            this_src.text = word.text
            if word.attrib['lemma']:
                this_src.strong_nums = ['H' + word.attrib['lemma']]
            else:
                this_src.strong_nums = ['-']
            this_src.position = position
            position += 1
            heb_srcs.append(this_src)

        # Ensure II > JJ for the II x JJ scores matrix about to be created.
        if len(heb_srcs) >= len(eng_srcs):
            vert_srcs, hor_srcs = heb_srcs, eng_srcs
        else:
            vert_srcs, hor_srcs = eng_srcs, heb_srcs
        II = len(vert_srcs)
        JJ = len(hor_srcs)
            
        def eval_match(hor, vert):
            def list_match(a, b):
                #fix. this can be smarter.
                if (len(a) == 0) | (len(b) == 0):
                    return float(0)
                matching_pairs = [1 for x in a for y in b if x == y]
                return float(len(matching_pairs)) / (len(a) * len(b))
            strongs_score = list_match(hor.strong_nums, vert.strong_nums)
            return strongs_score
        scores = [[eval_match(x, y) for x in hor_srcs] for y in vert_srcs]

        # Adjust values based on distance from diagonal, since most matches
        # should be along the diagonal.
        def calc_distance_from_diagonal(ii, jj, II, JJ):
            # Nonzero d prevents divide by zero.
            d = 1E-6 
            if (ii / (jj + d) > (II / (JJ + d))):
                return (sqrt(ii ** 2 + jj ** 2) * sin(atan(JJ / (II + d)) -
                                                  atan(jj / (ii + d))))
            else:
                return (sqrt(ii ** 2 + jj ** 2) * sin(atan(II / (JJ + d)) -
                                                  atan(ii / (jj + d))))
        max_score = 0
        for ii in range(II):
            for jj in range(JJ):
                dist = calc_distance_from_diagonal(ii, jj, II, JJ)
                scores[ii][jj] /=  dist * .6 + 1
                if scores[ii][jj] > max_score:
                    max_score = scores[ii][jj]
        # Subtract all entries from max_score to get a cost matrix instead of a
        # profit matrix. Munkres finds the minimum sum, not the max.
        for ii in range(II):
            for jj in range(JJ):
                scores[ii][jj] = max_score - scores[ii][jj]

        # Use imported munkres module (Hungarian algorithm implementation) to
        # optimize word matches.
        indices = self.munkres.compute(scores)

        # Ensure indices are in the order (eng_src index, heb_src index).
        if hor_srcs is eng_srcs:
            indices = sorted([(x[1], x[0]) for x in indices])

        # Indices furthest from the diagonal should be noted. Verses with an
        # index that is far from the diagonal can be sifted out as bad later.
        max_dist = 0 
        for entry in indices:
            dist = calc_distance_from_diagonal(entry[0], entry[1], II, JJ)
            if dist > max_dist:
                max_dist = dist

        # Create words_matrix, adding all hebrew srcs to the second row of
        # the matrix, underneath their corresponding english srcs. English and
        # Hebrew srcs with no correspondent should have a None below or above
        # them, resp.
        words_matrix = [eng_srcs, [None for x in eng_srcs]]
        num_taken = 0
        used_srcs = []
        while len(indices) > 0:
            cur_ind = indices.pop(0)
            words_matrix[1][cur_ind[0]] = heb_srcs[cur_ind[1]]
            used_srcs.append(cur_ind[1])
        for ii in range(len(heb_srcs)):
            if ii not in used_srcs:
                leftover_src = heb_srcs[ii]
                words_matrix[0].append(None)
                words_matrix[1].append(leftover_src)

        return (words_matrix, max_dist)

    def add_to_strong_dict(self, strong_num, uninf_word, eng_word):
        if strong_num == '':
            return
        if strong_num in self.strong_dict:
            temp_dict = self.strong_dict[strong_num]
            if uninf_word in temp_dict['uninf_word']:
                temp_dict['uninf_word'][uninf_word] += 1
            else:
                temp_dict['uninf_word'][uninf_word] = 1
            if eng_word in temp_dict['eng_word']:
                temp_dict['eng_word'][eng_word] += 1
            else:
                temp_dict['eng_word'][eng_word] = 1
        else:
            self.strong_dict[strong_num]={'uninf_word': {uninf_word: 1},
                                          'eng_word': {eng_word: 1}}
        
        
class GreekSource(object):
    def __init__(self, parent):
        src_dir = parent.src_dirs['grk']
        wrk_dir = parent.wrk_dirs['grk']
        self.parent = parent  #cur: replacing kwargs with refs to parent.
        self.src_dir_tagged = join(src_dir, 'byztxt/BP05FNL')
        self.src_dir_check = join(src_dir, 'byztxt/TR-PRSD')
        self.src_dir_text = join(src_dir, 'byztxt/BYZ05CCT')
        self.wrk_dir_tagged = join(wrk_dir, 'tagged')
        self.wrk_dir_check = join(wrk_dir, 'check')
        self.wrk_dir_text = join(wrk_dir, 'text')
        self.strong_dict = {}
        # Pretty Printers for debugging.
        self.ppr0 = PrettyPrinter(indent=2, depth=1, width=200).pprint #diag
        self.ppr1 = PrettyPrinter(indent=2, depth=1, width=200).pprint #diag
        self.ppr2 = PrettyPrinter(indent=2, depth=2, width=200).pprint #diag
        self.ppr3 = PrettyPrinter(indent=2, depth=3, width=200).pprint #diag
        self.ppr4 = PrettyPrinter(indent=2, depth=4, width=200).pprint #diag
        self.ppr5 = PrettyPrinter(indent=2, depth=5, width=200).pprint #diag
        self.munkres = Munkres()
        self.count_mismatch = [] #diag
        self.locs_mismatch = [] #diag
        self.discrepancies = {'tag_vs_check': {}, 'tag_vs_text': {},
                              'kjv_vs_check': {}}
        self.beta_to_uni_dict = self.create_beta_to_uni_dict()
        self.discrepancy_fixes = self.create_discrepancy_fixes()
        self.osis_lookup = {NT_BYZTXT_ABBREVS[i]: NT_OSIS_ABBREVS[i]
                            for i in range(len(NT_BYZTXT_ABBREVS))}
        self.osis_full_lookup = {NT_BYZTXT_ABBREVS[i]: NT_FULL_BOOK_NAMES[i]
                            for i in range(len(NT_BYZTXT_ABBREVS))}
        ## if kwargs['run_type'] == 'lite':
        ##     NT_BYZTXT_ABBREVS = ('2PE',)
        #Create processed files if not already existent.
        if (parent.refresh_working and not parent.testing):
            try:
                shutil.rmtree(wrk_dir)
            except OSError:
                pass
        if not os.path.exists(wrk_dir):
            for directory in [self.wrk_dir_tagged, self.wrk_dir_check,
                              self.wrk_dir_text]:
                os.makedirs(directory)
            print 'Preprocessing Greek Source - Tagged'
            self.pre_process_tagged(self.src_dir_tagged, self.wrk_dir_tagged)
            print 'Preprocessing Greek Source - Check'
            self.pre_process_tagged(self.src_dir_check, self.wrk_dir_check)
            print 'Preprocessing Greek Source - Text'
            self.pre_process_text(self.src_dir_text, self.wrk_dir_text)
        #([a-z]*)\      # a word followed by a space
        #([0-9]*)\      # a Strong''s number followed by a space
        #({[-A-Z]*?})?\ ?    # possibly a grammar tag followed by a space
        #([0-9]*)\ ?    # possibly another Strong''s number followed by a space
        #({[^}]*?})        # a grammar tag
        #(?=((\ [a-z])|(\ ?$)))     # lookahead to the next word or end of line
        self.tagged_re = re.compile(r'''
        ([a-z ]+)\              # one (or more, see Lk.13.7) words with a space
                                # before and after
        (.*?)                   # strong nums and grammar tags
        (?=((\ [a-z|])|(\ ?$))) # lookahead to the next word or end of line.
        ''', re.X)
        self.word_strong_tag_re = re.compile(r'''
        \ ([a-z ]*)\      # one (or more, see Lk.13.7) words with a space
                          # before and after
        ([0-9]*)\         # a Strong''s number followed by a space
        ({[-A-Z]*?})?\ ?  # possibly a grammar tag followed by a space
        ([0-9]*)\ ?       # possibly another Strong''s number followed by a
                          # space
        ({[^}]*?})        # a grammar tag
        ''', re.X)

        self.text_re = re.compile('([^ .,?;]+[.,?;]?)')
        self.variant_re = re.compile(r'''
        \|\               # a pipe and a space
        (.*?)                   # stuff (or perhaps nothing, see 2CO.TRP 7:16)
        \|\               # a pipe and a space
        (.*?)                   # stuff (presumably something)
        \|\               # a pipe and a space
        ''', re.X)

    def pre_process_tagged(self, src_dir, wrk_dir):
        #From raw source texts, create processed texts that are cleaner and
        #easier to read during debugging.
        master_text_orig = ''
        master_text = ''
        for src_fname in os.listdir(src_dir):
            if '~' in src_fname:
                continue # diag. this only occurs when i am editing the source
                      # texts as the code is running (emacs feature).
            if os.path.splitext(src_fname)[0] in NT_BYZTXT_ABBREVS:
                print '  Preprocessing ' + src_fname
                with open(join(src_dir,src_fname), 'r') as src_file:
                    text = src_file.read()
                master_text_orig += src_fname + '\n' + text + '\n***********\n'
                for pair in self.discrepancy_fixes[src_fname]:
                    text = re.compile(pair[0], re.S).sub(pair[1], text)
                    
                # AC.BP5 has a big fat OR which I think is in the boolean
                # sense, at 24:8 (or 24:6). fix. does this occur elsewhere?
                # Remove all verses in parentheses left over from processing
                # alternates.
                #text = re.compile(r'\(\d*?:\d*?\) ').sub('', text)
                text = re.compile('- ').sub('-', text) #See 2PE.TRP 1:21 N- GSM
                text = re.compile('[()]').sub('', text)
                if os.path.splitext(src_fname)[1] == '.BP5':
                    # Allowing variants in BP5 desyncs BP5 with CCT since
                    # variants are being removed from CCT. Verify that the
                    # correct variants are being chosen. fix.
                    text = re.compile(r'\|(.*?)\|.*?\|', re.S).sub(r'\1', text)
                else:
                    # Removing variants in TRP desyncs TRP with KJVfull src #s
                    #text = re.compile(r'\| ', re.S).sub(r'', text)
                    pass
                #bp()
                text = re.compile(r'[^a-zA-Z0-9{}|]{2,}', re.S).sub(r' ', text)
                #text = re.compile(r'\n').sub(r' ', text)
                text = re.compile(r'([\d]*:[\d]*)', re.S).sub(r'\n\1', text)
                #remove newlines before 1:1
                text = re.compile(r'.*? ?(1:1 .*)', re.S).sub(r'\1', text)
                master_text += '\n\n' + src_fname + '\n' + text
            else:
                continue
            with open(join(wrk_dir, src_fname), 'w') as dest_file:
                dest_file.write(text)
        with open(join(wrk_dir, 'master.txt'), 'w') as dest_file:
            dest_file.write(master_text) #Create a regexable file of all books
        with open(join(wrk_dir, 'master_orig.txt'), 'w') as dest_file:
            dest_file.write(master_text_orig) #Create a regexable file of all books    
    def pre_process_text(self, src_dir, wrk_dir):
        master_text = ''
        master_text_orig = ''
        for src_fname in os.listdir(src_dir):
            if '~' in src_fname:
                continue # diag. this only occurs when i am editing the source
                      # texts as the code is running.
            if os.path.splitext(src_fname)[0] in NT_BYZTXT_ABBREVS:
                print '  Preprocessing ' + src_fname
                if src_fname == 'AC.CCT':
                    with open(join(src_dir,'AC24.CCT'), 'r') as ac24:
                        fix_text = ac24.read()[:-1] # Don't take the last \n.
                        self.discrepancy_fixes['AC.CCT'].extend([
                            (r'24:06.*?(?=24:09)', fix_text)])
                with open(join(src_dir,src_fname), 'r') as src_file:
                    text = src_file.read()
                master_text_orig += src_fname + '\n' + text + '\n***********\n'
                try:
                    for pair in self.discrepancy_fixes[src_fname]:
                        text = re.compile(pair[0], re.S).sub(pair[1], text)
                except:
                    bp()
                # Fix. This is too simplistic and eliminates things such as the
                # sou/se in AC24.CCT (I assume it's in the other files as well.
                text = re.compile(r'{.*?}', re.S).sub(r'', text)
                text = re.compile(r'[ \n\r]{2,}', re.S).sub(r' ', text)
                text = re.compile(r'([\d]+:[\d]+)', re.S).sub(r'\n\1', text)
                #remove newlines before 1:1, and initial metadata lines.
                text = re.compile(r'.*?\n.*?\n(.*)', re.S).match(text).group(1)
                text = re.compile(r'0(\d:)').sub(r'\1', text)
                text = re.compile(r'(:)0(\d)').sub(r'\1\2', text)
                master_text += '\n\n' + src_fname + '\n' + text
            else:
                continue
            with open(join(wrk_dir, src_fname), 'w') as dest_file:
                dest_file.write(text)
        with open(join(wrk_dir, 'master.txt'), 'w') as dest_file:
            dest_file.write(master_text)
        with open(join(wrk_dir, 'master_orig.txt'), 'w') as dest_file:
            dest_file.write(master_text_orig)
                
    def create_language_unit(self, parent):
        print '  Creating Greek Language Unit'
        self.eng = parent['eng']
        this_lang_unit = LangUnit(parent, 'grk')
        this_lang_unit.is_capital_dict = self.create_is_capital_dict()
        this_lang_unit.swapcase_dict = self.create_swapcase_dict()
        this_lang_unit['nt'] = self.create_testament(this_lang_unit)
        return this_lang_unit
        
    def create_testament(self, parent):
        t0 = time.time()
        this_test = Testament(parent, 'nt')
        self.cur_test = this_test
        this_test.bad_verses = set()
        this_test.strong_num_paths = defaultdict(set)
        this_test.strong_num_verses = defaultdict(set)
        this_test.strong_num_uninfs = {}
        this_test.strong_num_tags = {}
        this_test.strong_num_is_proper = {}
        this_test.parsed_dict = GRK_PARSED_DICT
        book_parsed = {}
        prefix_ind_pairs = []
        # Filter and sort filenames according to NT book order.
        for fname in os.listdir(self.wrk_dir_tagged):
            prefix = fname.split('.')[0]
            try:
                ind = NT_BYZTXT_ABBREVS.index(prefix)
            except ValueError:
                pass
            else:
                prefix_ind_pairs.append((ind, prefix))
        for pair in sorted(prefix_ind_pairs):
            prefix = pair[1]
            with open(join(self.wrk_dir_tagged, prefix + '.BP5'),
                      'r') as src_file_tagged:
                book_parsed['tagged'] = src_file_tagged.readlines()
            with open(join(self.wrk_dir_check, prefix + '.TRP'),
                      'r') as src_file_check:
                book_parsed['check'] = src_file_check.readlines()
            with open(join(self.wrk_dir_text, prefix + '.CCT'),
                      'r') as src_file_text:
                book_parsed['text'] = src_file_text.readlines()
            osis_name = self.osis_lookup[prefix]
            full_name = self.osis_full_lookup[prefix]
            this_test[osis_name] = self.create_book(this_test, osis_name,
                                                  full_name, book_parsed)
        # Build some dicts mapping strong nums to OSIS paths, OSIS verses,
        # morphological tags, and "proper"ness and store them in the Testament.
        lower = this_test.parent.lower
        depunctuate = this_test.parent.depunctuate
        for book in this_test:
            for chap in book:
                for verse in chap:
                    if verse.is_bad:
                        this_test.bad_verses.add(verse.path)
                    for word in verse:
                        clean_text = lower(depunctuate(word.text))
                        for num in word.strong_nums:
                            tmp = this_test.strong_num_paths[num]
                            tmp.add(word.path)
                            tmp = this_test.strong_num_verses[num]
                            tmp.add(verse.path)
                            if num not in this_test.strong_num_tags:
                                this_test.strong_num_tags[num] = {}
                            tmp = this_test.strong_num_tags[num]
                            for tag in word.tags:
                                if tag not in tmp.keys():
                                    tmp[tag] = {}
                                tmp2 = tmp[tag]
                                if clean_text not in tmp2.keys():
                                    tmp2[clean_text] = 0
                                tmp2[clean_text] += 1
                            if num not in this_test.strong_num_is_proper:
                                this_test.strong_num_is_proper[num] = True
                            if len(word.text) > 0:
                                if not parent.is_capital_dict[word.text[0]]:
                                    this_test.strong_num_is_proper[num] = False
                            
        # Build a dict mapping strong nums to uninflected word forms.
        noun_points = {'Nominative': 3, 'Singular': 2, 'Masculine': 1}
        verb_points = {'Present': 2, 'Aorist': 1, 'Second Aorist': 1,
                       'Active': 1, 'Indicative': 1, 'First Person': 2,
                       'Singular': 2, 'Participle': -100}
        for num, path_set in this_test.strong_num_paths.items():
            best_path_score = float("-inf")
            for path in path_set:
                word = this_test['.'.join(path.split('.')[2:])]
                for tag in word.tags:
                    pinfo = parse_info(tag, this_test.parsed_dict)
                    cur_path_score = 0
                    if 'Verb' in pinfo:
                        for key in verb_points:
                            if key in pinfo:
                                cur_path_score += verb_points[key]
                    else:
                        for key in noun_points:
                            if key in pinfo:
                                cur_path_score += noun_points[key]
                    if cur_path_score > best_path_score:
                        clean_text = lower(depunctuate(word.text))
                        this_test.strong_num_uninfs[num] = clean_text
                        best_path_score = cur_path_score

        # Collect some stats.
        book_count = chap_count = verse_count = word_count = 0
        for book in this_test:
            book_count += 1
            for chap in book:
                chap_count += 1
                for verse in chap:
                    verse_count += 1
                    for word in verse:
                        word_count += 1
        this_test.number_of_books = book_count
        this_test.number_of_chaps = chap_count
        this_test.number_of_verses = verse_count
        this_test.number_of_words = word_count
        this_test.chaps_per_book = chap_count / float(book_count)
        this_test.verses_per_chap = verse_count / float(chap_count)
        this_test.words_per_verse = word_count / float(verse_count)
        this_test.time_to_create = time.time() - t0
        return this_test
    
    def create_book(self, parent, osis_name, full_name, book_parsed):
        t0 = time.time()
        this_book = Book(parent, osis_name)
        this_book.full_name = full_name
        print '    Creating ' + this_book.name
        self.cur_book = this_book
        chaps_parsed = []
        for line_num in range(len(book_parsed['tagged'])):
            line = {}
            for key in book_parsed:
                try:
                    line[key] = book_parsed[key][line_num] #error should be thrown here if either check or text are shorter than tagged. see why. test.
                except:
                    bp() #cur. determine why text and tagged and check are out of sync at the end of Acts.
            chap_num = int(re.match(r'(.*?):',line[key]).group(1))
            if chap_num > len(chaps_parsed):
                chaps_parsed.append([line])
            else:
                chaps_parsed[-1].append(line)
        for chap_parsed in chaps_parsed:
            chap_name = re.compile('\d*').match(chap_parsed[0][key]).group()
            this_book[chap_name] = self.create_chap(this_book,
                                                    chap_name, chap_parsed)
            if self.parent.mini:
                if len(this_book) == 2:
                    break

        this_book.time_to_create = time.time() - t0
        return this_book
    
    def create_chap(self, parent, chap_name, chap_parsed):
        t0 = time.time()
        this_chap = Chapter(parent, chap_name)
        self.cur_chap = this_chap
        verses_parsed = []
        for verse_parsed in chap_parsed:
            keylist = verse_parsed.keys()
            temp = verse_parsed[keylist[0]]
            temp1 = verse_parsed[keylist[1]]
            temp2 = verse_parsed[keylist[2]]
            verse_name = re.compile('.*?:(\d*)').match(temp).groups()[0]
            verse_name1 = re.compile('.*?:(\d*)').match(temp1).groups()[0]
            verse_name2 = re.compile('.*?:(\d*)').match(temp2).groups()[0]
            if (verse_name != verse_name1) | (verse_name != verse_name2):
                print keylist[0] + ' verse name: ' + verse_name
                print keylist[1] + ' verse name: ' + verse_name1
                print keylist[2] + ' verse name: ' + verse_name2
                bp()
            this_chap[verse_name] = self.create_verse(this_chap,
                                                      verse_name, verse_parsed)
        this_chap.time_to_create = time.time() - t0
        return this_chap
    
    def create_verse(self, parent, verse_name, verse_parsed):
        t0 = time.time()
        this_verse = Verse(parent, verse_name)
        this_verse.check_source = verse_parsed['check']
        this_verse.tagged_source = verse_parsed['tagged']
        this_verse.text_source = verse_parsed['text']
        self.cur_verse = this_verse
        words_parsed = {}
        kjv_verse = self.kjv_to_gnt_verse_map(this_verse)
        this_verse.eng_verse = kjv_verse
        assert(kjv_verse is not None)
        if len(kjv_verse) == 0:
            this_verse.is_bad = True
            return this_verse
        words_matrix = self.check_sync(kjv_verse, verse_parsed['check'])
        words_matrix, tmp = self.tagged_sync(words_matrix,
                                            verse_parsed['tagged'])
        this_verse.max_match_dist = tmp
        words_matrix, tmp = self.text_sync(words_matrix, verse_parsed['text'])
        this_verse.is_text_flagged = tmp
        this_verse.matrix = words_matrix
        sorted_columns = sorted([x for x in zip(*words_matrix) if x[3]],
                         key=lambda x: x[3].position)
        this_verse.columns = sorted_columns
        for ii in range(len(sorted_columns)):
            word_name = str(ii + 1)
            this_verse[word_name] = self.create_word(this_verse, word_name,
                                                     sorted_columns[ii])
        this_verse.time_to_create = time.time() - t0
        return this_verse
    
    def create_word(self, parent, word_name, column):
        t0 = time.time()
        this_word = BibleWord(parent, word_name)
        self.cur_word = this_word
        this_word.text = column[3].greek_text
        this_word.tags = column[2].tags
        this_word.strong_nums = ['G' + str(x) for x in column[2].strong_nums]
        this_word.eng_word = column[0]
        this_word.time_to_create = time.time() - t0
        return this_word

    def check_sync(self, kjv_verse, check):
        '''Synchronize check to kjv.'''
        # Verify correct number of '|' in check.
        counter = 0 #diag
        for letter in check: #diag
            if letter =='|':
                counter += 1
        if counter % 3 != 0: #diag
            print 'something is wrong. |''s should come in 3''s'
            bp()
        # Verify verse numbering of check matches that of kjv_verse.
        chap_colon_verse = re.match('.*?(?= )', check).group() #diag
        if chap_colon_verse != kjv_verse.parent.name + ':' + kjv_verse.name:
            bp() #diag
        try:
            max_src_num = max([max(x.srcs) for x in kjv_verse])
        except ValueError:
            # This occurs at e.g. Luke.17.36 where kjv_verse has no children
            # because there is no src/tag/strong information in kjvfull.
            # The next line ensures kjv_verse_by_srcs will be [].
            max_src_num = -1
        kjv_verse_by_srcs = [None for x in range(max_src_num + 1)]
        for word in kjv_verse:
            for src in word.srcs:
                kjv_verse_by_srcs[src] = word
        # Verify that kjvfull has no gaps in sources. I.e. if a verse has src
        # numbers thru 20 but has nothing with src = 14, something's up.
        if None in kjv_verse_by_srcs: #diag
            bp() # diag

        # Gather the possible srcs from check referred to in kjvfull. Mark them
        # as variants where appropriate and split words with two tags (such as
        # 2Pet.2.19 w 3739 {R-DSM} 3739 {R-DSN}) into two srcs.
        tagged_re_matches = [x for x in self.tagged_re.finditer(check)]
        variant_bar_matches = [x for x in re.compile('\|').finditer(check)]
        variant_set = set()
        for ii in range(len(variant_bar_matches) / 3):
            begin = variant_bar_matches[3 * ii].start()
            end = variant_bar_matches[3 * ii + 2].start()
            [variant_set.add(x) for x in range(begin, end)]
        check_srcs = []
        for match in tagged_re_matches:
            groups = self.word_strong_tag_re.match(match.group()).groups()
            # It seems that kjvfull only takes the first strong's number if
            # two are given in .TRP (see 2 Cor.7.16 word 1) unless there are
            # also two tags given (see 2Pet.2.19 word "w") in which case two
            # srcs are cited.
            if match.start() in variant_set:
                is_variant = True
            else:
                is_variant = False
            if groups[2]:
                element = Struct()
                element.text = groups[0]
                element.strong_nums = ['G' + groups[1]]
                element.tags = [groups[2].replace('{','').replace('}','')]
                element.is_variant = is_variant
                tmp = check[match.start():match.end()]
                element.source_text = tmp.replace('\r', '').replace('\n', '')
                element.position = len(check_srcs)
                check_srcs.append(element)
                element2 = Struct()
                element2.text = groups[0]
                element2.strong_nums = ['G' + groups[3]]
                element2.tags = [groups[4].replace('{','').replace('}','')]
                element2.is_variant = is_variant
                tmp = check[match.start():match.end()]
                element2.source_text = tmp.replace('\r', '').replace('\n', '')
                element2.position = len(check_srcs)
                check_srcs.append(element2)
            else:
                element = Struct()
                element.text = groups[0]
                element.strong_nums = ['G' + groups[1]]
                element.tags = [groups[4].replace('{','').replace('}','')]
                element.is_variant = is_variant
                tmp = check[match.start():match.end()]
                element.source_text = tmp.replace('\r', '').replace('\n', '')
                element.position = len(check_srcs)
                check_srcs.append(element)
        # Link check_srcs to kjvfull.
        kjv_row = []
        check_row = []
        for ii in range(len(check_srcs)):
            cur_chk = check_srcs[ii]
            # At Luke 1:26 the following applies because TRP has the two halves
            # of a variant at the end of the verse, so kjv_verse_by_srcs runs
            # out before check_srcs is iterated all the way through.
            if len(kjv_verse_by_srcs) == 0:
                kjv_row.append(None)
                check_row.append(cur_chk)
                continue
            cur_kjv = kjv_verse_by_srcs[0]
            #fix the following lines. Why do I just check the first entry?
            cond1 = cur_chk.strong_nums[0] in cur_kjv.strong_nums
            cond2 = cur_chk.tags[0] in cur_kjv.tags
            if cond1 & cond2:
                kjv_row.append(kjv_verse_by_srcs.pop(0))
            else:
                kjv_row.append(None)
            check_row.append(cur_chk)
        # Add all elements not matched to kjv, with Nones corresponding.
        leftover_check = [x for x in check_srcs if x not in check_row]
        check_row += leftover_check
        kjv_row += [None for x in leftover_check]
        # Add all elements not matched to check, with Nones corresponding.
        leftover_kjv = [x for x in kjv_verse if not x.srcs]
        kjv_row += leftover_kjv
        check_row += [None for x in leftover_kjv]
        return [kjv_row, check_row]

    def tagged_sync(self, words_matrix, tagged):
        '''Synchronize tagged to the words matrix, primarily check.'''
        tagged_re_matches = [x for x in self.tagged_re.finditer(tagged)]
        tagged_srcs = []
        # Create an object for each set of text/strongs/morph in tagged, and
        # put the objects in a list.
        for match in tagged_re_matches:
            groups = self.word_strong_tag_re.match(match.group()).groups()
            # It seems that kjvfull only takes the first strong's number if
            # two are given in .TRP (see 2 Cor.7.16 word 1) unless there are
            # also two tags given (see 2Pet.2.19 word "w") in which case two
            # srcs are cited.
            element = Struct()
            element.text = groups[0]
            element.strong_nums = [groups[1]]
            element.tags = [groups[4].replace('{','').replace('}','')]
            tmp = tagged[match.start():match.end()]
            element.source_text = tmp.replace('\r','').replace('\n','')
            element.position = len(tagged_srcs)
            if groups[2]:
                element.strong_nums.append(groups[3])
                element.tags.insert(0,
                                    groups[2].replace('{','').replace('}',''))
            tagged_srcs.append(element)

        # Link tagged_srcs to check_srcs.
        check_srcs = []
        nones_list = []
        for ii in range(len(words_matrix[1])):
            check_src = words_matrix[1][ii]
            if check_src:
                check_srcs.append(check_src)
            else:
                nones_list.append(ii)
                                  
        if len(tagged_srcs) >= len(check_srcs):
            vert_srcs, hor_srcs = tagged_srcs, check_srcs
        else:
            vert_srcs, hor_srcs = check_srcs, tagged_srcs
        II = len(vert_srcs)
        JJ = len(hor_srcs)
            
        # Create II x JJ matrix, with II > JJ.
        def eval_match(hor, vert):
            def text_match(text1, text2):
                #fix. this can be smarter.
                return float(text1 == text2)
            def list_match(a, b):
                #fix. this can be smarter.
                if (len(a) == 0) | (len(b) == 0):
                    return float(0)
                matching_pairs = [1 for x in a for y in b if x == y]
                return float(len(matching_pairs)) / (len(a) * len(b))
            text_score = text_match(hor.text, vert.text)
            strongs_score = list_match(hor.strong_nums, vert.strong_nums)
            tags_score = list_match(hor.tags, vert.tags)
            return (text_score + strongs_score + tags_score) / 3
        scores = [[eval_match(x, y) for x in hor_srcs] for y in vert_srcs]

        # Adjust values based on distance from diagonal, since most matches
        # should be along the diagonal.
        def calc_distance_from_diagonal(ii, jj, II, JJ):
            if (ii / (jj+1E-6) > (II / (JJ+1E-6))):
                return (sqrt(ii**2 + jj**2) * sin(atan(JJ / (II+1E-6)) -
                                                  atan(jj / (ii+1E-6))))
            else:
                return (sqrt(ii**2 + jj**2) * sin(atan(II / (JJ+1E-6)) -
                                                  atan(ii / (jj+1E-6))))
        max_score = 0
        for ii in range(II):
            for jj in range(JJ):
                dist = calc_distance_from_diagonal(ii, jj, II, JJ)
                scores[ii][jj] /=  dist * .6 + 1
                if scores[ii][jj] > max_score:
                    max_score = scores[ii][jj]
        # Subtract all entries from max_score to get a cost matrix instead of a
        # profit matrix. Munkres finds the minimum sum, not the max.
        for ii in range(II):
            for jj in range(JJ):
                scores[ii][jj] = max_score - scores[ii][jj]
        # Use imported munkres module (Hungarian algorithm implementation) to
        # optimize word matches.
        indices = self.munkres.compute(scores)

        # Ensure indices are in the order (check_src index, tagged_src index).
        if hor_srcs is check_srcs:
            indices = sorted([(x[1], x[0]) for x in indices])

        # Indices furthest from the diagonal should be noted. Verses with an
        # index that is far from the diagonal can be sifted out as bad later.
        max_dist = 0 
        for entry in indices:
            dist = calc_distance_from_diagonal(entry[0], entry[1], II, JJ)
            if dist > max_dist:
                max_dist = dist

        # Add row to words_matrix.
        words_matrix.append([])
        nones_added_so_far = 0
        for kk in range(len(words_matrix[1])):
            if kk in nones_list:
                words_matrix[2].append(None)
                nones_added_so_far += 1
            # The len(indices) > 0 applies for, e.g. Col.4.18.
            if (len(indices) > 0) and (indices[0][0] + nones_added_so_far == kk):
                words_matrix[2].append(tagged_srcs[indices[0][1]])
                indices.pop(0)
            else:
                words_matrix[2].append(None)
        return (words_matrix, max_dist)

    def text_sync(self, words_matrix, text):
        '''Synchronize tagged to the words matrix, primarily tagged.'''
        text = ' '.join(text.replace('\r','').replace('\n','').split(' ')[1:])
        text_re_matches = [x for x in self.text_re.findall(text)]
        text_srcs = []
        for ii in range(len(text_re_matches)):
            element = Struct()
            element.source_text = text_re_matches[ii]
            element.position = ii
            element.greek_text = self.bword_to_uword(text_re_matches[ii])
            ## # I know this sucks and is entirely unPythonic, but why is it
            ## # 2-3 times faster than a regex and 10+ times faster than
            ## # joining a list comprehension?
            ## temp = x.replace('.', '').replace(', ', '').replace('?', '')
            ## element.without_accents = temp.replace(';', '').replace(',' ,'')
            text_srcs.append(element)

        # Link to tagged.
        flagged = False
        tagged_row = words_matrix[2]
        text_row = []
        for ii in range(len(tagged_row)):
            if not tagged_row[ii]:
                text_row.append(None)
                continue
            broken = False
            for jj in range(len(text_srcs)):
                try:
                    cond = tagged_row[ii].position == text_srcs[jj].position
                except AttributeError:
                    # Occurs when e.g. tagged_row[ii] is None.
                    pass
                else:
                    if cond:
                        src = text_srcs.pop(jj)
                        text_row.append(src)
                        broken = True
                        break
            if not broken:
                text_row.append(None)
        # All words in text should be linked to words in tagged.
        #assert(len(text_srcs) == 0)
        ## for ii in range(3):
        ##     words_matrix[ii].extend([None for x in range(num_remaining)])
        words_matrix.append(text_row)
        return (words_matrix, flagged)

    def run_test(self, testament):
        # NotImplemented yet. Fix.
        for book in testament:
            print 1
            # Tests at chapter level
            #   Verses in Eng source failing to map to respective Greek source
            #   verses. Should be able to be found by low G1/G2 matrix quality.
            # Tests at verse level
            #   Agreement between E and G1.
            #     -Words in E that failed to link to G1 properly.
            #     -Words in G1 that failed to link to E properly.
            #     -E/G1 disagreement in number of sources.
            #     -G1 variants not seen in E
            #   Agreement between G1 and G2.
            #     -Overall matrix quality. The identity matrix is considered
            #     high quality. Max_match_dist will be an indicator but a
            #     better one is preferred.
            #     -Words in G1 not matched to G2, esp. non-variants.
            #     -Words in G2 not matched to G1, esp. non-variants.
            #   Agreement between G2 and GP
            #     -Words in G2 not matched to GP.
            #     -Words in GP not matched to G2.
            # Tests at word level
            #   -High quality match between G2 and GP text.
            #   -English word found.
            # Flag verses and chaps which fail tests
            # Overall run stats
            #   -Total number of books, chaps, verses
            #   -Time taken
            #     -Per Book/Chap/Verse/Word Avg
            #     -Per Verse as a function of words in the verse. If this
            #      scales quickly then maybe the Hungarian algorithm could be
            #      optimized.
            # General profiling results?
            
        bp()
        return results_string

    def print_matrix(self, matrix, param='source_text', limit=80):
        try:
            # Check if this is words_matrix or scores_matrix.
            matrix[0][0].lang
        except AttributeError:
            entry_width = 4
            height = len(matrix)
            width = len(matrix[0])
            matrix_str = '[\n'
            row_counter = 0
            for row in matrix:
                row_str = '['
                if len(row) != len(matrix[0]):
                    print 'This is NOT a matrix'
                    bp()
                col_counter = 0
                for elem in row:
                    if elem >= 0.01:
                        elem_str = repr(elem) + '    '
                    elif elem > 0:
                        elem_str = '0.01       '
                    else:
                        elem_str = ' ' * entry_width
                    elem_str = elem_str[0:entry_width]
                    ## if row_counter == col_counter:
                    ##     elem_str = '*' + elem_str + '*'
                    row_str += elem_str + '  '
                    col_counter += 1
                row_str = row_str[0:-1] + ']'
                matrix_str += row_str + '\n'
                row_counter += 1
            matrix_str += ']\n'
            print matrix_str
        else:
            print_rows = []
            for ii in range(len(matrix)):
                if len(matrix[ii]) != len(matrix[0]):
                    print 'bad matrix'
                    bp()
                print_rows.append([])
                for jj in range(len(matrix[ii])):
                    try:
                        print_rows[ii].append(getattr(matrix[ii][jj], param))
                    except AttributeError:
                        if matrix[ii][jj] is None:
                            print_rows[ii].append(repr(matrix[ii][jj]))
                        elif param == 'source_text':
                            print_rows[ii].append(getattr(matrix[ii][jj], 'text'))

            lengths = [0 for x in range(len(print_rows[0]))]
            for ii in range(len(print_rows)):
                for jj in range(len(print_rows[ii])):
                    if len(print_rows[ii][jj]) > lengths[jj]:
                       lengths[jj] = len(print_rows[ii][jj])
            prnt = ' '
            for ii in range(len(print_rows)):
                for jj in range(len(print_rows[ii])):
                    tmp = print_rows[ii][jj]
                    print_rows[ii][jj] += ' ' * (lengths[jj] - len(tmp) + 2)
            cur_prnt = ['' for x in range(len(print_rows))]
            for jj in range(len(print_rows[0])):
                if jj == 75:
                    bp()
                if len(cur_prnt[0]) + len(print_rows[0][jj]) > limit:
                    for ii in range(len(print_rows)):
                        print cur_prnt[ii]
                    print '\n'
                    cur_prnt = [print_rows[x][jj] for x in range(len(print_rows))]
                else:
                    for ii in range(len(print_rows)):
                        cur_prnt[ii] += print_rows[ii][jj]
            for ii in range(len(print_rows)):
                print cur_prnt[ii]

    def kjv_to_gnt_verse_map(self, verse):
        # It appears the below mappings were based on Tischendorf or something,
        # because both TRP and BP5 have, e.g. Luke.23.17. Go through and update
        # them?
        vm = {}
        #vm = {'Acts.8.37': None, 'Acts.15.34': None, 'Rom.16.24': None}
        ## vm = {'Matt.17.21': None, 'Matt.18.11': None, 'Matt.21.44': None,
        ##       'Mark.7.16': None, 'Mark.9.44': None, 'Mark.9.46': None,
        ##       'Mark.11.26': None, 'Mark.15.28': None, 'Luke.23.17': None,
        ##       'Luke.24.12': None, 'Luke.24.40': None, 'John.5.4': None,
        ##       'John.21.25': None, 'John.1.38': 'John.1.39',
        ##       'John.1.39': 'John.1.40', 'John.1.40': 'John.1.41',
        ##       'John.1.41': 'John.1.42', 'John.1.42': 'John.1.43',
        ##       'John.1.43': 'John.1.44', 'John.1.44': 'John.1.45',
        ##       'John.1.45': 'John.1.46', 'John.1.46': 'John.1.47',
        ##       'John.1.47': 'John.1.48', 'John.1.48': 'John.1.49',
        ##       'John.1.49': 'John.1.50', 'John.1.50': 'John.1.51',
        ##       'John.1.51': 'John.1.52', 'Acts.8.37': None,
        ##       'Acts.15.34': None, 'Acts.28.29': None, 'Rom.16.24': None,
        ##       'Phil.1.16': 'Phil.1.17', 'Phil.1.17': 'Phil.1.16'}
        # Entries formerly in verse map but removed due to redundancy with
        # discrepancy fixes: 'Matt.23.14': None, 'Acts.24.7': None,
        # '2Cor.13.14': '2Cor.13.13', '3John.1.14': None,
        # 'Rev.13.1': 'Rev.12.18'
        #3John.1.14 is supposed to be ('3John.1.14','3John.1.14'). fix.
        osis_path = '.'.join(verse.path.split('.')[1:])
        if osis_path in vm:
            if vm[osis_path]:
                return self.eng[vm[osis_path]]
            else:
                return None
        else:
            return self.eng[osis_path]
    def create_discrepancy_fixes(self):
        discrepancy_fixes = {}
        for abbrev in NT_BYZTXT_ABBREVS:
            discrepancy_fixes[abbrev + '.BP5'] = []
            discrepancy_fixes[abbrev + '.TRP'] = []
            discrepancy_fixes[abbrev + '.CCT'] = []

        # Matt 15:5-6
        # A: 15:5 "umeiv...wfelhyhv kai...autou"
        #    15:6 "kai...umwn"
        # B: 15:5 "umeiv...wfelhyhv"
        #    15:6 "kai...autou kai...umwn"                 
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['MT.BP5'].extend([
            (r'(15:5.*?)(kai .*?N-ASF} autou 846 {P-GSM})(.*?15:6)',
             r'\1\3 \2')])
        discrepancy_fixes['MT.TRP'].extend([
            (r'\(15:6\)', r'')])
        discrepancy_fixes['MT.CCT'].extend([
            (r'(15:05.*?s,) (.*?:)(.*?15:06)', r'\1\3 \2')])

        # Matt 17:14-15
        # A: 17:14 "kai...autw"
        #    17:15 "kai...legwn kurie...udwr"
        # B: 17:14 "kai...autw kai...legwn"
        #    17:15 "kurie...udwr"
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['MT.BP5'].extend([
            (r'(17:14.*?P-ASM})(.*?17:15)(.*?)(?=kurie)', r'\1\3\2 ')])
        discrepancy_fixes['MT.TRP'].extend([
            (r'\(17:15\)', r'')])
        discrepancy_fixes['MT.CCT'].extend([
            (r'(17:14.*?\)to\\n)(.*?17:15 )(.*?)(?= Ku)', r'\1 \3\2')])

        # Matt 20:4-5
        # A: 20:4 "kakeinoiv...umin"
        #    20:5 "oi...aphlyon palin...wsautwv"
        # B: 20:4 "kakeinoiv...umin oi...aphlyon"
        #    20:5 "palin...wsautwv"
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['MT.BP5'].extend([
            (r'(20:4.*?P-2DP})(.*?20:5)(.*?)(?=palin)', r'\1\3\2 ')])
        discrepancy_fixes['MT.TRP'].extend([
            (r'\(20:5\)', r'')])
        discrepancy_fixes['MT.CCT'].extend([
            (r'(20:04.*?i\^n.)(.*?20:05 )(.*?)(?= Pa/lin)', r'\1 \3\2')])

        # Matt. 23:13 and 14.
        # A: 23:13 "ouai...krima" 23:14 "ouai...eiselthein"
        # B: 23:13 "ouai...eiselthein" 23:14 "ouai...krima"
        # KJV is B, CCT is A, TRP and BP5 are nominally A but have
        # parentheses for B. Choosing B.
        discrepancy_fixes['MT.BP5'].extend([
            (r'(23:13.*?)(23:14 .*?)(?=23:15)', r'\2\1'),
            (r'23:13', r'qwertyuiop'),
            (r'23:14', r'23:13'),
            (r'qwertyuiop', r'23:14'),
            (r'\(23:1[34]\)', r'')])
        discrepancy_fixes['MT.TRP'].extend([
            (r'(23:13.*?)(23:14 .*?)(?=23:15)', r'\2\1'),
            (r'23:13', r'qwertyuiop'),
            (r'23:14', r'23:13'),
            (r'qwertyuiop', r'23:14'),
            (r'\(23:1[34]\)', r'')])
        discrepancy_fixes['MT.CCT'].extend([
            (r'(23:13.*?)(23:14 .*?)(?=23:15)', r'\2\1'),
            (r'23:13', r'qwertyuiop'),
            (r'23:14', r'23:13'),
            (r'qwertyuiop', r'23:14'),
            (r'\(23:1[34]\)', r'')])

        # Matt 26:60-61
        # A: 26:60 "kai...euron"
        #    26:61 "usteron...qeudomarturev eipon...auton"
        # B: 26:60 "kai...euron usteron...qeudomarturev"
        #    26:61 "eipon auton"
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['MT.BP5'].extend([
            (r'(26:60.*})(.*?26:61)(.*?)(?=eipon)', r'\1\3\2 ')])
        discrepancy_fixes['MT.TRP'].extend([
            (r'\(26:61\)', r'')])
        discrepancy_fixes['MT.CCT'].extend([
            (r'(26:60.*?ron\.)(.*?26:61 )(.*?)(?= ei)', r'\1 \3\2')])

        # Mark 6:27-28
        # A: 6:27 "kai...autou"
        #    6:28 "o...fulakh kai...authn"
        # B: 6:27 "kai...autou o...fulakh"
        #    6:28 "kai authn"
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['MR.BP5'].extend([
            (r'(6:27.*?P-GSM})(.*?6:28)(.*?)(?=kai)', r'\1\3\2 ')])
        discrepancy_fixes['MR.TRP'].extend([
            (r'\(6:28\)', r'')])
        discrepancy_fixes['MR.CCT'].extend([
            (r'(06:27.*?tou\^).(.*?06:28 )(.*?)(?= kai)', r'\1 \3\2')])

        # Luke 1:73-4
        # A: 1:73 "orkon...hmwn tou...hmin"
        #    1:74 "afobwv...autw"
        # B: 1:73 "orkon...hmwn"
        #    1:74 "tou...hmin afobwv...autw"                 
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['LU.BP5'].extend([
            (r'(1:73.*?)(tou .*?P-1DP})(.*?1:74)', r'\1\3 \2')])
        discrepancy_fixes['LU.TRP'].extend([
            (r'\(1:74\)', r'')])
        discrepancy_fixes['LU.CCT'].extend([
            (r'(01:73.*?), (.*?,)(.*?01:74)', r'\1\3 \2')])

        # Luke 17:36
        # A: 17:36 "duo...afeyhsetai"
        # B: 17:36 blank
        # All are B, except TRP has B with A as a variant.
        discrepancy_fixes['LU.TRP'].extend([
            (r'(17:36.*17:37)', r'17:36 \n17:37')])

        # Acts 4:5-6
        # A: 4:5 "egeneto...grammatein"
        #    4:6 "ein ierousalhm...arcieratikou"
        # B: 4:5 "egeneto...grammateiv...ein ierousalhm"
        #    4:6 "kai annas...arcieratikou"
        # All are B, except TRP has A with B as a variant.
        discrepancy_fixes['AC.TRP'].extend([
            (r'\n4:6 ', r' ')])

        # Acts 9:28-9
        # A: 9:28 "kai...ierousalhm kai...ihsou"
        #    9:29 "elalei...anelein"
        # B: 9:28 "kai...ierousalhm"
        #    9:29 "kai...ihsou elalei...anelein"
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['AC.BP5'].extend([
            (r' (9:28.*?)(kai 2532 {CONJ} parrhsiazomenov.*?})([\r\n].*?9:29)',
            r'\1\3 \2')])
        discrepancy_fixes['AC.TRP'].extend([
            (r'\(9:29\)', r'')])
        discrepancy_fixes['AC.CCT'].extend([
            (r'(09:28.*?Ierousalh/m).*?(ai.*})(.*?09:29)', r'\1.\3 K\2')])

        # Acts 13:32-33
        # A: 13:32 "kai...genomenhn oti...ihsoun
        #    13:33 "wv...se"
        # B: 13:32 "kai...genomenhn"
        #    13:33 "oti...ihsoun wv...se"
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['AC.BP5'].extend([
            (r'(13:32.*?)(oti.*?N-ASM})(.*?13:33)', r'\1\3 \2')])
        discrepancy_fixes['AC.TRP'].extend([
            (r'\(13:33\)', r'')])
        discrepancy_fixes['AC.CCT'].extend([
            (r'(13:32.*?)(o\(/ti.*?n:)(.*?13:33)', r'\1\3 \2')])

        # Acts 24:2 has a (24:3) in it. Yet all 4 texts agree on 24:2 and 24:3, so the (24:3) can be removed in the main body of pre_process_tagged.
        # Acts 24:2-3
        # A: 24:2 "klhyentov...legwn"
        #    24:3 "pollhv...pronoiav panth...eucaristiav"
        # B: 24:2 "klhyentov...legwn pollhv...pronoiav"
        #    24:3 "panth...eucaristiav"
        # All are B.
        discrepancy_fixes['AC.TRP'].extend([
            (r'\(24:3\)', r'')])
        discrepancy_fixes['AC.BP5'].extend([
            (r'\(24:3\)', r'')])

        # Acts 24:6-8
        # A: 24:6 "on...krinein parelthwn...aphgagen keleusan...se"
        #    24:7 blank
        #    24:8 "par...autou"
        # B: 24:6 "on...krinein"
        #    24:7 "parelthwn...aphgagen"
        #    24:8 "keleusan...se par...autou"
        # BP5 is A with variant for B, CCT is A, but AC24.CCT shows B
        # TRP & KJV are B. Choosing B
        discrepancy_fixes['AC.BP5'].extend([
            (r'\(24:8\)(.*?)\n:END.*?24:8 ', r'24:8\1'),
            (r'\(24:7\)', r'24:7'),
            (r'(24:6.*?)\|.*?VAR: ', r'\1')])

        # Rom. 14:24-26 and 16:25-27
        # A: verses located in chap. 14
        # B: verses located in chap. 16
        # KJV and TRP are B. CCT and BP5 are A. Choosing B.
        discrepancy_fixes['RO.BP5'].extend([
            (r'(14:24.*?)(15:1.*)', r'\2\1'),
            (r'14:2[456]', r'')])
            # There are square brackets in RO.TRP. Is this a colophon?
            # fix. See also 2CO.TRP. This may be common. Note that in
            # KJVfull this is not brought over (at least in 2CO).
        discrepancy_fixes['RO.CCT'].extend([
            (r'(14:24.*?)(15:01.*)', r'\2\1'),
            (r'14:24', r'16:25'),
            (r'14:25', r'16:26'),
            (r'14:26', r'16:27')])

        # 1 Cor. 2:13
        # This is a non-standard discrepancy fix. There are only three instances
        # in the BP5 files where two morph tags occur one after the other.
        # This is one of them.
        # A: pneumatikoiv 4152 {A-DPN} {A-DPM}
        # B: pneumatikoiv 4152 {A-DPN}
        # All are B except tagged is A. Choosing B
        discrepancy_fixes['1CO.BP5'].extend([
            (r'{A-DPN} {A-DPM}', r'{A-DPN}')])

        # 1 Cor. 12:1
        # This is a non-standard discrepancy fix. There are only three instances
        # in the BP5 files where two morph tags occur one after the other.
        # This is one of them.
        # A: twn 3588 {T-GPN} {T-GPM} pneumatikwn 4152 {A-GPN} {A-GPM}
        # B: twn 3588 {T-GPN} pneumatikwn 4152 {A-GPN}
        # All are B except tagged is A. Choosing B
        discrepancy_fixes['1CO.BP5'].extend([
            (r'{T-GPN} {T-GPM}', r'{T-GPN}'),
            (r'{A-GPN} {A-GPM}', r'{A-GPN}')])

        # 2 Cor. 13:12-14
        # A: 13:12 "aspasasye...filhmati aspazontai...pantev"
        #    13:13 "h...amhn"
        #    13:14 nonexistent
        # B: 13:12 "aspasasye...filhmati"
        #    13:13 "aspazontai...pantev"
        #    13:14 "h...amhn"
        # All are B. A is a variant suggested in TRP.
        discrepancy_fixes['2CO.TRP'].extend([
            (r'\(.*?\) ', r'')])

        # Gal 5:7
        # Technically this isn't a standard discrepancy fix. I'm just too lazy
        # to make the parser work with three strong's in a row in TRP files. So
        # I'm cheating until I actually fix this. fix.
        discrepancy_fixes['GA.TRP'].extend([
            (r'1465 348 5656', r'1465 5656')])

        # Rev 7:4
        # Technically this isn't a standard discrepancy fix. I'm just too lazy
        # to make the parser work with three strong's in a row in TRP files. So
        # I'm cheating until I actually fix this. fix.
        discrepancy_fixes['RE.TRP'].extend([
            (r'rmd.*?{A-NUI-ABB}',
             r'r 1540 {A-NUI-ABB} m 5062 {A-NUI-ABB} d 5064 {A-NUI-ABB}')])

        # Eph 3:17-18
        # A: 3:17 "katoikhsai...umwn"
        #    3:18 "en...teyemeliwmenoi ina...uqon"
        # B: 3:17 "katoikhsai...umwn en...teyemeliwmenoi"
        #    3:18 "ina uqon"
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['EPH.BP5'].extend([
            (r'(3:17.*})(.*?3:18)(.*?)(?=ina)', r'\1\3\2 ')])
        discrepancy_fixes['EPH.TRP'].extend([
            (r'\(3:18\)', r'')])
        discrepancy_fixes['EPH.CCT'].extend([
            (r'(03:17.*?mw\^n):(.*?03:18 )(.*?)(?= i)', r'\1 \3\2')])

        # 1 Thess 2:11
        # A: 2:11 "kayaper...paramuthumenoi"
        #    2:12 "kai marturoumenoi...ein...doxan"
        # B: 2:11 "kayaper...paramuthumenoi...kai marturoumenoi"
        #    2:12 "ein...doxan"
        # CCT and BP5 are A, TRP is B, and KJV does not have the phrase
        # "kai marturoumenoi". Choosing B.
        discrepancy_fixes['1TH.BP5'].extend([
            (r'(2:12 )(.*?)(?=eiv)', r'\2\1')])
        discrepancy_fixes['1TH.TRP'].extend([
            (r'\(2:12\)', r'')])
        discrepancy_fixes['1TH.CCT'].extend([
            (r'(02:12 )(.*?)(?=ei\)s)', r'\2\1')])

        # Philemon 1:11-12
        # A: 1:11 "ton...eucrhston on...anepemqa"
        #    1:12 "su...proslabou"
        # B: 1:11 "ton...eucrhston"
        #    1:12 "on...anepemqa su...proslabou"
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['PHM.BP5'].extend([
            (r'(1:11.*?)(on 37.*})(.*?1:12)', r'\1\3 \2')])
        discrepancy_fixes['PHM.TRP'].extend([
            (r'\(1:12\)', r'')])
        discrepancy_fixes['PHM.CCT'].extend([
            (r'(01:11.*?u\)/xrhston,) (.*?:)(.*?01:12)', r'\1\3 \2')])

        # Heb 1:1-2
        # A: 1:1 "polumerwn...profhtaiv ep...uiw"
        #    1:2 "on...epoihsen"
        # B: 1:1 "polumerwn...profhtaiv"
        #    1:2 "ep...uiw on...epoihsen"                 
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['HEB.BP5'].extend([
            (r'(\s1:1.*?)(ep .*?DSM})(.*?\s1:2)', r'\1\3 \2')])
        discrepancy_fixes['HEB.TRP'].extend([
            (r'\(1:2\)', r'')])
        discrepancy_fixes['HEB.CCT'].extend([
            (r'(01:01.*?), (.*?,)(.*?01:02)', r'\1\3 \2')])

        # Heb 7:20-1
        # A: 7:20 "kai...orkwmosiav oi...gegonotev"
        #    7:21 "o...melcisedek"
        # B: 7:20 "kai...orkwmosiav"
        #    7:21 "oi...gegonotev o...melcisedek"                 
        # BP5 and CCT are A, KJV and TRP are B. Choosing B.
        discrepancy_fixes['HEB.BP5'].extend([
            (r'(7:20.*?)(oi .*?P-NPM})(.*?7:21)', r'\1\3 \2')])
        discrepancy_fixes['HEB.TRP'].extend([
            (r'\(7:21\)', r'')])
        discrepancy_fixes['HEB.CCT'].extend([
            (r'(07:20.*?) - (.*?,)(.*?07:21)', r'\1\3 \2')])

        # 3 John 1:14-15
        # A: 1:14 "elpizw...lalhsomen"
        #    1:15 "eirhnh...onoma"
        # B: 1:14 "elpizw...lalhsomen eirhnh...onoma"
        #    1:15 N/A
        # All are B. A is a variant suggested in TRP.
        discrepancy_fixes['3JO.TRP'].extend([
            (r'\(.*?\) ', r'')])

        # Rev 12:17-13:1
        # A: 12:18 "kai...yalasshv"
        #    13:1  "yalasshv...blasfhmiav"
        # B: 12:18 N/A
        #    13:1  "kai...yalasshv yalasshv...blasfhmiav"
        # All are B.
        discrepancy_fixes['RE.TRP'].extend([
            (r'\(12:18\)', r''),
            (r'\(13:1\)', r'')])

        # Rev. 17:8
        # A: 17:7 "kai...kerata to...upagein" 17:8 "kai...perastai"
        # B: 17:7 "kai...kerata" 17:8 "to...upagein kai...perastai"
        # All are B except BP5.
        discrepancy_fixes['RE.BP5'].extend([
            (r'17:8 ', r'')])
        #So many variants in rev 17:8. why? fix?
        return discrepancy_fixes

    def bword_to_uword(self, betacode):
        uni = cur_group = lastletter = ''
        groups = []
        betacode=betacode.replace('\n','')
        betacode=betacode.replace('\r','')
        # If word is capitalized, first move capital letter and accents to
        # first group.
        if not betacode[0].islower():
            for ii in range(len(betacode)):
                if betacode[ii].isupper():
                    assert(len(groups) == 0)
                    groups = [cur_group + betacode[ii]]
                    betacode = betacode[ii + 1:]
                    break
        for char in betacode:
            if char in '\\/()+^|':
                groups[-1] = groups[-1]+char
            elif char.islower():
                groups.append(char)
                lastletter = char
            elif char in ':;.,\'-':
                groups.append(char)
        if lastletter == 's': #Handle final sigma.
            for ii in range(len(groups)):
                if 's' in groups[-ii-1]:
                    groups[-ii-1] = groups[-ii-1].replace('s','final_s')
                    break
        for group in groups:
            if group in self.beta_to_uni_dict:
                uni += self.beta_to_uni_dict[group]
        return uni

    def create_beta_to_uni_dict(self):
        # See http://www.unicode.org/charts/PDF/U0370.pdf and
        # http://www.unicode.org/charts/PDF/U1F00.pdf for source. The following
        # dictionary, beta_to_uni_dict, is sufficient for parsing the betacode
        # from BYZ05CCT, but not all the entries are necessary.
        eng_letters = 'ABGDEZHQIKLMNCOPR STUFXYW       abgdezhqiklmncopr stufx'
        eng_letters += 'yw'
        counter = 0
        beta_to_uni_dict = {}
        # Put all simple letters into dictionary.
        for char in eng_letters:
            # Is an exec really necessary here (and further down)? Fix.
            exec('beta_to_uni_dict[char]=u\'\\u0'+str(hex(int('391',16)+counter))[-3:]+'\'')
            counter += 1
        del beta_to_uni_dict[' ']
        counter = 0
        #Include 1F00 thru 1F6F.
        for base_letter in ['a','e','h','i','o','u','w']:
            for case in ['lower','upper']:
                for accent in [None,'\\','/','^']:
                    for breathing in [')','(']:
                        if case == 'upper':
                            char = base_letter.upper()
                            if accent:
                                char = accent+char
                            char = breathing+char
                        else:
                            char = base_letter
                            char = char+breathing
                            if accent:
                                char = char+accent
                        exec('beta_to_uni_dict[char]=u\'\\u1'+
                             str(hex(int('F00',16)+counter))[-3:]+'\'')
                        counter += 1
        #Include 1F70 thru 1F7F.
        counter = 0
        for base_letter in ['a','e','h','i','o','u','w']:
            for accent in ['\\','/']:
                char = base_letter+accent
                if char not in beta_to_uni_dict:
                    exec('beta_to_uni_dict[char]=u\'\\u1'+
                         str(hex(int('F70',16)+counter))[-3:]+'\'')
                counter += 1
        #Include 1F80 thru 1FAF.
        counter = 0
        for base_letter in ['a','h','w']:
            for case in ['lower','upper']:
                for accent in [None,'\\','/','^']:
                    for breathing in [')','(']:
                        if case == 'upper':
                            char = base_letter.upper()
                            char = '|'+char
                            if accent:
                                char = accent+char
                            char = breathing+char
                        else:
                            char = base_letter
                            char = char+breathing
                            if accent:
                                char = char+accent
                            char = char+'|'
                            ## char = base_letter
                            ## if accent:
                            ##     char = char+accent
                            ## char = char+breathing
                            ## char = char+'|'
                        exec('beta_to_uni_dict[char] = u\'\\u1'+
                             str(hex(int('F80',16)+counter))[-3:]+'\'')
                        counter += 1
        #Include 1FB2 thru 1FB7, 1FC2 thru 1FC7, 1FF2 thru 1FF7.
        counter = 0
        col_counter = -1
        col_str = 'BCF'
        for base_letter in ['a','h','w']:
            col_counter += 1
            row_counter = 2
            for accent_iota_subscript_pair in ['\\|','|','/|',False,'^','^|']:
                if accent_iota_subscript_pair:
                    char = base_letter
                    char = char+accent_iota_subscript_pair
                    exec('beta_to_uni_dict[char]=u\'\\u1'+
                         str(hex(int('F'+col_str[col_counter]+
                                     str(row_counter),16)))[-3:]+'\'')
                row_counter += 1

        # Include 1FE6 and friends to unicode chart.
        counter = 0
        for base_letter in ['a','h','i','u','w']:
            for accent in ['^']:
                char = base_letter+accent
                if char not in beta_to_uni_dict:
                    exec('beta_to_uni_dict[char]=u\'\\u1'+
                         str(hex(int('FB6',16)+16*counter))[-3:]+'\'')
                counter += 1
        #Include 0386 thru 039F, 03AA thru 03B0, and 03CA thru 03CE, and other
        #specials.
        specials_list = (('/O', u'\u038c'),('/I', u'\u038a'),('/H', u'\u0389'),
                       ('w/', u'\u03ce'),('/E', u'\u0388'),('/A', u'\u0386'),
                       ('h/', u'\u03ae'),('/W', u'\u038f'),('/U', u'\u038e'),
                       ('o/', u'\u03cc'),('a/', u'\u03ac'),('u/', u'\u03cd'),
                       ('u+', u'\u03cb'),('e/', u'\u03ad'),('i/', u'\u03af'),
                       ('i+', u'\u03ca'),('.',u'.'),(',',u','),(';',u'\u003b'),
                       (':',u'\u00B7'),('u+\\',u'\u1fe2'),('i+\\',u'\u1fd2'),
                       ('i+/',u'\u1fd3'),('u+/',u'\u1fe3'),('r(',u'\u1fe5'),
                       ('w^|',u'\u1ff7'),('final_s', u'\u03c2'),('\'',u'\''))
        for item in specials_list:
            beta_to_uni_dict[item[0]] = item[1]
        return beta_to_uni_dict

    def create_swapcase_dict(self):
        # Return a mapping from, e.g., the unicode equivalent of ')^a' to the
        # unicode equivalent of ')^A'.
        specials_list = ['final_s']
        swapcase_dict = {}
        for key in self.beta_to_uni_dict:
            if key in specials_list:
                continue
            match = re.search(r'[A-z]', key)
            if match:
                letter = match.group()
                complement_key = key.replace(letter, letter.swapcase())
            if complement_key in self.beta_to_uni_dict:
                uni = self.beta_to_uni_dict[key]
                uni_complement = self.beta_to_uni_dict[complement_key]
                swapcase_dict[uni] = uni_complement
        return swapcase_dict

    def create_is_capital_dict(self):
        # Map, e.g. ')^A' to True since it is indeed capital. Map ';' to None.
        specials_list = ['final_s']
        is_capital_dict = {}
        for key in self.beta_to_uni_dict:
            uni = self.beta_to_uni_dict[key]
            match = re.search(r'[a-zA-Z]', key)
            if key in specials_list:
                is_capital_dict[uni] = False
            elif match:
                letter = match.group()
                if letter == letter.lower():
                    is_capital_dict[uni] = False
                else:
                    is_capital_dict[uni] = True
            else:
                # E.g. ';'
                is_capital_dict[uni] = None
        return is_capital_dict

        
if __name__ == '__main__':
    t = time.time()
    bible_parser = BibleParser(OUTPUT_FILE_NAME_FULL, OUTPUT_FILE_NAME_LIGHT,
                               testing=False, refresh_working=False,
                               mini=False)
    grk_nt = bible_parser.bible['grk.nt']
    heb_ot = bible_parser.bible['heb.ot']
    eng_ot = bible_parser.bible['eng.ot']
    grk_book = bible_parser.bible['grk.nt.Matt']
    grk_chap = bible_parser.bible['grk.nt.Matt.1']
    grk_verse = bible_parser.bible['grk.nt.Matt.1.1']
    grk_word = bible_parser.bible['grk.nt.Matt.1.1.1']
    print "Total script run time: " + str(time.time() - t) + " s"
