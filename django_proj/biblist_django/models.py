from django.db import models
from math import exp, tanh, log
# TODO find actual maxes for the max_lengths below.
class Testament(models.Model):
    name = models.CharField(max_length=30, null=True, db_index=True)
    disp_name = models.CharField(max_length=30, null=True)
    prim_lang = models.CharField(max_length=3, null=True)
    sec_lang = models.CharField(max_length=3, null=True)

    def __unicode__(self):
        return self.name

    def get_verse(self, path):
        book, chap, verse = path.split('.')
        return self.book_set.get(name=book).chapter_set.get(
            number=chap).verse_set.get(number=verse)

class Book(models.Model):
    testament = models.ForeignKey(Testament, db_index=True)
    full_name = models.CharField(max_length=20, null=True)
    name = models.CharField(max_length=20, null=True)
    index = models.IntegerField(null=True, db_index=True)

    def __unicode__(self):
        return self.name
        
class Chapter(models.Model):
    book = models.ForeignKey(Book)
    number = models.IntegerField(null=True)
    index = models.IntegerField(null=True, db_index=True)

    def __unicode__(self):
        return unicode(self.number)
        
class Verse(models.Model):
    chapter = models.ForeignKey(Chapter)
    is_bad = models.BooleanField(default=False)
    number = models.IntegerField(null=True)
    index = models.IntegerField(null=True, db_index=True)
    importance = models.FloatField(null=True, db_index=True)
    path = models.CharField(max_length=15, null=True, db_index=True)

    def __unicode__(self):
        return unicode(self.number)

        
class EngVerse(models.Model):
    verse = models.OneToOneField(Verse)
    text = models.CharField(max_length=1000, null=True)
    
class StrongWord(models.Model):
    number = models.CharField(max_length=5, null=True)
    verses = models.ManyToManyField(Verse)
    uninflected_word = models.CharField(max_length=20, null=True,
                                        db_index=True)
    testament = models.ForeignKey(Testament, null=True, db_index=True)
    occs = models.IntegerField(null=True, db_index=True)
    importance = models.FloatField(null=True)
    
    def __unicode__(self):
        return unicode(self.number)

class Word(models.Model):
    verse = models.ForeignKey(Verse)
    strong_word = models.ForeignKey(StrongWord, null=True)
    text = models.CharField(max_length=50, null=True)
    eng_text = models.CharField(max_length=50, null=True)
    tag = models.CharField(max_length=10, null=True)
    number = models.IntegerField(null=True)
    index = models.IntegerField(null=True, db_index=True)

    def __unicode__(self):
        return self.text
        
class User(models.Model):
    name = models.CharField(max_length=30, null=True, db_index=True)
    
    def __unicode__(self):
        return self.name

    def update_bible_data(self):
        '''Update (or create) UserWord and UserVerse objects.'''
        # Create missing UserWords.
        user_strong_words = [x.strong_word for x in UserWord.filter(user=self)]
        for strong_word in StrongWord.objects.all():
            if strong_word not in user_strong_words:
                UserWord.create(user=self, strong_word=strong_word,
                                importance=strong_word.importance)

        # Create missing UserVerses.
        user_verse_paths = [x.path for x in UserVerse.filter(user=self)]
        for verse in Verse.objects.all():
            if verse.path not in user_verse_paths:
                UserVerse.create(user=self,
                                 testament=verse.chapter.book.testament,
                                 path=verse.path,
                                 verse=verse)

    def calculate_importances(self, strong_words, max_occs):
        for testament in Testament.objects.all():
            for user_word in UserWord.filter(user=user, testament=testament):
            # Algorithm for computing importance as a function of occurrence
            # frequency. The +1 is so that words with zero occurrences will
            # have zero importance.
                tmp = (log(float(len(strong_word.paths) + 1)) /
                       log(float(max_occs + 1)))
                strong_word.importance = tmp

            
class UserWord(models.Model):
    user = models.ForeignKey(User, db_index=True)
    strong_word = models.ForeignKey(StrongWord, db_index=True)
    importance = models.FloatField()
    knownness = models.FloatField()
    suitability = models.FloatField()
    last_asked = models.IntegerField(null=True)
    is_blacklisted = models.BooleanField(default=False, db_index=True)

    def update_after_test(self, test_num, sat, passed):
        # Update times asked.
        if sat == 'ask' and test_num is not None:
            self.last_asked = test_num

        # Update knownness. fixme: introduce smooth functions for this.
        ## If knownness is 1.0, this is a blacklisted number, and it
        ## should not be changed.
        if self.knownness == 1.0:
            return
        if self.knownness is None:
            self.knownness = 0
        if sat == 'show':
            if passed:
                if self.knownness < 0.95:
                    self.knownness += .005
            else:
                self.knownness -= 0.3
        elif sat == 'ask':
            if passed:
                if self.knownness > .95:
                    self.knownness += (0.99 - self.knownness) * .5
                elif self.knownness > .75:
                    self.knownness += .1
                elif self.knownness > .5:
                    self.knownness += .15
                elif self.knownness > .25:
                    self.knownness += .15
                else:
                    self.knownness += .15
            else:
                if self.knownness > .75:
                    self.knownness -= .2
                elif self.knownness > .5:
                    self.knownness -= .15
                elif self.knownness > .25:
                    self.knownness -= .05
        elif sat == 'tell':
            if passed:
                self.knownness = 0.95
            else:
                if self.knownness > .25:
                    self.knownness += .01
                elif self.knownness > .15:
                    self.knownness += .02
                else:
                    self.knownness += .03
        if self.knownness < 0:
            self.knownness = 0

    def update_learnability(self):
        x = 0 if self.knownness is None else self.knownness
        # Increase to flatten out bell curve. Recommend between .1 and .35.
        flatness = .25
        # Increase to weigh lesser known words more heavily.
        tilt_factor = .3
        # Increase to shift bell curve towards lesser known words
        center = .7
        # Increase to shift bell curve up and make fully unknown words more
        # likely to appear.
        y_offset = .05
        tmp = (exp(-(x - center) ** 2 / flatness ** 2) +
               tilt_factor * x + y_offset)
        slanted_bell_curve_component = tmp
        # Increase to sharpen the dropoff, such that dropoff doesn't begin
        # until 95-98
        steepness = 5
        # Ensures that as knownness approaches 1, learnability approaches 0.
        zeroing_component = -tanh(steepness * (x - 1))
        # Ensures that learnability peaks near 1.
        tmp = ((1 + tilt_factor * center + y_offset) *
               (-tanh(steepness * (center - 1)))) ** -1
        normalized_component = tmp
        self.learnability = (slanted_bell_curve_component * zeroing_component *
                             normalized_component)

    def update_ripeness(self, cur_test_num):
        if self.last_asked is None:
            time_since = float('inf')
        else:
            time_since = cur_test_num - self.last_asked
        # Higher means the ripeness more rapidly approaches 1 as time goes on.
        steepness = .1
        # At time_since == inf, ripeness will be 10 ** exp_at_inf.
        exp_at_inf = 0
        # At time_since == 0, ripeness will be 10 ** exp_at_zero.
        exp_at_zero = -1.8
        self.ripeness = 10 ** ((exp_at_inf - exp_at_zero) *
                               tanh(steepness * (time_since)) + exp_at_zero)

    def update_suitability(self):
        self.suitability = self.importance * self.ripeness * self.learnability

class UserVerse(models.Model):
    user = models.ForeignKey(User, db_index=True)
    testament = models.ForeignKey(Testament, null=True, db_index=True)
    path = models.CharField(max_length=12)
    last_asked = models.IntegerField(null=True)
    word_count = models.IntegerField(null=True)
    verse = models.ForeignKey(Verse, db_index=True)

    def update_ripeness(self, cur_test_num):
        if self.last_asked:
            time_since = cur_test_num - self.last_asked
        else:
            time_since = float('inf')

        if time_since < 5:
            self.ripeness = float(0)
            return
        # Higher means the ripeness more rapidly approaches 1 as time goes on.
        steepness = .02
        # At time_since == inf, ripeness will be 10 ** exp_at_inf.
        exp_at_inf = 0
        # At time_since == 0, ripeness will be 10 ** exp_at_zero.
        exp_at_zero = -2
        self.ripeness = 10 ** ((exp_at_inf - exp_at_zero) *
                               tanh(steepness * (time_since)) + exp_at_zero)

    def update_suitability(self):
        # Update verse suitability, preferring verses with higher
        # ripeness and importance, and preferring shorter verses to
        # longer ones.
        
        # Value of 1 means that verse suitability is proportional to mean word
        # suitability and underemphasizes verse shortness. Value of 2
        # emphasizes verse shortness probably at too large an expense of word
        # knownness.
        shortness_weighting = 1.5
            
        self.suitability = (float(self.ripeness * self.verse.importance) /
                            float(self.word_count) **
                            shortness_weighting)
        
class VRRecord(models.Model):
    '''Record for results from Vocab and Reading modules.'''
    user = models.ForeignKey(User, db_index=True)
    verse = models.ForeignKey(Verse, null=True)
    datetime = models.DateTimeField(null=True)
    module = models.CharField(max_length=5, null=True, db_index=True)

class DiagRecord(models.Model):
    '''Record for results from diagnostic test.'''
    user = models.ForeignKey(User, db_index=True)
    strong_word = models.ForeignKey(StrongWord, null=True, db_index=True)
    knownness = models.FloatField()
    datetime = models.DateTimeField(db_index=True)

class SATValue(models.Model):
    vrrecord = models.ForeignKey(VRRecord, db_index=True)
    index = models.IntegerField(db_index=True)
    value = models.CharField(max_length=4)

class PassedValue(models.Model):
    vrrecord = models.ForeignKey(VRRecord, db_index=True)
    index = models.IntegerField(db_index=True)
    value = models.NullBooleanField(default=False, null=True)

