#+TITLE:     Biblist Application Info
#+AUTHOR:    Rich Kinder
#+EMAIL:     kinderrich@gmail.com
#+DATE:      2014-08-21 Thur
#+DESCRIPTION: Setup information for Biblist
#+KEYWORDS:
#+LANGUAGE:  en
#+OPTIONS:   H:3 num:t toc:t \n:nil @:t ::t |:t ^:nil -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:http://orgmode.org/org-info.js
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+LINK_UP:   
#+LINK_HOME: 
#+XSLT:

* Application Setup
** Install dependencies
#+BEGIN_SRC sh
sudo apt-get install python-kivy
sudo pip install kivy-garden
cd ~/biblist
garden install --app graph
#+END_SRC

** Install fonts
Download Linux Libertine, and put LinLibertine_Rah.ttf, LinLibertine_RBah.ttf
and any licenses into biblist/fonts/.

** Install dependencies for bible_parser.py
This is not necessary if you already have the pickle files.
*** Install python modules
#+BEGIN_SRC sh
sudo pip install munkres
#+END_SRC

*** Install source texts
**** English
In biblist/source_texts/eng, put kjvfull.xml, which can be downloaded from 
http://www.crosswire.org/~dmsmith/kjv2006/ (the OSIS xml link).

**** Greek
From http://www.byztxt.com/downloads.html, get TR-PRSD.ZIP, BYZ05CCT.ZIP, and
BP05FNL.ZIP, and unzip them into folders TR-PRSD, BYZ05CCT, and BP05FNL (resp.)
in biblist/source_texts/grk/byztxt/. Note: As of 10/28/2014 the TR-PRSD file
appears to be broken (it is unzippable, at least on Ubuntu), but it can be
gotten from my Dropbox.

**** Hebrew
Clone the morphhb project, then copy the morphhb/wlc folder to 
biblist/source_texts/heb/
#+BEGIN_SRC sh
git clone git://github.com/openscriptures/morphhb.git
cp -r morphhb/wlc ~/biblist/source_texts/heb
#+END_SRC

Clone the HebrewLexicon project, then copy the HebrewMesh.xml file to 
biblist/source_texts/heb/
#+BEGIN_SRC sh
git clone git://github.com/openscriptures/HebrewLexicon.git
cp HebrewLexicon/OldLexicon/HebrewMesh.xml ~/biblist/source_texts/heb/
#+END_SRC

* Development Setup
** Install dependencies required for testing
#+BEGIN_SRC sh
sudo apt-get install python-pytest
#+END_SRC
