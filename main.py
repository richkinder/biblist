# Thin wrapper around biblist_kivy.py, because buildozer needs a file
# named main.py. I would rather have this very kivy-specific file in the
# biblist_kivy subdir, but I'm not sure how to get buildozer to include a
# directory above main.py. By default it wants to include only
# subdirectories.
__version__ = "0.1.3"

# Standard library imports
import os
import sys

# Project imports
## Add the biblist_kivy folder to the path, so biblist_kivy.py can be imported.
try:
    cur_dir = os.path.dirname(os.path.abspath(__file__))
except NameError:
    # This file has been executed interactively (perhaps in the Python
    # interpreter through execfile) and does not necessarily have an
    # associated file. Assume that the interpreter was started in the
    # same directory as the file.
    cur_dir = os.getcwd()

sys.path.append(os.path.join(cur_dir, "biblist_kivy"))
from biblist_kivy import BiblistApp
# Enable post-mortem debugging with pdb.pm()
import pdb

if __name__ == '__main__':
    BiblistApp().run()
