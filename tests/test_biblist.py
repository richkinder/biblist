# This file intended to be run with py.test. See the last block for more info.
 
# Standard library imports.
import os
import sys

# Third-party imports
import pytest

# Project imports
try:
    parent_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
except NameError:
    # This file has been executed interactively (perhaps in the Python
    # interpreter through execfile) and does not necessarily have an
    # associated file. Assume that the interpreter was started in the
    # same directory as the file.
    parent_dir = os.path.dirname(os.getcwd())
sys.path.append(parent_dir)

import biblist
import peradaptil

@pytest.fixture()
def app_with_data():
    app_with_data = biblist.AppBackend()
    app_with_data.get_data_file(app_with_data.default_data_file_path)
    return app_with_data

@pytest.fixture()
def app():
    return biblist.AppBackend()

# Tests for initial variables.
def test_default_ail(app):
    assert(isinstance(app.default_ail, peradaptil.AdaptiveInterlinear))
    

# Tests for user actions: addition, deletion, renaming, switching, and
# current user.
def test_add_user(app):
    app.add_user('Alan')
    app.add_user('Bob')
    app.add_user('Charles')
    users = sorted([x.name for x in app.users])
    assert users == ['Alan', 'Bob', 'Charles']

def test_add_user_current_verse(app):
    app.add_user('Alan')
    assert app.read_cur_ail.prim_lang == "grk"
    assert app.read_cur_verse == "Matt.1.1"

def test_add_user_non_alphabetical(app):
    app.add_user('Charles')
    app.add_user('Alan')
    app.add_user('Bob')
    users = sorted([x.name for x in app.users])
    assert users == ['Alan', 'Bob', 'Charles']

def test_add_bad_user_redundant(app):
    app.add_user('Alan')
    with pytest.raises(biblist.UsernameClashError):
        app.add_user('alAn')

def test_add_bad_user_blank(app):
    with pytest.raises(biblist.BadUsernameError):
        app.add_user('')

def test_add_bad_user_tabs(app):
    with pytest.raises(biblist.BadUsernameError):
        app.add_user('	') # That is a tab in there.

def test_add_bad_user_newline(app):
    with pytest.raises(biblist.BadUsernameError):
        app.add_user('\n')

def test_add_bad_user_initial_space(app):
    with pytest.raises(biblist.BadUsernameError):
        app.add_user(' Alan')

def test_add_bad_user_final_space(app):
    with pytest.raises(biblist.BadUsernameError):
        app.add_user('Alan ')

def test_delete_user(app):
    app.add_user('Alan')
    app.add_user('Bob')
    app.add_user('Charles')
    app.delete_user('Charles')
    users = sorted([x.name for x in app.users])
    assert users == ['Alan', 'Bob']

def test_delete_last_user(app):
    app.add_user('Alan')
    with pytest.raises(biblist.DeleteLastUserError):
        app.delete_user('Alan')

def test_delete_bad_user(app):
    app.add_user('Alan')
    with pytest.raises(biblist.BadUsernameError):
        app.delete_user('Jim')

def test_rename_user(app):
    app.add_user('Alan')
    app.rename_user('Alan', 'Al')
    users = sorted([x.name for x in app.users])
    assert users == ['Al']

def test_rename_bad_user(app):
    app.add_user('Alan')
    with pytest.raises(biblist.BadUsernameError):
        app.rename_user('Cain', 'Abel')

def test_rename_redundant_user(app):
    app.add_user('Alan')
    app.add_user('Bob')
    with pytest.raises(biblist.UsernameClashError):
        app.rename_user('Bob', 'Alan')

def test_cur_user(app):
    app.add_user('Alan')
    assert app.cur_user.name == 'Alan'

def test_cur_user_after_add(app):
    app.add_user('Alan')
    app.add_user('Bob')
    assert app.cur_user.name == 'Bob'

def test_switch_user(app):
    app.add_user('Alan')
    app.add_user('Bob')
    app.switch_user('Alan')
    assert app.cur_user.name == 'Alan'

def test_switch_bad_user(app):
    app.add_user('Alan')
    with pytest.raises(biblist.BadUsernameError):
        app.switch_user('Bob')

def test_switch_user_vocab_verse(app):
    """Verify that switching a user also switches the vocab verse."""
    app.add_user('Alan')
    app.vocab_switch_verse("Mark.1.1")
    app.add_user('Bob')
    app.vocab_switch_verse("Luke.1.1")
    app.switch_user('Alan')
    assert app.vocab_cur_book == "Mark"
    assert app.vocab_cur_chap == "Mark.1"
    assert app.vocab_cur_verse == "Mark.1.1"

def test_switch_user_read_verse(app):
    """Verify that switching a user also switches the read verse."""
    app.add_user('Alan')
    app.read_switch_verse("Mark.1.1")
    app.add_user('Bob')
    app.read_switch_verse("Luke.1.1")
    app.switch_user('Alan')
    assert app.read_cur_book == "Mark"
    assert app.read_cur_chap == "Mark.1"
    assert app.read_cur_verse == "Mark.1.1"

def test_get_data_file(app):
    app.get_data_file("../interlinears/grk_eng_nt")
    assert(len(app.ails) > 0)
    assert(len(app.users) > 0)
    # Incomplete...

def test_vocab_answer(app):
    self.vocab_answered_correctly()
    assert self.vocab_hist[-1].is_correct == True
    self.vocab_answered_incorrectly()
    assert self.vocab_hist[-1].is_correct == False
    assert self.vocab_hist[-2].is_correct == True
    
if __name__ == '__main__':
    # Could also be run with pytest.main(['--pdb', '--verbose']) which
    # is equivalent to py.test --pdb --verbose
    pytest.main()
