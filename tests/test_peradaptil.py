# -*- coding: utf-8 -*-
# This file intended to be run with py.test. See the last block for more info.

# Standard library imports.
import os
import sys

# Third-party imports
import pytest

# Project imports
try:
    parent_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
except NameError:
    # This file has been executed interactively (perhaps in the Python
    # interpreter through execfile) and does not necessarily have an
    # associated file. Assume that the interpreter was started in the
    # same directory as the file.
    parent_dir = os.path.dirname(os.getcwd())
sys.path.append(parent_dir)
import peradaptil

# Static Interlinear tests
@pytest.fixture(scope="module")
def sil():
    src_dir = os.path.join(parent_dir, "texts", "grk_eng_kjv")
    return peradaptil.StaticInterlinear(src_dir)

## StaticInterlinear object tests
def test_prim_lang(sil):
    assert sil.prim_lang == "grk"

def test_sec_lang(sil):
    assert sil.sec_lang == "eng"

def test_first_book(sil):
    first_book = sil.first_book
    second_book = sil.first_book.next()
    assert first_book is second_book.prev()

    
## Book tests
def test_book_name(sil):
    book = sil["John"]
    assert book.name == "John"
        
def test_book_length(sil):
    book = sil["John"]
    assert len(book) == 21

def test_book_next(sil):
    book = sil["John"]
    assert book.next.name == "Acts"

def test_book_prev(sil):
    book = sil["John"]
    assert book.prev.name == "Acts"

def test_book_prim_lang(sil):
    book = sil["John"]
    assert book.prim_lang == "grk"

def test_book_sec_lang(sil):
    book = sil["John"]
    assert book.sec_lang == "eng"

def test_book_abbrev(sil):
    book = sil["John"]
    assert book.abbrev == "Joh"

## Chapter tests
def test_chap_name(sil):
    chap = sil["John.1"]
    assert chap.name == "John.1"
        
def test_chap_length(sil):
    chap = sil["John.1"]
    assert len(chap) == 51
        
def test_chap_next(sil):
    chap = sil["John.1"]
    assert chap.next.name == "John.2"

def test_chap_prev(sil):
    chap = sil["John.1"]
    assert chap.prev.name == "Luke.24"

def test_chap_prim_lang(sil):
    chap = sil["John.1"]
    assert chap.prim_lang == "grk"

def test_chap_sec_lang(sil):
    chap = sil["John.1"]
    assert chap.sec_lang == "eng"

def test_chap_num(sil):
    chap = sil["John.2"]
    assert chap.num == 2

## Verse tests
def test_verse_name(sil):
    verse = sil["John.1.1"]
    assert verse.name == "John.1.1"
        
def test_verse_length(sil):
    verse = sil["John.1.1"]
    assert(len(verse) == 1000000) # not sure how many words in John 1:1
        
def test_verse_next(sil):
    verse = sil["John.1.1"]
    assert verse.next.name == "John.1.2"

def test_verse_prev(sil):
    verse = sil["John.1.1"]
    assert verse.prev.name == "Luke.24.53"

def test_verse_prim_lang(sil):
    verse = sil["John.1.1"]
    assert verse.prim_lang == "grk"

def test_verse_sec_lang(sil):
    verse = sil["John.1.1"]
    assert verse.sec_lang == "eng"

def test_verse_sec_text(sil):
    verse = sil["John.1.1"]
    assert(verse.sec_text == ("In the beginning was the Word, and the Word "
                              "was with God, and the Word was God."))
def test_verse_num(sil):
    verse = sil["John.1.2"]
    assert verse.num == 2


## Word tests    
def test_word_name(sil):
    word = sil["John.1.1.1"]
    assert word.name == "John.1.1.1"
        
def test_word_length(sil):
    word = sil["John.1.1.1"]
    assert(len(word) == 2) # 2 letters in En? is there a smooth breathing mark?

def test_word_next(sil):
    word = sil["John.1.1.1"]
    assert word.next.name == "John.1.1.2"

def test_word_prev(sil):
    word = sil["John.1.1.1"]
    assert(word.prev.name == "Luke.24.53.12") # not sure how many.

def test_word_prim_lang(sil):
    word = sil["John.1.1.1"]
    assert word.prim_lang == "grk"

def test_word_sec_lang(sil):
    word = sil["John.1.1.1"]
    assert word.sec_lang == "eng"
    
def test_word_strong(sil):
    word = sil["John.1.1.1"]
    assert word.strong.num == "1722"

def test_word_grammar(sil):
    word = sil["John.1.1.1"]
    assert word.grammar == "PREP"

def test_word_text(sil):
    word = sil["John.1.1.1"]
    assert word.text == "Ἐν"

def test_word_sec_text(sil):
    word = sil["John.1.1.1"]
    assert word.sec_text == "In"

def test_word_num(sil):
    word = sil["John.1.1.2"]
    assert word.num == 2

## Character tests
def test_char_name(sil):
    char = sil["John.1.1.1.1"]
    assert char.name == "John.1.1.1.1"
        
def test_char_length(sil):
    char = sil["John.1.1.1.1"]
    assert isinstance(char, unicode)
        
def test_char_next(sil):
    char = sil["John.1.1.1.1"]
    assert char.next.name == "John.1.1.1.2"

def test_char_prev(sil):
    char = sil["John.1.1.1.1"]
    assert(char.prev.name == "Luke.24.53.12.4") # not sure how many.

def test_char_prim_lang(sil):
    char = sil["John.1.1.1.1"]
    assert char.prim_lang == "grk"

def test_char_sec_lang(sil):
    char = sil["John.1.1.1.1"]
    assert char.sec_lang == "eng"

def test_char_num(sil):
    char = sil["John.1.1.1.2"]
    assert char.num == 2

# Adaptive interlinear tests.
@pytest.fixture(scope="module")
def ail(sil):
    return peradaptil.AdaptiveInterlinear(sil)
    
def test_prim_lang(ail):
    assert ail.prim_lang == "grk"

def test_sec_lang(ail):
    assert ail.sec_lang == "eng"

def test_export(ail):
    ail.export("interlinears/grk_eng_nt")
    ail_path = os.path.join(parent_dir, "interlinear", "grk_eng_nt_ail.pickle")
    sil_path = os.path.join(parent_dir, "interlinear", "grk_eng_nt_sil.pickle")
    assert os.path.exists(ail_path)
    assert os.path.exists(sil_path)
    
# Supply an adaptive interlinear.

# How does the adaptive relate to the static? What must it have that the
# static doesn't?
# -Attributes:
# --History of the user's interaction with the static.
# -Methods:
# --Word and verse suitabilities, and therefore knownesses,
# learnabilities, ripenesses, etc. (for vocab)
# --Grammar form suitabilities (for parsing)
# --Word hiding/showing based on knownnesses (for reading).


if __name__ == '__main__':
    # Could also be run with pytest.main(['--pdb', '--verbose']) which
    # is equivalent to py.test --pdb --verbose
    pytest.main()
